<?php

namespace App\Http\Controllers\Website\Report;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\Report;
use App\Models\User;
use App\Models\Informasi;
use Validator;
use Illuminate\Http\Request;
use Auth;
use Google\Cloud\Storage\StorageClient;

class ControllerReport extends Controller
{
    public function index(Request $request)
    {
        return Report::get();
    }

    public function FormReport()
    {

        $report = Report::orderBy('created_at', 'DESC')
            ->get();
        // return $transaksisaldo; // untuk trace "HASIL JSON"
        // ->whereHas('user', function ($query){
        //   $query->status=1
        // $transaksisaldo = transaksisaldo::join('user', 'user.id' , '=', 'transaksisaldo.user_id')
        //                   ->select('user.nama', 'transaksisaldo.nominal', 'transaksisaldo.tanggaltransaksi')
        //                   ->where('transaksisaldo.tipetransaksi','TOPUP')
        //                   ->get();
        return view('menus.report.DataReport ', compact('report'));
    }

    public function ActionReportBlockUser(Request $request, $id)
    {

        $report = Report::find($id);
        $report->statusreport = 1;
        $report->save();

        $reportbarbershop = Report::Where('judul', '=', 'Customer Report Barbershop')->get();
        if ($reportbarbershop) {
            $user = User::find($report->user_merchant_id);
            $user->statusjalan = 0;
            $user->save();
        } else {
            $user = User::find($report->user_customer_id);
            $user->statusjalan = 0;
            $user->save();
        }

        // $user = User::find($report->user_customer_id);
        // $user->statusjalan = 0;
        // $user->save();


        $this->sendNotificationToDevice('Block User', 'Akun anda telah diblokir', $user->firebase_fcm_token);

        $informasi = new Informasi();
        $informasi->judul = 'Block User';
        $informasi->isi = 'Akun anda telah diblokir oleh admin, untuk sementara akun anda tidak dapat melakukan transaksi';
        $informasi->user_customer_id = $user->id;
        $informasi->save();

        return redirect('FormReport');
        // ->with('success', 'Blocked user successfully')
    }
}
