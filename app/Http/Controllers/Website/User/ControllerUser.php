<?php

namespace App\Http\Controllers\Website\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use Auth;

class ControllerUser extends Controller
{

  public function index(Request $request)
  {
    return User::get();

    // return "abx";
    // $encrypted = Crypt::encrypt('secret');
    // return $encrypted;
    // return Hash::make("123");
    // $decrypted = Crypt::decrypt("eyJpdiI6IlppY05HY1F0ZzE4MnRRcEZOT0tIOGc9PSIsInZhbHVlIjoiaTBcL2x5aVIyOXg5KzBwckFRVU1mVUE9PSIsIm1hYyI6ImM1ZjcyZWM5Mzc2OTRlNjNmYTQ3MDljZjEzMjY3NDJlYWJkYjJkNzE4YjcwZDE0ZTM4MDZjYzk2NDQyYzdiZTAifQ==");
    // return $decrypted;
  }

  public function FormUser()
  {
    // $user = User::get();
    $user = User::where('jenisuser', '!=', 1)->get();

    return view('menus.user.DataUser ', compact('user'));
  }
  public function AddForm()
  {
    return view('menus.user.AddUser');
  }

  public function EditForm(Request $request)
  {
    $user = User::find($request->segment(2));
    return view('menus.user.EditUser ', compact('user'));
  }

  public function AddData(Request $request)
  {
    // return $request->all();
    $messages = [
      'nama.required' => ':attribute harus diisi.',
      'email.required' => ':attribute harus diisi.',
      'alamat.required' => ':attribute harus diisi.',
    ];
    $validator = Validator::make($request->all(), [
      'nama' => 'required|max:255',
      'email' => 'required',
      'alamat' => 'required|max:255',

    ], $messages);

    if ($validator->fails()) {
      return redirect('AddForm')
        ->withErrors($validator)
        ->withInput();
    }


    $user = new User(); //insert into users() values ()
    $user->nama = $request->input('nama');
    $user->email = $request->input('email');
    // $user->password = Hash::make($request->input('password'));//encrpy password yang tidak bisa di decrpyt
    $user->password = Hash::make("123");
    $user->alamat = $request->input('alamat');
    $user->save();
    return redirect('FormUser')->with('success', 'Post created successfully');
  }

  public function EditData(Request $request)
  {
    // untuk edit
    // return $request->all();
    // if($request->has('id'))
    //   {
    // $messages = [
    //     'nama.required'    => 'The :attribute and :other must match.',
    //     'email.required'    => ':attribute harus diisi.',
    //    'alamat.required'=> 'The :attribute and :other must match.',
    // ];
    // $validator = Validator::make($request->all(), [
    //     'nama' => 'required|max:255',
    //     'email' => 'required',
    //     'alamat' => 'required|max:255',
    // ],$messages);
    //
    // if ($validator->fails()) {
    //     return redirect('EditForm')
    //                 ->withErrors($validator)
    //                 ->withInput();
    // }
    $user = User::find($request->input('id')); //insert into users() values ()
    $user->nama = $request->input('nama');
    $user->email = $request->input('email');
    // $user->password = Hash::make($request->input('password'));
    $user->alamat = $request->input('alamat');
    $user->save();
    return redirect('FormUser');
    // }
  }

  public function DeleteData($id)
  {
    User::find($id)->delete();
    return redirect('FormUser');
  }
}
