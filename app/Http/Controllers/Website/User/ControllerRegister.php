<?php

namespace App\Http\Controllers\Website\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class ControllerRegister extends Controller
{
    public function FormRegister()
    {
        $user = User::get();
        return view('login.register', compact('user'));
    }

    public function AddDataRegister(Request $request)
    {
        $messages = [
         'nama.required'=> ':attribute harus diisi.',
         'email.required'=> ':attribute harus diisi.',
         'password.required'=> ':attribute harus diisi.',
         'repassword.required'=> ':attribute harus sama dengan password.',
     ];
        $validator = Validator::make($request->all(), [
         'nama' => 'required|max:255',
         'email' => 'required|email',
         'password' => 'required|min:3',
         'repassword' => 'required_with:password|same:password|min:3',

     ], $messages);

        if ($validator->fails()) {
            return redirect('FormRegister')
                     ->withErrors($validator)
                     ->withInput();
        }

        $user = new User(); //insert into users() values ()
        $user->nama = $request->input('nama');
        $user->email = $request->input('email');
        $user->jenisuser = 1;
        $user->statusverified = 0;
        $user->statusjalan = 0;
        $user->password = Hash::make($request->input('password'));
        $user->api_token = str_random(60);
        $user->save();
        return redirect('FormRegister')->with('success', 'Post created successfully');
    }
}
