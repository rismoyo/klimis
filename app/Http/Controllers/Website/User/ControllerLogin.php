<?php

namespace App\Http\Controllers\Website\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use App\Models\Service;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;

class ControllerLogin extends Controller
{
    public function FormLogin()
    {
        return view('login.login');
    }

    public function DataLogin(Request $request)
    {
        $messages = [
            'email.required' => ':attribute harus diisi.',
            'password.required' => ':attribute harus diisi.',
        ];
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:3',

        ], $messages);

        if ($validator->fails()) {
            return redirect('FormLogin')
                ->withErrors($validator)
                ->withInput();
        }
        // , 'statusverified'=>1, 'statusjalan'=>1
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password'), 'jenisuser' => 1])) {
            // Authentication passed...
            return redirect('FormHome');
            // Alert::success('pesan yang ingin disampaikan', 'Judul Pesan');
            // alert()->success('You have been logged out.', 'Good bye!');
        }

        return redirect('/');
    }

    public function logout()
    {
        Auth::logout();
        alert()->success('You have been logged out.', 'Good bye!');
        // Authentication passed...
        return redirect('/');
    }

    public function cobalogin()
    {
        //   $user = new User(); //insert into users() values ()
        //   //$user->nama = $request->input('nama');
        //   $user->nama = 'admin2';
        //   $user->email = 'admin2@gmail.com';
        //   $user->jenisuser = 1;
        //   $user->statusverified = 0;
        //   $user->statusjalan = 0;
        //   // $user->password = Hash::make($request->input('password'));//encrpy password yang tidak bisa di decrpyt
        //   $user->password = Hash::make("admin2");
        //   $user->api_token = str_random(60);
        // //  $user->alamat = $request->input('alamat');
        //   $user->save();
        //
        // $user = new User();
        // $user->email = 'metro@gmail.com';
        // $user->nama = 'metro';
        // $user->jenisuser = '2';
        // $user->password = Hash::make("metro");
        // $user->api_token = str_random(60);
        // $user->save();

        // $service = new Service();
        // $service->namaservice = 'haircut';
        // $service->tarif = '10000';
        // $service->keterangan = 'potong rambut biasa';
        // $service->user_id = 1;
        // $service->save();
    }
}
