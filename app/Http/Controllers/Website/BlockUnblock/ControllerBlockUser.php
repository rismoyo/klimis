<?php

namespace App\Http\Controllers\Website\BlockUnblock;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use App\Models\Informasi;
use Validator;
use Illuminate\Http\Request;
use Auth;

class ControllerBlockUser extends Controller
{
    public function index(Request $request)
    {
        return User::get();
    }

    public function FormBlockUser()
    {
        $user = User::where('jenisuser', '!=', 1)->get();
        return view('menus.blockunblock.DataBlockUser', compact('user'));
    }

    public function BlockUser(Request $request, $id)
    {
        $user = User::find($id);
        $user->statusjalan = 0;
        $user->save();

        $this->sendNotificationToDevice('Block User', 'Akun anda telah diblokir', $user->firebase_fcm_token);

        $informasi = new Informasi();
        $informasi->judul = 'Block User';
        $informasi->isi = 'Akun anda telah diblokir oleh admin, untuk sementara akun anda tidak dapat melakukan transaksi';
        $informasi->user_customer_id = $user->id;
        $informasi->save();
        // }, 5);


        return redirect('FormBlockUser')->with('success', 'Blocked user successfully');
    }

    public function UnblockUser(Request $request, $id)
    {
        $user = User::find($id);
        $user->statusjalan = 1;
        $user->save();

        $this->sendNotificationToDevice('Unblock User', 'Akun anda telah di unblock', $user->firebase_fcm_token);

        $informasi = new Informasi();
        $informasi->judul = 'Unblock User';
        $informasi->isi = 'Akun anda telah di unblokir oleh admin, sudah dapat digunakan untuk melakukan transaksi kembali';
        $informasi->user_customer_id = $user->id;
        $informasi->save();

        return redirect('FormBlockUser')->with('success', 'Unblocked user successfully');
    }

    //     public function AddForm (){
    //       return view('menus.user.AddUser');
    //     }
    //
    //     public function EditForm (Request $request){
    //         $user = User::find ($request->segment(2));
    //         return view('menus.user.EditUser ',compact('user'));
    //     }
    //
    //     public function AddData (Request $request){
    //    // return $request->all();
    //      $messages = [
    //          'nama.required'=> ':attribute harus diisi.',
    //          'email.required'=> ':attribute harus diisi.',
    //          'alamat.required'=> ':attribute harus diisi.',
    //      ];
    //      $validator = Validator::make($request->all(), [
    //          'nama' => 'required|max:255',
    //          'email' => 'required',
    //          'alamat' => 'required|max:255',
    //
    //      ],$messages);
    //
    //      if ($validator->fails()) {
    //          return redirect('AddForm')
    //                      ->withErrors($validator)
    //                      ->withInput();
    //      }
    //
    //      $user = new User(); //insert into users() values ()
    //      $user->nama = $request->input('nama');
    //      $user->email = $request->input('email');
    //      // $user->password = Hash::make($request->input('password'));//encrpy password yang tidak bisa di decrpyt
    //      $user->password = Hash::make("123");
    //      $user->alamat = $request->input('alamat');
    //      $user->save();
    //       return redirect('FormUser')->with('success','Post created successfully');
    //  }
    //
    //  public function EditData(Request $request){
    //  // untuk edit
    // // return $request->all();
    //  // if($request->has('id'))
    //  //   {
    //      // $messages = [
    //      //     'nama.required'    => 'The :attribute and :other must match.',
    //      //     'email.required'    => ':attribute harus diisi.',
    //      //    'alamat.required'=> 'The :attribute and :other must match.',
    //      // ];
    //      // $validator = Validator::make($request->all(), [
    //      //     'nama' => 'required|max:255',
    //      //     'email' => 'required',
    //      //     'alamat' => 'required|max:255',
    //      // ],$messages);
    //      //
    //      // if ($validator->fails()) {
    //      //     return redirect('EditForm')
    //      //                 ->withErrors($validator)
    //      //                 ->withInput();
    //      // }
    //      $user = User::find($request->input('id')); //insert into users() values ()
    //      $user->nama = $request->input('nama');
    //      $user->email = $request->input('email');
    //      // $user->password = Hash::make($request->input('password'));
    //     $user->alamat = $request->input('alamat');
    //      $user->save();
    //      return redirect('FormUser');
    //     // }
    //  }

    public function DeleteData($id)
    {
        User::find($id)->delete();
        return redirect('FormUser');
    }
}
