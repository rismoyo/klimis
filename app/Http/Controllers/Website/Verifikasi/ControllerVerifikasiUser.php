<?php

namespace App\Http\Controllers\Website\Verifikasi;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use App\Models\Informasi;
use Validator;
use Illuminate\Http\Request;
use Auth;

class ControllerVerifikasiUser extends Controller
{

  public function index()
  {
    return User::get();
  }

  // *********** untuk menampilakan data  user yang ingin membuka barbershop ***************
  public function FormVerifikasiUser()
  {
    // where('statusverified', 0)->where('pinalamat', '!=', '')->where('telepon', '!=', '')where('jenisuser', '=', 2)
    $user = User::where('jenisuser', '!=', '1')->whereNotNull('pinalamat')->get();
    return view('menus.verifikasi.DataVerifikasiUser', compact('user'));
  }

  // *********** untuk menverifikasi user yang ingin membuka barbershop ***************
  public function VerifikasiUser(Request $request, $id)
  {
    $user = User::find($id);
    $user->statusverified = 1;
    $user->jenisuser = 3;
    $user->save();

    $this->sendNotificationToDevice('Verifikasi Barbershop', 'Anda sudah terdaftar sebagai Merchant barbershop', $user->firebase_fcm_token);
    $informasi = new Informasi();
    $informasi->judul = 'Verifikasi Barbershop';
    $informasi->isi = 'Anda sudah terdaftar sebagai merchant barbershop dan jangan lupa untuk mengisi dan mengatur jadwal barbershop, stylist, service dll';
    $informasi->user_customer_id = $user->id;
    $informasi->save();
    return redirect('FormVerifikasiUser')->with('success', 'Verification user successfully');
  }
}
