<?php

namespace App\Http\Controllers\Website\Home;

use App\Http\Controllers\Controller;
use App\Models\Transaksisaldo;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use App\Models\Report;
use Validator;
use Illuminate\Http\Request;
use Auth;

class ControllerHome extends Controller
{
  public function FormHome()
  {
    // return User::get();
    // $jumlahUser = User::count();
    $belumVerifikasi = User::where('statusverified', 0)->where('jenisuser', '=', 3)->count();
    $statusReport = Report::where('statusreport', 0)->count();
    $blockUser = User::where('statusjalan', 0)->count();
    $topupUser = Transaksisaldo::where('tipetransaksi', 'TOPUP')->where('statusverifikasi', 0)->count();
    $withdrawUser = Transaksisaldo::where('tipetransaksi', 'WITHDRAW')->where('statusverifikasi', 0)->count();
    return view('menus.home', compact('belumVerifikasi', 'statusReport', 'blockUser', 'topupUser', 'withdrawUser'));
  }
}
