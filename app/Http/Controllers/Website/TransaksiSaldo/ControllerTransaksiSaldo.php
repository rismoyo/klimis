<?php

namespace App\Http\Controllers\Website\TransaksiSaldo;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use App\Models\Transaksisaldo;
use App\Models\User;
use App\Models\Informasi;
use Validator;
use Illuminate\Http\Request;
// use Auth;
use Google\Cloud\Storage\StorageClient;

class ControllerTransaksiSaldo extends Controller
{
    public function index(Request $request)
    {
        return Transaksisaldo::get();

        // return "abx";
        // $encrypted = Crypt::encrypt('secret');
        // return $encrypted;
        // return Hash::make("123");
        // $decrypted = Crypt::decrypt("eyJpdiI6IlppY05HY1F0ZzE4MnRRcEZOT0tIOGc9PSIsInZhbHVlIjoiaTBcL2x5aVIyOXg5KzBwckFRVU1mVUE9PSIsIm1hYyI6ImM1ZjcyZWM5Mzc2OTRlNjNmYTQ3MDljZjEzMjY3NDJlYWJkYjJkNzE4YjcwZDE0ZTM4MDZjYzk2NDQyYzdiZTAifQ==");
        // return $decrypted;
    }

    public function FormTopup()
    {

        $transaksisaldo = Transaksisaldo::with(['user'])
            ->where('tipetransaksi', 'TOPUP')
            ->orderBy('created_at', 'DESC')
            // ->where('statusverifikasi', 0)

            // ->orderBy('statusverifikasi', 'ASC')
            ->get();
        // return $transaksisaldo; // untuk trace "HASIL JSON"
        // ->whereHas('user', function ($query){
        //   $query->status=1
        // $transaksisaldo = transaksisaldo::join('user', 'user.id' , '=', 'transaksisaldo.user_id')
        //                   ->select('user.nama', 'transaksisaldo.nominal', 'transaksisaldo.tanggaltransaksi')
        //                   ->where('transaksisaldo.tipetransaksi','TOPUP')
        //                   ->get();
        return view('menus.topup.DataVerifikasiTopup ', compact('transaksisaldo'));
    }


    // public function DetailBuktiTransfer(Request $request, $id)
    // {
    //     $transaksisaldo = Transaksisaldo::with(['user'])
    //         ->select('urlbuktibayar')
    //         ->where('id', '=', $id)
    //         ->where('tipetransaksi', 'TOPUP')
    //         // ->where('statusverifikasi', 0)
    //         ->orderBy('created_at', 'desc')
    //         // ->orderBy('statusverifikasi', 'ASC')
    //         ->get();
    //     // return $transaksisaldo; // untuk trace "HASIL JSON"
    //     // ->whereHas('user', function ($query){
    //     //   $query->status=1
    //     // $transaksisaldo = transaksisaldo::join('user', 'user.id' , '=', 'transaksisaldo.user_id')
    //     //                   ->select('user.nama', 'transaksisaldo.nominal', 'transaksisaldo.tanggaltransaksi')
    //     //                   ->where('transaksisaldo.tipetransaksi','TOPUP')
    //     //                   ->get();
    //     return view('menus.topup.DataVerifikasiTopup ', compact('transaksisaldo'));
    // }

    public function VerifikasiTopupUser(Request $request, $id)
    {

        // $this->sendNotificationToDevice('abcd', 'erty', 'dZuZw3x69kk:APA91bEppqsHwjQJiqjNQcguB2bIuHSM1qmJsgT3y4_BYxwdQS2GtmBacD1ZOhAL3pUq_6HJqduWEjF514pnbLyLILtU8SwLgQk0eAScVwqPeI5sA-VUu_YxiFaAlYxkCUl9he8diMhv');

        $transaksisaldo = Transaksisaldo::find($id);
        $transaksisaldo->statusverifikasi = 1;
        $transaksisaldo->save();

        // return $transaksisaldo;
        //menambah saldo yang sudah di topup

        // $titletext = 'Topup';
        // $bodytext = 'Anda Berhasil Topup';
        // $fcmtokenuser = User::select('firebase_fcm_token')->where('id', '=', Auth::id())->get();
        $user = User::find($transaksisaldo->user_id);
        $user->saldo = $user->saldo + $transaksisaldo->nominal;
        $user->save();
        // $this->sendNotificationToDevice($titletext, $bodytext, $fcmtokenuser);

        // return $user;
        // $user = User::find($id);
        $this->sendNotificationToDevice('Transaksi Saldo', 'Anda Berhasil Melakukan Topup', $user->firebase_fcm_token);

        $informasi = new Informasi();
        $informasi->judul = 'Transaksi Saldo';
        $informasi->isi = 'Anda Berhasil Melakukan Topup, saldo anda bertambah sesuai dengan nominal topup';
        $informasi->user_customer_id = $user->id;
        $informasi->save();

        return redirect('FormTopup')->with('success', 'Topup successfully');
    }


    public function FormWithdraw()
    {
        $transaksisaldo = Transaksisaldo::with(['user'])->where('tipetransaksi', 'WITHDRAW')
            // ->where('statusverifikasi', 0)
            ->orderBy('created_at', 'desc')
            // ->orderBy('statusverifikasi', 'ASC')
            ->get();
        // return $transaksisaldo; // untuk trace "HASIL JSON"
        // ->whereHas('user', function ($query){
        //   $query->status=1
        // $transaksisaldo = transaksisaldo::join('user', 'user.id' , '=', 'transaksisaldo.user_id')
        //                   ->select('user.nama', 'transaksisaldo.nominal', 'transaksisaldo.tanggaltransaksi')
        //                   ->where('transaksisaldo.tipetransaksi','TOPUP')
        //                   ->get();
        return view('menus.withdraw.DataVerifikasiWithdraw ', compact('transaksisaldo'));
    }


    public function VerifikasiWithdrawUser(Request $request, $id)
    {
        $transaksisaldo = Transaksisaldo::find($id);
        $transaksisaldo->statusverifikasi = 1;
        $transaksisaldo->save();

        // return $transaksisaldo;
        //menambah saldo yang sudah di withdraw
        $user = User::find($transaksisaldo->user_id);
        $user->saldo = $user->saldo - $transaksisaldo->nominal;
        $user->save();

        // return $user;

        $this->sendNotificationToDevice('Transaksi Saldo', 'Admin sudah melakukan transfer ke rekening Anda', $user->firebase_fcm_token);

        $informasi = new Informasi();
        $informasi->judul = 'Transaksi Saldo';
        $informasi->isi = 'Anda berhasil melakukan withdraw, Admin sudah melakukan transfer sesuai nominal withdraw ke rekening Anda';
        $informasi->user_customer_id = $user->id;
        $informasi->save();

        return redirect('FormWithdraw')->with('success', 'Withdraw successfully');
    }
}
