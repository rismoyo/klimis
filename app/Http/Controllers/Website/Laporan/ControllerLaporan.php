<?php

namespace App\Http\Controllers\Website\laporan;

use App\Charts\MyChart;
use App\Http\Controllers\Controller;
use App\Models\Transaksisaldo;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class ControllerLaporan extends Controller
{
    public function FormLaporan()
    {
        return view('menus.laporan.DataLaporan');
    }

    public function index(Request $request)
    {
        return User::get();
    }

    public function FormBarberAktif()
    {
        $user = User::where('jenisuser', '=', 3)->get();
        return view('menus.laporan.DataBarbershopAktif', compact('user'));
    }

    public function FormVerifikasi()
    {
        $user = User::where('jenisuser', '=', 3)->get();
        return view('menus.laporan.DataVerifikasiBarbershop', compact('user'));
    }

    public function FormPembelianSaldo()
    {
        $transaksisaldo = Transaksisaldo::where('tipetransaksi', '=', 'TOPUP')->get();
        return view('menus.laporan.DataPembelianSaldo', compact('transaksisaldo'));
    }

    public function FormPenarikanTunai()
    {
        // return $this->ChartDanaBarber();
        $transaksisaldo = Transaksisaldo::where('tipetransaksi', '=', 'WITHDRAW')->get();
        return view('menus.laporan.DataPenarikanTunai', compact('transaksisaldo'));
    }

    // public function ChartDanaBarber()
    // {
    //     $data = User::get();
    //     $collection = collect($data);
    //     $labels = $collection->pluck('nama');
    //     $datasets = $collection->pluck('saldo');
    //     $chart = new MyChart();
    //     $chart->labels($labels->all());
    //     $chart->dataset('Saldo', 'bar', $datasets)->options([
    //         'backgroundColor' => '#ff0000',
    //     ]);;
    //     //        $chart->dataset('My dataset 2', 'bar', [4, 3, 2, 1]);
    //     return view('menus.laporan.ChartDanaBarber', compact('chart'));
    // }

    public function ChartTopupTerbanyak()
    {
        $data = DB::select("select us.nama as nama , SUM(tr.nominal) as sum
        from transaksisaldo as tr, user as us
        where tr.user_id = us.id  AND tr.tipetransaksi = 'TOPUP' AND tr.statusverifikasi = 1 
        group by us.nama ");
        $collection = collect($data);
        $labels = $collection->pluck('nama');
        $datasets = $collection->pluck('sum');
        $chart = new MyChart();
        $chart->labels($labels->all());
        $chart->dataset('Saldo', 'bar', $datasets)->options([
            'backgroundColor' => '#ff0000',
        ]);;
        return view('menus.laporan.ChartTopupTerbanyak', compact('chart'));
    }

    public function ChartwithdrawTerbanyak()
    {
        $data = DB::select("select us.nama as nama , SUM(tr.nominal) as sum
        from transaksisaldo as tr, user as us
        where tr.user_id = us.id  AND tr.tipetransaksi = 'WITHDRAW' AND tr.statusverifikasi = 1
        group by us.nama  ");
        $collection = collect($data);
        $labels = $collection->pluck('nama');
        $datasets = $collection->pluck('sum');
        $chart = new MyChart();
        $chart->labels($labels->all());
        $chart->dataset('Saldo', 'bar', $datasets)->options([
            'backgroundColor' => '#ff0000',
        ]);;
        return view('menus.laporan.ChartwithdrawTerbanyak', compact('chart'));
    }

    public function ChartstylistTerlaris()
    {
        $data = DB::select("select st.namastylist as nama , count(bh.stylist_id) as sum
        from booking_header AS bh, stylist AS st 
        where bh.stylist_id = st.id and bh.status_booking = 3 and bh.deleted_at IS NULL
        group by st.namastylist ");
        $collection = collect($data);
        $labels = $collection->pluck('nama');
        $datasets = $collection->pluck('sum');
        $chart = new MyChart();
        $chart->labels($labels->all());
        $chart->dataset('Total Pelanggan', 'bar', $datasets)->options([
            'backgroundColor' => '#ff0000',
        ]);;
        return view('menus.laporan.ChartDanaBarber', compact('chart'));
    }

    public function ChartbarberTerlaris()
    {
        $data = DB::select("select ST.nama as nama, COUNT(BH.user_merchant_id) as sum
        from booking_header AS BH, user AS ST
        where BH.user_merchant_id = ST.id AND BH.status_booking = 3 AND BH.deleted_at IS NULL AND ST.deleted_at IS NULL
        group by BH.user_merchant_id");
        $collection = collect($data);
        $labels = $collection->pluck('nama');
        $datasets = $collection->pluck('sum');
        $chart = new MyChart();
        $chart->labels($labels->all());
        $chart->dataset('Total Pelanggan', 'bar', $datasets)->options([
            'backgroundColor' => '#ff0000',
        ]);;
        return view('menus.laporan.ChartDanaBarber', compact('chart'));
    }


    public function ChartStybarTerlaris(Request $request) //tidak tahu
    {
        $data = DB::select("select ST.namastylist as nama , COUNT(BH.stylist_id) as sum
        from booking_header AS BH, stylist AS ST,user AS US 
        where BH.user_merchant_id = " . $request->input('idusermerchant') . " and BH.stylist_id = ST.id and BH.status_booking = 3 and BH.deleted_at IS NULL
        group by BH.stylist_id, US.nama")->get();
        $collection = collect($data);
        $labels = $collection->pluck('nama');
        $datasets = $collection->pluck('sum');
        $chart = new MyChart();
        $chart->labels($labels->all());
        $chart->dataset('Total Pelanggan', 'bar', $datasets)->options([
            'backgroundColor' => '#ff0000',
        ]);;
        return view('menus.laporan.ChartDanaBarber', compact('chart'));
    }

    public function uscustaktif()
    {
        $data = DB::select("count(us.id) as sum
        from user as us ,booking_header as bh
        where  us.id = bh.user_customer_id AND us.deleted_at IS NULL AND bh.deleted_at IS NULL
        group by us.id")->get();
        $collection = count($data);
        return $collection;
    }
    public function usmercaktif()
    {
        $data = DB::select("count(us.id) as sum
        from user as us ,booking_header as bh
        where  us.id = bh.user_merchant_id AND us.deleted_at IS NULL AND bh.deleted_at IS NULL
        group by us.id")->get();
        $collection = count($data);
        return $collection;
    }

    public function usercust()
    {
        $data = DB::select("count(*) as sum
        from user 
        where  statusjalan=1 AND jenisuser=2 AND deleted_at IS NULL
        ")->first();
        return $data;
    }

    public function usermercregcomp()
    {
        $data = DB::select("count(*) as sum
        from user 
        where  statusjalan=1 AND jenisuser=3 AND statusverified=1 AND deleted_at IS NULL
        ")->first();
        return $data;
    }

    public function usermercregpend()
    {
        $data = DB::select("count(*) as sum
        from user 
        where  statusjalan=1 AND jenisuser=3 AND statusverified=0 AND deleted_at IS NULL
        ")->first();
        return $data;
    }

    public function userblocked()
    {
        $data = DB::select("count(*) as sum
        from user 
        where  statusjalan=0 AND deleted_at IS NULL
        ")->first();
        return $data;
    }


    // public function Form()
    // {
    //   $transaksisaldo = Transaksisaldo::where('tipetransaksi', '=', 'WITHDRAW')->get();
    //   return view('menus.laporan.DataPenarikanTunai', compact('transaksisaldo'));
    // }


    // public function BlockUser(Request $request, $id)
    // {
    //   $user = User::find($id);
    //   $user->statusjalan = 0;
    //   $user->save();

    //   $this->sendNotificationToDevice('Block User', 'Akun anda telah diblokir', $user->firebase_fcm_token);

    //   $informasi = new Informasi();
    //   $informasi->judul = 'Block User';
    //   $informasi->isi = 'Akun anda telah diblokir oleh admin, untuk sementara akun anda tidak dapat melakukan transaksi';
    //   $informasi->user_customer_id = $user->id;
    //   $informasi->save();
    //   // }, 5);


    //   return redirect('FormBlockUser')->with('success', 'Blocked user successfully');
    // }

    // public function UnblockUser(Request $request, $id)
    // {
    //   $user = User::find($id);
    //   $user->statusjalan = 1;
    //   $user->save();

    //   $this->sendNotificationToDevice('Unblock User', 'Akun anda telah di unblock', $user->firebase_fcm_token);

    //   $informasi = new Informasi();
    //   $informasi->judul = 'Unblock User';
    //   $informasi->isi = 'Akun anda telah di unblokir oleh admin, sudah dapat digunakan untuk melakukan transaksi kembali';
    //   $informasi->user_customer_id = $user->id;
    //   $informasi->save();

    //   return redirect('FormBlockUser')->with('success', 'Unblocked user successfully');
    // }


    // public function DeleteData($id)
    // {
    //   User::find($id)->delete();
    //   return redirect('FormUser');
    // }
}
