<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Carbon\Carbon;
use Kreait\Firebase;
use FirebaseServiceProvider;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;
use Kreait\Firebase\Exception\Messaging\NotFound;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendNotificationToDevice($title, $body, $deviceToken, $data = [], $click_action = '')
    {
        Log::info('sendNotificationToDevice : Title = ' . $title . ", Body = " . $body . ", Device = " . $deviceToken . " Data = " . json_encode($data) . " Action = " . $click_action);
        $isSuccess = false;
        try {
            $firebase = app('firebase');
            $messaging = $firebase->getMessaging();
            // $messaging = (new Firebase\Factory())->createMessaging();
            // $notification = Notification::create()->withTitle($title)->withBody($body);
            $message = CloudMessage::withTarget('token', $deviceToken)->withNotification(Notification::create($title, $body));
            // $firebase->getMessaging()->validate($message);

            if ($click_action <> '') {
                $data['click_action'] = (string) $click_action;
            }
            if ($data != []) {
                $message = $message->withData($data);
            }
            $messaging->send($message);
            $isSuccess = true;
        } catch (InvalidMessage $e) {
            $isSuccess = false;
        } catch (NotFound $e) {
            $isSuccess = false;
        }
        return $isSuccess;
    }

    public function setSuccessResponse($data = [], $input = [], $code = 200, $message = 'OK')
    {
        return [
            'data' => $data,
            'input' => $input,
            'status' => [
                'code' => $code,
                'message' => $message,
            ],
        ];
    }
    public function setErrorResponse($error = [], $input = [], $code = 502, $message = 'Oops theres and error')
    {
        return [
            'error' => $error,
            'input' => $input,
            'status' => [
                'code' => $code,
                'message' => $message,
            ],
        ];
    }
    public function generateCode($code, Carbon $date, $counter)
    {
        $morph_counter = sprintf('%05d', $counter);
        return $code . '/' . $date->format('Y/md') . '/' . $morph_counter;
    }
    public function random_color_part()
    {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }
    public function random_color()
    {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
    public function createLogInfo($logs)
    {
        Log::info($logs);
    }
}
