<?php

namespace App\Http\Controllers\Android;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\Promo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;

class ControllerPromoA extends Controller
{
    // input data promo
    public function addpromo(Request $request)
    {
        $messages = [
      'namapromo.required'    => ':attribute perlu diisi.',
      'diskon.required'    => ':attribute perlu diisi.',
      'waktuawal.required'    => ':attribute perlu diisi.',
      'waktuakhir.required'    => ':attribute perlu diisi.',
    ];
        $validator = Validator::make($request->all(), [
        'namapromo' => 'required|',
        'diskon' => 'required|',
        'waktuawal' => 'required|',
        'waktuakhir' => 'required|',
    ], $messages);

        if ($validator->fails()) {
            $error = ["message"=>"terjadi kesalahan","errors"=>$validator->errors()];
            return response()->json($error, 401);
        }

        $waktuawal = Carbon::createFromTimestampMs($request->input('waktuawal'))->toDateTimeString();
        $waktuakhir = Carbon::createFromTimestampMs($request->input('waktuakhir'))->toDateTimeString();

        $promo = new Promo();
        $promo->namapromo = $request->input('namapromo');
        $promo->diskon  = $request->input('diskon');
        $promo->waktuawal = $waktuawal;
        $promo->waktuakhir = $waktuakhir;
        $promo->save();
        return response()->json(['data' => $promo], 200, [], JSON_NUMERIC_CHECK);
    }


    public function getlistpromo(Request $request)
    {
        // mengambil list data promo barbershop berdasarkan login user
        $promo= Promo::where('user_id', '=', Auth::id())->get();
        return response()->json(['data' => $promo], 200, [], JSON_NUMERIC_CHECK);
    }


    public function getdatapromo(Request $request)
    {
        $promo = Promo::find($request->id);
        return response()->json(['data' => $promo], 200, [], JSON_NUMERIC_CHECK);
    }


    // public function getdatauserpromo(Request $request)
    // {
    //     $transaksisaldo= Transaksisaldo::find($id);
    //     $transaksisaldo->statusverifikasi=1;
    //     $transaksisaldo->save();
    //
    //     // return $transaksisaldo;
    //     //menambah saldo yang sudah di topup
    //     $user = User::find($transaksisaldo->user_id);
    //     $user->saldo = $user->saldo + $transaksisaldo->nominal;
    //     $user->save();
    //
    //
    //     $promo = Promo::find($request->user_id);
    //     return response()->json(['data' => $promo], 200, [], JSON_NUMERIC_CHECK);
    // }

    public function deletepromo(Request $request)
    {
        $promo = Promo::find($request->id);
        $promo->delete();
        return response()->json(['data' => $promo], 200, [], JSON_NUMERIC_CHECK);
    }


    public function editpromo(Request $request)
    {
        $namapromo = $request->namapromo;
        $diskon  = $request->diskon;
        $waktuawal = $request->waktuawal;
        $waktuakhir = $request->waktuakhir;

        $waktuawal = Carbon::createFromTimestampMs($request->input('waktuawal'))->toDateTimeString();
        $waktuakhir = Carbon::createFromTimestampMs($request->input('waktuakhir'))->toDateTimeString();

        $promo = Promo::find($request->id);
        $promo ->namapromo =$namapromo;
        $promo->diskon  = $diskon;
        $promo->waktuawal = $waktuawal;
        $promo->waktuakhir = $waktuakhir;
        $promo->save();

        return response()->json(['data' => $promo], 200, [], JSON_NUMERIC_CHECK);
    }
}
