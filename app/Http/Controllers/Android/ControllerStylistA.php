<?php

namespace App\Http\Controllers\Android;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\Stylist;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;

class ControllerStylistA extends Controller
{
    public function addstylist(Request $request)
    {
        $messages = [
      'namastylist.required'    => ':attribute perlu diisi.',
    ];
        $validator = Validator::make($request->all(), [
        'namastylist' => 'required|',
    ], $messages);

        if ($validator->fails()) {
            $error = ["message"=>"terjadi kesalahan","errors"=>$validator->errors()];
            return response()->json($error, 401);
        }

        $stylist = new Stylist();
        $stylist->namastylist = $request->input('namastylist');
        $stylist->urlgambar = $request->input('urlgambar');
        $stylist->statuskerja = 1;
        $stylist->user_id = $request->user()->id;
        $stylist->save();
        return response()->json(['data' => $stylist], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getliststylistmerchant(Request $request)
    {
        $stylist = Stylist::where('user_id','=', Auth::id())->get();
        return response()->json(['data' => $stylist], 200, [], JSON_NUMERIC_CHECK);
    }


    public function getliststylistcustomer(Request $request)
    {
        $stylist = Stylist::get();
        return response()->json(['data' => $stylist], 200, [], JSON_NUMERIC_CHECK);
    }


    public function getdatastylist(Request $request)
    {
        $stylist = Stylist::find($request->id);
        return response()->json(['data' => $stylist], 200, [], JSON_NUMERIC_CHECK);
    }

    public function deletestylist(Request $request)
    {

        $stylist = Stylist::find($request->id);
        $stylist->delete();
        return response()->json(['data' => $stylist], 200, [], JSON_NUMERIC_CHECK);
    }


    public function editstylist(Request $request)
    {
        $namastylist = $request->namastylist;
        $urlgambar = $request->urlgambar;

        $stylist = Stylist::find($request->id);
        $stylist ->namastylist = $namastylist;

        if ($urlgambar){
            $stylist ->urlgambar = $urlgambar;
        }

        $stylist->save();
        return response()->json(['data' => $stylist], 200, [], JSON_NUMERIC_CHECK);
    }
}
