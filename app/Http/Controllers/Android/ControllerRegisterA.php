<?php

namespace App\Http\Controllers\Android;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;

class ControllerRegisterA extends Controller
{
    public function daftaruser(Request $request)
    {
        $messages = [
        'nama.required'    => ':attribute perlu diisi.',
        'nama.min'    => ':attribute minimal 3 characther.',
        'email.required'    => ':attribute perlu diisi.',
        'password.required'    => ':attribute perlu disi.',
        'password.min'    => ':attribute minimal 3 characther.',
        // 'password_con.required'    => ':attribute perlu diisi',
        // // 'password_confirm.required'    => ':attribute perlu diisi',
        // 'password_confirmation.same'    => ':attribute harus sama',
        // 'password_confirmation.min'    => ':attribute minimal 3 characther',
        // 'password_confirmation.required'    => ':attribute perlu diisi.',
        'telepon.required'    => ':attribute perlu diisi.',
        'alamat.required'    => ':attribute perlu diisi.',
      ];
        $validator = Validator::make($request->all(), [
          'nama' => 'required|min:3',
          'email' => 'required|email',
          'password' => 'required|min:3',
          // 'password_confirmation' => 'required|min:3|max:25|same:password',
            // 'konfirmasi_password' => 'required|min:3|max:25',
          // 'konfirmasi_password' => 'required_with:password|same:password',
          // 'konfirmasi_password' => 'required|min:3|max:25|required_with:password',

          'telepon' => 'required|numeric',
          'alamat' => 'required|min:5',

      ], $messages);

        if ($validator->fails()) {
            $error = ["message"=>"terjadi kesalahan","errors"=>$validator->errors()];
            return response()->json($error, 401);
        }

        $user = new User();
        $user->nama = $request->input('nama');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->telepon = $request->input('telepon');
        $user->alamat = $request->input('alamat');
        $user->statusjalan = 0;
        $user->statusverified = 0;
        $user->jenisuser = $request->input('jenisuser');
        $user->api_token = str_random(60);
        $user->save();
        return response()->json(['data' => $user], 200, [], JSON_NUMERIC_CHECK);
    }
}
