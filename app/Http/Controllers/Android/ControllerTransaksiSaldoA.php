<?php

namespace App\Http\Controllers\Android;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\Transaksisaldo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;

class ControllerTransaksisaldoA extends Controller
{
    // input data transaksi
    public function addtopupsaldo(Request $request)
    {
        $messages = [
      'nominal.required'    => ':attribute perlu diisi.',
      'rekening.required'    => ':attribute perlu diisi.',
      'urlbuktibayar.required'    => ':attribute perlu diisi.',
    ];
        $validator = Validator::make($request->all(), [
        'nominal' => 'required|',
        'rekening' => 'required|',
        'urlbuktibayar' => 'required|',
    ], $messages);

        if ($validator->fails()) {
            $error = ["message"=>"terjadi kesalahan","errors"=>$validator->errors()];
            return response()->json($error, 401);
        }

        // $tanggaltransaksi = Carbon::createFromTimestampMs($request->input('tanggaltransaksi'))->toDateTimeString();

        $transaksisaldo = new Transaksisaldo();
        $transaksisaldo->tipetransaksi ="TOPUP";
        $transaksisaldo->nominal  = $request->input('nominal');
        $transaksisaldo->rekening = $request->input('rekening');
        $transaksisaldo->statusverifikasi = 0;
        $transaksisaldo->urlbuktibayar = $request->input('urlbuktibayar');
        // $transaksisaldo->tanggaltransaksi = $request->input('tanggaltransaksi');
        $transaksisaldo->user_id = $request->user()->id;
        $transaksisaldo->save();
        return response()->json(['data' => $transaksisaldo], 200, [], JSON_NUMERIC_CHECK);
    }

    public function addwithdrawsaldo(Request $request)
    {
        $messages = [
      'nominal.required'    => ':attribute perlu diisi.',
    ];
        $validator = Validator::make($request->all(), [
        'nominal' => 'required|',
    ], $messages);

        if ($validator->fails()) {
            $error = ["message"=>"terjadi kesalahan","errors"=>$validator->errors()];
            return response()->json($error, 401);
        }

        $transaksisaldo = new Transaksisaldo();
        $transaksisaldo->tipetransaksi = "WITHDRAW";
        $transaksisaldo->nominal  = $request->input('nominal');
        $transaksisaldo->statusverifikasi = 0;
        $transaksisaldo->user_id = $request->user()->id;
        $transaksisaldo->save();
        return response()->json(['data' => $transaksisaldo], 200, [], JSON_NUMERIC_CHECK);
    }




}
