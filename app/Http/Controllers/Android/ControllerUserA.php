<?php

namespace App\Http\Controllers\Android;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;

class ControllerUserA extends Controller
{
    public function getlistusermerchant(Request $request)
    {
        // biar g bingung list = array , data = 1 object

        // $user = User::get();

        $user = User::where('jenisuser', '=', 3)->get();// data yang dimunculkan dengan jenisuser =3
        return response()->json(['data' => $user], 200, [], JSON_NUMERIC_CHECK);
    }

    // digunakan untuk mengambil data  dan selanjutnya untuk melengkapi data diri user (Verifikasi user)
    public function getdatauser(Request $request)
    {
      // return Auth::id();// get user all, untuk ngetrace
      // return Auth::user();// untuk ngetrace
      // return $request->all();


        // $user = User::select('saldo')->where('id','=',Auth::id())->first();
        $user = Auth::user($request->id);
        return response()->json(['data' => $user], 200, [], JSON_NUMERIC_CHECK);
    }



    public function verifikasiuser(Request $request)
    {
        $messages = [
        'nama.required'    => ':attribute perlu diisi.',
        'nama.min'    => ':attribute minimal 3 characther.',
        'telepon.required'    => ':attribute perlu diisi.',
        'alamat.required'    => ':attribute perlu diisi.',
        'alamat.min'    => ':attribute minimal 5 characther.',
      ];
        $validator = Validator::make($request->all(), [
          'nama' => 'required|min:3',
          'telepon' => 'required|numeric',
          'alamat' => 'required|min:5',

      ], $messages);

        if ($validator->fails()) {
            $error = ["message"=>"terjadi kesalahan","errors"=>$validator->errors()];
            return response()->json($error, 401);
        }

        $user = new User();
        $user->nama = $request->input('nama');
        $user->telepon = $request->input('telepon');
        $user->pinalamat = $request->input('pinalamat');
        $user->pinlatitude = $request->input('pinlatitude');
        $user->pinlongitude = $request->input('pinlongitude');
        $user->save();
        return response()->json(['data' => $user], 200, [], JSON_NUMERIC_CHECK);
    }




    public function edituser(Request $request)
    {
      
        $nama = $request->nama;
        $telepon  = $request->telepon;
        $alamat = $request->alamat;
        $pinalamat = $request->pinalamat;
        $pinlatitude = $request->pinlatitude;
        $pinlongitude = $request->pinlongitude;


        $user = User::find($request->id);
        $user ->nama =$nama;
        $user->telepon =$telepon;
        $user->alamat = $alamat;
        $user->pinalamat = $pinalamat;
        $user->pinlatitude = $pinlatitude;
        $user->pinlongitude = $pinlongitude;
        $user->save();

        return response()->json(['data' => $user], 200, [], JSON_NUMERIC_CHECK);
    }



    // public function getdatauserfor(Request $request)
    // {
    //   // return Auth::id();// get user all, untuk ngetrace
    //   // return Auth::user();// untuk ngetrace
    //   // return $request->all();


    //     // $user = User::select('saldo')->where('id','=',Auth::id())->first();
    //     $user = Auth::user($request->id);
    //     return response()->json(['data' => $user], 200, [], JSON_NUMERIC_CHECK);
    // }
}
