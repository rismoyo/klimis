<?php

namespace App\Http\Controllers\Android;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\Jadwalstylist;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;

class ControllerJadwalStylistA extends Controller
{
    public function addjadwalstylist(Request $request)
    {
        $messages = [
      'jamawalkerja.required'    => ':attribute perlu diisi.',
      'jamakhirkerja.required'    => ':attribute perlu diisi.',
      'harikerja.required'    => ':attribute perlu diisi.',
      'keterangan.required'    => ':attribute perlu diisi.',
    ];
        $validator = Validator::make($request->all(), [
        'jamawalkerja' => 'required|',
        'jamakhirkerja' => 'required|',
        'harikerja' => 'required|',
        'keterangan' => 'required|',
    ], $messages);

        if ($validator->fails()) {
            $error = ["message"=>"terjadi kesalahan","errors"=>$validator->errors()];
            return response()->json($error, 401);
        }

        $jamawalkerja = Carbon::createFromTimestampMs($request->input('jamawalkerja'))->toDateTimeString();
        $jamakhirkerja = Carbon::createFromTimestampMs($request->input('jamakhirkerja'))->toDateTimeString();

        $jadwalstylist = new jadwalstylist();
        $jadwalstylist->jamawalkerja = $jamawalkerja;
        $jadwalstylist->jamakhirkerja = $jamakhirkerja;
        $jadwalstylist->harikerja = $request->input('harikerja');
        $jadwalstylist->keterangan = $request->input('keterangan');
        $jadwalstylist->stylist_id = $request->input('stylist_id');
        $jadwalstylist->save();
        return response()->json(['data' => $jadwalstylist], 200, [], JSON_NUMERIC_CHECK);
    }

    // mengambil list data jadwal stylist berdasarkan login user
    public function getlistjadwalstylist(Request $request)
    {

      // SELECT  js.*
      // FROM user u,  stylist s, jadwalstylist js
      // where u.id = s.user_id  AND  s.id = js.stylist_id AND u.id = 2 ;

        $jadwalstylist = Jadwalstylist::select('jadwalstylist.*')
            ->join('stylist', 'stylist.id', '=', 'jadwalstylist.stylist_id')
            ->join('user', 'user.id', '=', 'stylist.user_id')
            ->get();
        return response()->json(['data' => $jadwalstylist], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getdatajadwalstylist(Request $request)
    {
        $jadwalstylist = Jadwalstylist::find($request->id);
        return response()->json(['data' => $jadwalstylist], 200, [], JSON_NUMERIC_CHECK);
    }

    public function deletejadwalstylist(Request $request)
    {
        $jadwalstylist = Jadwalstylist::find($request->id);
        $jadwalstylist->delete();
        return response()->json(['data' => $jadwalstylist], 200, [], JSON_NUMERIC_CHECK);
    }


    public function editjadwalstylist(Request $request)
    {
        $jamawalkerja = $request->jamawalkerja;
        $jamakhirkerja  = $request->jamakhirkerja;
        $harikerja = $request->harikerja;
        $keterangan = $request->keterangan;
        $stylist_id = $request->stylist_id;

        $jamawalkerja = Carbon::createFromTimestampMs($request->input('jamawalkerja'))->toDateTimeString();
        $jamakhirkerja = Carbon::createFromTimestampMs($request->input('jamakhirkerja'))->toDateTimeString();

        $jadwalstylist = Jadwalstylist::find($request->id);
        $jadwalstylist ->jamawalkerja =$jamawalkerja;
        $jadwalstylist->jamakhirkerja  = $jamakhirkerja;
        $jadwalstylist->harikerja = $harikerja;
        $jadwalstylist->keterangan = $keterangan;
        $jadwalstylist->stylist_id = $stylist_id;
        $jadwalstylist->save();

        return response()->json(['data' => $jadwalstylist], 200, [], JSON_NUMERIC_CHECK);
    }
}
