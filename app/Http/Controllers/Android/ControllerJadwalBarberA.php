<?php

namespace App\Http\Controllers\Android;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\Jadwalbarber;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;

class ControllerJadwalBarberA extends Controller
{
    public function addjadwalbarber(Request $request)
    {
        // tidak perlu karena agak berat jika di atur di laravel, lebih cepat diatur langsung di android studio
        $messages = [
      'jambuka.required'    => ':attribute perlu diisi.',
      'jamtutup.required'    => ':attribute perlu diisi.',
      'hari.required'    => ':attribute perlu diisi.',
      'keterangan.required'    => ':attribute perlu diisi.',
    ];
        $validator = Validator::make($request->all(), [
        'jambuka' => 'required|',
        'jamtutup' => 'required|',
        'hari' => 'required|max:45',
        'keterangan' => 'required|',
    ], $messages);

        if ($validator->fails()) {
            $error = ["message"=>"terjadi kesalahan","errors"=>$validator->errors()];
            return response()->json($error, 401);
        }

        $jambuka = Carbon::createFromTimestampMs($request->input('jambuka'))->toDateTimeString();
        $jamtutup = Carbon::createFromTimestampMs($request->input('jamtutup'))->toDateTimeString();

        $jadwalbarber = new Jadwalbarber();
        $jadwalbarber->jambuka = $jambuka;
        $jadwalbarber->jamtutup = $jamtutup;
        $jadwalbarber->hari = $request->input('hari');
        $jadwalbarber->keterangan = $request->input('keterangan');
        $jadwalbarber->user_id = $request->user()->id;
        $jadwalbarber->save();
        return response()->json(['data' => $jadwalbarber], 200, [], JSON_NUMERIC_CHECK);
    }

    // mengambil list data jadwal barbershop  berdasarkan login user
    public function getlistjadwalbarber(Request $request)
    {
        // $jadwalbarber = Jadwalbarber::get();
        $jadwalbarber = Jadwalbarber::where('user_id','=', Auth::id())->get();
        return response()->json(['data' => $jadwalbarber], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getdatajadwalbarber(Request $request)
    {
        $jadwalbarber = Jadwalbarber::find($request->id);
        return response()->json(['data' => $jadwalbarber], 200, [], JSON_NUMERIC_CHECK);
    }

    public function deletejadwalbarber(Request $request)
    {
        $jadwalbarber = Jadwalbarber::find($request->id);
        $jadwalbarber->delete();
        return response()->json(['data' => $jadwalbarber], 200, [], JSON_NUMERIC_CHECK);
    }


    public function editjadwalbarber(Request $request)
    {
        $jambuka = $request->jambuka;
        $jamtutup  = $request->jamtutup;
        $hari = $request->hari;
        $keterangan = $request->keterangan;

        $jambuka = Carbon::createFromTimestampMs($request->input('jambuka'))->toDateTimeString();
        $jamtutup = Carbon::createFromTimestampMs($request->input('jamtutup'))->toDateTimeString();

        $jadwalbarber = Jadwalbarber::find($request->id);
        $jadwalbarber ->jambuka =$jambuka;
        $jadwalbarber->jamtutup  = $jamtutup;
        $jadwalbarber->hari = $hari;
        $jadwalbarber->keterangan = $keterangan;
        $jadwalbarber->save();

        return response()->json(['data' => $jadwalbarber], 200, [], JSON_NUMERIC_CHECK);
    }
}
