<?php
namespace App\Http\Controllers\Android;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;

class ControllerLoginA extends Controller
{
    public function logingoogle(Request $request)
    {
        //table user tambah field (provier_id), (provider)
        $user = User::where('provider_id', $request->input('provider_id'))->first();
        $message = '';
        $status = '';
        if (!$user) {
            $user = new User();
            $user->nama = '';
            $user->email = '';
            $user->provider_id = $request->input('provider_id');
            $user->provider = 'google';
            //  $user->aktif = '1';
            $user->statusverified = '0';
            $user->api_token = str_random(60);
            $user->save();
        }
        // if (!$user->verified)
        // {
        //   // $message = 'Need Verified';
        //   // $status = '101';
        //   // $accessToken = 'Bearer '.$user->api_token;
        //   $error = ["message"=>"Need Verified"];
        //   return Auth::user();
        // }
        return $user;
        // return response()->json(['data' => $user,'message'=> $message,'status'=> $status,'access_token'=>$accessToken]);
    }



    public function loginuser(Request $request)
    {
        //untuk validasi Android

        $messages = [
        'email.required'    => ':attribute perlu diisi.',
        'password.required'    => ':attribute perlu diisi.',
        'password.min'    => ':attribute minimal 3 character.',
      ];
        $validator = Validator::make($request->all(), [
          'email' => 'required|email',
          'password' => 'required|min:3',

      ], $messages);

        if ($validator->fails()) {
            $error = ["message"=>"terjadi kesalahan","errors"=>$validator->errors()];
            return response()->json($error, 401);
        }

        if (Auth::attempt(['email' =>$request->input('email'), 'password' => $request->input('password'),'statusjalan'=>1,'statusverified'=>1, 'jenisuser'=>1])) {
            $errors = ["message"=>"Masukkan username yang sesuai"];
            return response()->json($errors, 401);
        // return Auth::user();
        }
         elseif (Auth::attempt(['email' =>$request->input('email'), 'password' => $request->input('password'),'statusjalan'=>1,'statusverified'=>1, 'jenisuser'=>2])) {
            // code...
            return Auth::user();
        } elseif (Auth::attempt(['email' =>$request->input('email'), 'password' => $request->input('password'), 'jenisuser'=>3])) {
            // code...
            return Auth::user();
        }
    }



    // public function loginbarbershop(Request $request)
    // {
    //     //untuk validasi Android

    //     $messages = [
    //     'email.required'    => ':attribute perlu diisi.',
    //     'password.required'    => ':attribute perlu diisi.',
    //     'password.min'    => ':attribute minimal 3 character.',

    //     'telepon.required'    => ':attribute perlu diisi.',
    //   ];
    //     $validator = Validator::make($request->all(), [
    //       'email' => 'required|email',
    //       'password' => 'required|min:3',
    //       // 'telepon' => 'required|min:3',

    //   ], $messages);

    //     if ($validator->fails()) {
    //         $error = ["message"=>"terjadi kesalahan","errors"=>$validator->errors()];
    //         return response()->json($error, 401);
    //     }


    //     // if (Auth::attempt(['telepon' =>$request->input('telepon'),'password' => $request->input('password'),'statusjalan'=>1,'statusverified'=>1, 'jenisuser'=>1])) {
    //     //     $errors = ["message"=>"Masukkan username yang sesuai"];
    //     //     return response()->json($errors, 401);
    //     // // return Auth::user();
    //     // }


    //     if (Auth::attempt(['email' =>$request->input('email'), 'password' => $request->input('password'),'statusjalan'=>1,'statusverified'=>1, 'jenisuser'=>1])) {
    //         $errors = ["message"=>"Masukkan username yang sesuai"];
    //         return response()->json($errors, 401);
    //     // return Auth::user();
    //     }
    //      elseif (Auth::attempt(['email' =>$request->input('email'), 'password' => $request->input('password'),'statusjalan'=>1,'statusverified'=>1, 'jenisuser'=>2])) {
    //         // code...
    //         return Auth::user();
    //     } elseif (Auth::attempt(['email' =>$request->input('email'), 'password' => $request->input('password'), 'statusjalan'=>1,'statusverified'=>1,'jenisuser'=>3])) {
    //         // code...
    //         return Auth::user();
    //     }
    // }


    public function loginmerchant(Request $request)
    {
        //untuk validasi Android

        $messages = [
        'email.required'    => ':attribute perlu diisi.',
        'password.required'    => ':attribute perlu diisi.',
      ];
        $validator = Validator::make($request->all(),  [
          'email' => 'required|email',
          'password' => 'required|min:3',

      ], $messages);
        if ($validator->fails()) {
            $error = ["message"=>"terjadi kesalahan","errors"=>$validator->errors()];
            return response()->json($error, 401);
        }

        if (Auth::attempt(['email'=>$request->input('email'),'password' => $request->input('password'), 'statusjalan'=>1,'statusverified'=>1, 'jenisuser'=>3])) {
            // $errors = ["message"=>"Masukkan usernama yang sesuai"];
            // return response()->json($errors, 401);
            return Auth::user();
        }
        //  elseif (Auth::attempt(['email' =>$request->input('email'), 'password' => $request->input('password'),'statusjalan'=>1,'statusverified'=>1, 'jenisuser'=>2])) {
        //     // code...
        //     return Auth::user();
        // } elseif (Auth::attempt(['email' =>$request->input('email'), 'password' => $request->input('password'),'statusjalan'=>1,'statusverified'=>1, 'jenisuser'=>1])) {
        //     // code...
        //     return Auth::user();
        // }
    }




    public function logincustomer(Request $request)
    {
        //untuk validasi Android

        $messages = [
        'email.required'    => ':attribute perlu diisi.',
        'password.required'    => ':attribute perlu diisi.',
      ];
        $validator = Validator::make($request->all(),  [
          'email' => 'required|email',
          'password' => 'required|min:3',

      ], $messages);
        if ($validator->fails()) {
            $error = ["message"=>"terjadi kesalahan","errors"=>$validator->errors()];
            return response()->json($error, 401);
        }

        if (Auth::attempt(['email'=>$request->input('email'),'password' => $request->input('password'), 'statusjalan'=>1,  'jenisuser'=>2])) {
            // $errors = ["message"=>"Masukkan usernama yang sesuai"];
            // return response()->json($errors, 401);
            return Auth::user();
        }
         elseif (Auth::attempt(['email' =>$request->input('email'), 'password' => $request->input('password'),'statusjalan'=>1,'statusverified'=>1, 'jenisuser'=>3])) {
            // code...
            return Auth::user();
        } elseif (Auth::attempt(['email' =>$request->input('email'), 'password' => $request->input('password'),'statusjalan'=>1,'statusverified'=>1, 'jenisuser'=>1])) {
            // code...
            return Auth::user();
        }
    }



    public function logout()
    {
        Auth::logout();
    }

    // public function registeruser(){
    //   $user = new User();
    //   $user->email = 'test@gmail.com';
    //   $user->nama = 'test';
    //   $user->jenisuser = '2';
    //   $user->password = Hash::make("test");
    //   $user->api_token = str_random(60);
    //   $user->save();
    // }
}
