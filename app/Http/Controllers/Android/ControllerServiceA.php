<?php

namespace App\Http\Controllers\Android;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\Service;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;

class ControllerServiceA extends Controller
{
    public function addservice(Request $request)
    {

      // tidak perlu karena agak berat jika di atur di laravel, lebih cepat diatur langsung di android studio

        $messages = [
      'namaservice.required'    => ':attribute perlu diisi.',
        'tarif.required'    => ':attribute perlu diisi.',
      'keterangan.required'    => ':attribute perlu diisi.',
      'tipeservice.required'    => ':attribute perlu diisi.',
    ];
        $validator = Validator::make($request->all(), [
        'namaservice' => 'required|',
        'tarif' => 'required|',
        'keterangan' => 'required|',
          'tipeservice' => 'required|',
    ], $messages);

        if ($validator->fails()) {
            $error = ["message"=>"terjadi kesalahan","errors"=>$validator->errors()];
            return response()->json($error, 401);
        }

        $service = new Service();
        $service->namaservice = $request->input('namaservice');
        $service->tarif = $request->input('tarif');
        $service->keterangan = $request->input('keterangan');
        $service->tipeservice = $request->input('tipeservice');
        $service->user_id = $request->user()->id;
        // $service->stylist_id = $request->stylist()->id;
        $service->save();
        return response()->json(['data' => $service], 200, [], JSON_NUMERIC_CHECK);
    }

    // mengambil data service barbershop khusus login sebagai barbershop
    public function getlistservicemerchant(Request $request)
    {
        // $service = Service::get();
        $service = Service::where('user_id', '=', Auth::id())->get();
        return response()->json(['data' => $service], 200, [], JSON_NUMERIC_CHECK);
    }


    // mengambil data service barbershop  login sebagai customer
    public function getlistservicecustomer(Request $request)
    {
        $service = Service::get();
        return response()->json(['data' => $service], 200, [], JSON_NUMERIC_CHECK);
    }

    public function getdataservice(Request $request)
    {
        $service = Service::find($request->id);
        return response()->json(['data' => $service], 200, [], JSON_NUMERIC_CHECK);
    }

    public function editservice(Request $request)
    {
        // return response()->json(['data' => []], 200, [], JSON_NUMERIC_CHECK);

        $namaservice = $request->namaservice;
        $tarif  = $request->tarif;
        $keterangan = $request->keterangan;
        $tipeservice  = $request->tipeservice;

        // $service = service::find($request->input("idservice"));
        $service = service::find($request->id);
        $service ->namaservice = $namaservice;
        $service->tarif = $tarif;
        $service->keterangan = $keterangan;
        $service->tipeservice = $tipeservice;
        $service->save();

        return response()->json(['data' => $service], 200, [], JSON_NUMERIC_CHECK);
    }


    public function deleteservice(Request $request)
    {
        $service = service::find($request->id);
        $service->delete();
        return response()->json(['data' => $service], 200, [], JSON_NUMERIC_CHECK);
    }
}
