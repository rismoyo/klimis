<?php

namespace App\Http\Controllers\Android;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\Carousel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;

class ControllerBookingA extends Controller
{
    public function addcarousel(Request $request)
    {
        $messages = [
      'namacarousel.required'    => ':attribute perlu diisi.',
      'urlgambar.required'    => ':attribute perlu diisi.',
    ];
        $validator = Validator::make($request->all(), [
        'namacarousel' => 'required|',
        'urlgambar' => 'required|',
    ], $messages);

        if ($validator->fails()) {
            $error = ["message"=>"terjadi kesalahan","errors"=>$validator->errors()];
            return response()->json($error, 401);
        }

        $carousel = new Booking();
        $carousel->namacarousel = $request->input('namacarousel');
        $carousel->urlgambar = $request->input('urlgambar');
        $carousel->user_id = $request->user()->id;
        $carousel->save();
        return response()->json(['data' => $carousel], 200, [], JSON_NUMERIC_CHECK);
    }


    // mengambil list data carousel barbershop berdasarkan login user
    public function getlistcarousel(Request $request)
    {
        // $carousel = Carousel::get();
        $carousel = Carousel::with('user')->where('user_id','=',Auth::id())->get();
        return response()->json(['data' => $carousel], 200, [], JSON_NUMERIC_CHECK);
    }

    // mengambil data  carousel digunakan untuk edit data
    public function getdatacarousel(Request $request)
    {
        $carousel = Carousel::find($request->id);
        return response()->json(['data' => $carousel], 200, [], JSON_NUMERIC_CHECK);
    }
    //  digunakan untuk hapus data
    public function deletecarousel(Request $request)
    {
        $carousel = Carousel::find($request->id);
        $carousel->delete();
        return response()->json(['data' => $carousel], 200, [], JSON_NUMERIC_CHECK);
    }


    public function editcarousel(Request $request)
    {
        $namacarousel = $request->namacarousel;
        $urlgambar = $request->urlgambar;

        $carousel = Carousel::find($request->id);
        $carousel ->namacarousel =$namacarousel;
        if ($urlgambar) {
            $urlgambar ->urlgambar =$urlgambar;
        }
        $carousel->save();

        return response()->json(['data' => $carousel], 200, [], JSON_NUMERIC_CHECK);
    }
}
