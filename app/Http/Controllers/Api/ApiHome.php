<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\Carousel;
use App\Models\Service;
use App\Models\User;
use App\Models\Stylist;
use App\Models\Favorite;
use App\Models\Promo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use DB;

class ApiHome extends Controller
{
  //  *************** menampilkan seluruh data user barbershop pada halaman utama user customer **************************
  public function getData(Request $request)
  {
    $input = $request->all();
    // $user_id = Auth::id();

    // $center_lat = '-7.2718329';//
    // $center_lng = '112.74255604999999';///
    // $distance = 6;///

    // $distance = 6;
    // $near_by = User::getNearByLocation($center_lat, $center_lng, $distance)->with('carousel_utama', 'carousels')

    //   ->join('service', 'service.user_id', '=', 'user.id')
    //   ->where('user.jenisuser', '=', 3)
    //   ->where('user.statusjalan', '=', 1)
    //   ->where('user.statusbuka', '=', 1)
    //   ->where('user.statusverified', '=', 1)
    //   ->whereNotNull('service.user_id')
    //   ->distinct()
    //   ->get();

    // $user = User::with(['carousel_utama', 'carousels', 'services'])
    //   ->join('service', 'service.user_id', '=', 'user.id')
    //   ->where('user.jenisuser', '=', 3)
    //   ->where('user.statusjalan', '=', 1)
    //   ->where('user.statusbuka', '=', 1)
    //   ->where('user.statusverified', '=', 1)
    //   ->whereNotNull('service.user_id')
    //   ->distinct()
    //   ->get();


    // $near_by = User::getNearByLocation($center_lat, $center_lng, $distance)->with('carousel_utama', 'carousels')

    //   ->where('jenisuser', '=', 3)
    //   ->where('statusjalan', '=', 1)
    //   ->where('statusbuka', '=', 1)
    //   ->where('statusverified', '=', 1)
    //   ->get();

    $user = User::with(['carousel_utama', 'carousels', 'ratingkomentars_to', 'ratingkomentars_from'])
      ->where('jenisuser', '=', 3)
      ->where('statusjalan', '=', 1)
      ->where('statusbuka', '=', 1)
      ->where('statusverified', '=', 1)
      ->get();
    // ->where('id', '!=', $request->input('id'))
    // ->orWhereNull('id')
    // $promo = User::with(['promo'])
    //   // ->where('user_id', '=', $user_id)
    //   // ->whereBetween('reservation_from', [$from, $to])
    //   // ->whereDate('created_at', Carbon::today())
    //   // ->orderBy('created_at', 'desc')
    //   ->whereDate('waktuawal', '<=', Carbon::today())
    //   ->whereDate('waktuakhir', '>=', Carbon::today())
    //   ->orderBy('created_at', 'desc')
    //   ->get();

    $promo = User::with(['promo_aktif', 'carousel_utama', 'carousels'])
      ->whereHas('promos', function ($query) {
        $query->whereDate('waktuawal', '<=', Carbon::today());
        $query->whereDate('waktuakhir', '>=', Carbon::today());
      })->get();
    // $carousel = Carousel::with(['user'])
    //   // ->where('jenisuser', '=', 3)
    //   // ->where('statusjalan', '=', 1)
    //   // ->where('statusbuka', '=', 1)
    //   // ->where('statusverified', '=', 1)
    //   ->where('utama', '=', 1)
    //   // ->whereNotNull('carousel_utama')
    //   ->get();


    $data = [
      'user' => $user,
      // 'near_by' => $near_by,
      'promo' => $promo,
    ];

    return response()->json($this->setSuccessResponse($data, $input));
  }


  public function getNearbyMerchant(Request $request)
  {
    $input = $request->all();
    // $user_id = Auth::id();
    // $center_lat = '-7.2718329';
    // $center_lng = '112.74255604999999';

    $center_lat = $request->input('center_lat');
    $center_lng = $request->input('center_lng');
    $distance = 6;


    $near_by = User::getNearByLocation($center_lat, $center_lng, $distance)->with('carousel_utama', 'carousels')
      // ->where('id', '!=', $request->input('id'))
      // ->orWhereNull('id')
      ->where('jenisuser', '=', 3)
      ->where('statusjalan', '=', 1)
      ->where('statusbuka', '=', 1)
      ->where('statusverified', '=', 1)
      ->get();



    $promo = User::with(['promo_aktif', 'carousel_utama', 'carousels'])
      ->whereHas('promos', function ($query) {
        $query->whereDate('waktuawal', '<=', Carbon::today());
        $query->whereDate('waktuakhir', '>=', Carbon::today());
      })->get();
    // $carousel = Carousel::with(['user'])
    //   // ->where('jenisuser', '=', 3)
    //   // ->where('statusjalan', '=', 1)
    //   // ->where('statusbuka', '=', 1)
    //   // ->where('statusverified', '=', 1)
    //   ->where('utama', '=', 1)
    //   // ->whereNotNull('carousel_utama')
    //   ->get();


    $data = [
      'near_by' => $near_by,
      'promo' => $promo,
    ];

    return response()->json($this->setSuccessResponse($data, $input));
  }

  // ********************************untuk percobaan *************************
  public function getDataUser(Request $request)
  {
    // $input = $request->all();

    $user_id = Auth::id();
    return $user_id;
    // $center_lat = '-7.2718329';
    // $center_lng = '112.74255604999999';
    // $distance = 6;
    // $near_by = User::getNearByLocation($center_lat, $center_lng, $distance)->with('carousels')->get();
    // $user = User::where('id', '!=', Auth::id())->get();
    // if ($user == $userid) {
    //   $user = User::where('jenisbuka', 3)->get();
    //  }
    $fcmtokenuser = DB::table('user')->select('firebase_fcm_token')->where('id', '=', $user_id)->get();
    // ->orderBy('created_at', 'desc')
    // ->get();
    $data = $fcmtokenuser;
    return response()->json($this->setSuccessResponse($data));
  }


  //  ******* menampilkan data merchant barbershop berdasarkan id merchant barbershop yang dipilih oleh user customer ********
  // ******* dan juga menampilkan informasi data user seperti-> nama, saldo yang akan di tampilkan ****


  public function getSearchStylistList(Request $request)
  {
    $input = $request->all();
    $stylist = Stylist::with(['user'])->where('statuskerja', 1)
      ->get();
    $data = [
      'stylist' => $stylist,
    ];
    return response()->json($this->setSuccessResponse($data, $input));
  }



  public function getViewMerchantDetail(Request $request)
  {
    $input = $request->all();
    $merchant = User::with(['jadwalbarbers', 'jadwalbarber_today', 'carousel_utama', 'carousels', 'promo_aktif'])
      ->find($request->input('id'));

    $tipeservice = '%';
    // $tipeserviceboth = 'Home dan Barbershop';
    if ($request->input('tipe') == 1) {
      $tipeservice = 'Home dan Barbershop';
    }
    // else if ($request->input('tipe') == 0) {
    //   $tipeservice = 'Only Barbershop';
    // }


    // $tipeServiceHomeBarbershop = Service::with(['user'])->Where('tipeservice', '=', 'Home dan Barbershop')->get();
    // $tipeServiceOnlyBarbershop = Service::with(['user'])->Where('tipeservice', '=', 'Only Barbershop')->get();
    // if ($tipeServiceHomeBarbershop) {
    //   $isTipeService = 0;
    // } else if ($tipeServiceOnlyBarbershop) {
    //   $isTipeService = 1;
    // }
    $services = Service::with(['user'])
      ->where('user_id', '=', $request->input('id'))
      ->where('tipeservice', 'like', $tipeservice)
      ->orderBy('created_at', 'desc')
      ->get();
    $favorite = Favorite::with(['usermerchant'])
      ->where('usermerchant_id', '=', $request->input('id'))
      ->orderBy('created_at', 'desc')
      ->first();
    $stylist = Stylist::with(['user', 'jadwalstylists'])
      ->where('user_id', '=', $request->input('id'))
      ->where('statuskerja', '=', 1)
      ->orderBy('created_at', 'desc')
      ->get();

    // $promo = Promo::with(['user'])
    //   // ->select('diskon')
    //   ->where('user_id', '=', $request->input('id'))
    //   ->whereDate('waktuawal', '<=', Carbon::today())
    //   ->whereDate('waktuakhir', '>=', Carbon::today())
    //   ->orderBy('created_at', 'desc')
    //   ->get();
    $data = [
      'merchant' => $merchant,
      'services' => $services,
      'favorite' => $favorite,
      'stylist' => $stylist,
      // 'promo' => $promo,
    ];
    return response()->json($this->setSuccessResponse($data, $input));
  }




  public function getViewStylistMerchantDetail(Request $request)
  {
    $input = $request->all();
    $merchant = User::with(['jadwalbarbers', 'jadwalbarber_today', 'carousel_utama', 'carousels', 'promo_aktif'])
      ->find($request->input('id'));

    // $tipeservice = '%';
    // $tipeserviceboth = 'Home dan Barbershop';
    if ($request->input('tipe') == 1) {
      $tipeservice = 'Home dan Barbershop';
    }

    $services = Service::with(['user'])
      ->where('user_id', '=', $request->input('id'))
      ->where('tipeservice', '=', $tipeservice)
      ->orderBy('created_at', 'desc')
      ->get();
    $favorite = Favorite::with(['usermerchant'])
      ->where('usermerchant_id', '=', $request->input('id'))
      ->orderBy('created_at', 'desc')
      ->first();
    $stylist = Stylist::with(['user', 'jadwalstylists'])
      ->where('user_id', '=', $request->input('id'))
      ->where('statuskerja', '=', 1)
      ->orderBy('created_at', 'desc')
      ->get();

    $data = [
      'merchant' => $merchant,
      'services' => $services,
      'favorite' => $favorite,
      'stylist' => $stylist,
      // 'promo' => $promo,
    ];
    return response()->json($this->setSuccessResponse($data, $input));
  }


  //  *** untuk update saldo. setelah proses transaksi  pengurangan saldo yang dimiliki sekarang dengan DP ****
  //****("penghitungan dilakukan di android")*****
  public function postUpdateSaldoUser(Request $request) ///////////////////////////
  {
    // $request->merge($request->input('data'));
    // $this->createLogInfo('postUpdateSaldoUser : ' . json_encode($request->all()));

    // $user_id = Auth::id();
    // // $user_id = 2;
    // $user = User::find($user_id);
    // DB::transaction(function () use ($request, $user) {
    //   $edit = false;
    //   if ($request->has('id')) {
    //     $edit = true;
    //   }

    //   $user->saldo = $request->input('saldo');

    //   $user->save();
    // }, 5);
    // // $profile = new Profile();
    // return $this->getUpdateSaldoUser($request);
  }

  // ** digunakan untuk kembalian yang diakukan function postUpdateSaldoUser() ***
  public function getUpdateSaldoUser(Request $request)
  {
    $input = $request->all();
    $user_id = Auth::id();
    // $user_id=2;
    $carousel = User::orderBy('created_at', 'desc')
      ->find($user_id);
    $data = $carousel;
    return response()->json($this->setSuccessResponse($data, $input));
  }
}
