<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BookingHeader;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;
use App\Models\Informasi;
use App\Models\Favorite;
use App\Models\Ratingkomentar;
use App\Models\User;
use App\Models\Report;
use App\Models\Promo;
use App\Models\Stylist;

class ApiCustomer extends Controller
{

  public function getInformasiCustomerList(Request $request)
  {
    $input = $request->all();
    // $user_id = Auth::id();
    $informasi = Informasi::with(['usercustomer'])
      ->where('user_customer_id', '=', Auth::id())
      ->orderBy('created_at', 'desc')
      ->get();
    $data = $informasi;
    return response()->json($this->setSuccessResponse($data, $input));
  }

  public function getInformasiCustomerFromId(Request $request)
  {
    $input = $request->all();
    $informasi = Informasi::find($request->input('id'));
    $data = $informasi;
    return response()->json($this->setSuccessResponse($data, $input));
  }

  //////////////////////////////////////////// PENGECEKAN RATING KOMENTAR ////////////////////////////////////////////
  public function getCheckRatingKomentarFromId(Request $request)
  {
    $input = $request->all();
    $ratingkomentar = Ratingkomentar::with(['booking_header', 'user_customer'])
      ->find($request->input('booking_header_id'));
    $data = $ratingkomentar;
    return response()->json($this->setSuccessResponse($data, $input));
  }

  ///////////////////////////////////////////////////////////// FAVORITE /////////////////////////////////////////////////////

  public function postFavoriteEntry(Request $request)
  {
    // return "";

    // $favorite = new Favorite();
    // $request->merge($request->input('data'));
    // $this->createLogInfo('postfavorite : '.json_encode($request->all()));

    // if($favorite = Favorite::Where('usercustomer_id', '=', $request->input()->where('usermerchant_id', '=', $request->input('id') ->get()){

    //   $favorite->usermerchant_id = $request->input('usermerchant_id');
    //   $favorite->usercustomer_id = Auth::id();
    //   $favorite->save();
    // }else{
    //   $favorite = Favorite::Find($request->input('id'));
    //   $favorite->delete();
    // }
    // return $this->getFavoriteList($request);
    // $isfavorite = 0;

    // $this->sendNotificationToDevice('abcd', 'erty', 'dZuZw3x69kk:APA91bEppqsHwjQJiqjNQcguB2bIuHSM1qmJsgT3y4_BYxwdQS2GtmBacD1ZOhAL3pUq_6HJqduWEjF514pnbLyLILtU8SwLgQk0eAScVwqPeI5sA-VUu_YxiFaAlYxkCUl9he8diMhv');
    $input = $request->all();
    $favorite_exist = Favorite::Where('usercustomer_id', '=', Auth::id())->where('usermerchant_id', '=', $request->input('usermerchant_id'))->first();
    if ($favorite_exist) {
      //  delete
      $favorite = Favorite::Where('usercustomer_id', '=', Auth::id())->where('usermerchant_id', '=', $request->input('usermerchant_id'));
      $favorite->delete();
      $isfavorite = 0;
    } else {
      // insert
      $favorite = new Favorite();
      $favorite->usercustomer_id = Auth::id();
      $favorite->usermerchant_id = $request->input('usermerchant_id');
      $favorite->save();
      $isfavorite = 1;
    }
    $result = [
      'is_exists' => $isfavorite
    ];

    return response()->json($this->setSuccessResponse($result, $input));
  }

  public function getFavoriteDelete(Request $request)
  {
    $favorite_exist = Favorite::Where('usercustomer_id', '=', Auth::id())->where('usermerchant_id', '=', $request->input('usermerchant_id'))->first();
    if ($favorite_exist) {
      // ini delete
      $favorite = Favorite::Where('usercustomer_id', '=', Auth::id())->where('usermerchant_id', '=', $request->input('usermerchant_id'));
      $favorite->delete();
    }
    return $this->getFavoriteList($request);
  }


  //mengambil data yang sudah difavoritkan oleh user yang sudah login
  public function getFavoriteData(Request $request)
  {
    $isfavorite = 0;
    $input = $request->all();
    $favorite = Favorite::with(['usermerchant', 'usercustomer'])
      ->where('usercustomer_id', '=', Auth::id())->where('usermerchant_id', '=', $request->input('usermerchant_id'))
      ->orderBy('created_at', 'desc')
      ->first();
    if ($favorite) {
      $isfavorite = 1;
    }

    $result = [
      'is_exists' => $isfavorite
    ];
    return response()->json($this->setSuccessResponse($result, $input));
  }

  // mengambil seluruh list data favorite merchant berdasarkan customer login (RESPONSE SETELAH INPUT FAVORITE IN DATABASE)
  public function getFavoriteList(Request $request)
  {
    //   $user_id = Auth::id();
    //   $input = $request->all();
    //   $merchant = User::with(['carousels','carousel_utama'])
    //   ->find($user_id);
    //   $favorite = Favorite::with(['usermerchant'])
    //   ->where('usercustomer_id', '=', $user_id)
    //   ->orderBy('created_at','desc')
    //   ->get();
    //   $data = [
    //     'merchant' => $merchant,
    //     'favorite' => $favorite,
    //   ];
    //   return response()->json($this->setSuccessResponse($data, $input));
    // }


    $input = $request->all();
    $user_id = Auth::id();
    $favorite = Favorite::with(['usermerchant'])
      ->where('usercustomer_id', '=', $user_id)
      ->orderBy('created_at', 'desc')
      ->get();
    $data = $favorite;
    return response()->json($this->setSuccessResponse($data, $input));
  }

  // public function sendNotifBooking(Request $request)
  // {
  //   $cust = User::find(18);
  //   $this->sendNotificationToDevice('Opo Jare Aku', 'janck', $cust->firebase_fcm_token);
  // }


  ///////////////////////////////////////////////////////////// RATING & KOMENTAR  /////////////////////////////////////////////////////

  // ***** menympan transaksi booking header dan detail ke database *****
  public function postCustomerRatingKomentar(Request $request)
  {
    $request->merge($request->input('data')); //return list (object) ini kan? iyo wes mbo save auto upload yo?
    $this->createLogInfo('postCustomerRatingKomentar : ' . json_encode($request->all()));
    $ratingkomentar = new Ratingkomentar();

    // pie iso? gak bisa
    // echo $mytime->toDateTimeString(); wes jelas itu msalh e :v bentaraham g ngot.. tapi iki tak coba ng postman jalan ki, la iya ini yg km lempar d android kan json format e 
    DB::transaction(function () use ($request, $ratingkomentar) {
      // $mytime = Carbon\Carbon::now();
      // $edit = false;
      // if ($request->has('id')) {
      //   $edit = true;
      // }
      $ratingkomentar->review  = $request->input('review');
      $ratingkomentar->jumlah  = $request->input('jumlah');
      $ratingkomentar->user_customer_id = Auth::id();
      $ratingkomentar->booking_header_id = $request->input('booking_header_id');
      $ratingkomentar->user_merchant_id =  $request->input('user_merchant_id');
      $ratingkomentar->save();

      // $bookingheader->status_booking =  5;
      // $bookingheader->save();

      // $Bookingheader->rating_komentar_id  = $request->input('rating_komentar_id');
      // $ratingkomentar->save();
    }, 5);

    return $this->getCustomerRatingKomentarReturn($request);
  }

  //RETURN
  public function getCustomerRatingKomentarReturn(Request $request)
  {
    $input = $request->all();
    $user_id = Auth::id();
    $ratingkomentar = Ratingkomentar::with(['user_customer', 'booking_header'])
      // ->where('user_customer_id', '=', Auth::id())
      ->orderBy('created_at', 'desc')
      ->find($user_id);
    $data = $ratingkomentar;
    return response()->json($this->setSuccessResponse($data, $input));
  }


  // **** menampilkan data rating& komentar di detail rating & komentar ******
  // public function getCustomerRatingKomentarData(Request $request)
  // {
  //   $input = $request->all();
  //   $ratingkomentar = Ratingkomentar::with(['user_customer'])
  //     ->where('user_customer_id', '=', $request->input('id'))
  //     ->orderBy('created_at', 'desc')
  //     ->get();


  //   $data = [
  //     'ratingkomentar' => $ratingkomentar,
  //     // 'bookingdetail' => $bookingdetail,
  //   ];
  //   return response()->json($this->setSuccessResponse($data, $input));
  // }


  ///////////////////////////////////////////////////////////// REPORT /////////////////////////////////////////////////////

  // ***** menympan transaksi booking header dan detail ke database *****
  public function postCustomerReportBarbershopEntry(Request $request)
  {
    $request->merge($request->input('data')); //return list (object)
    $this->createLogInfo('postBarbershopReportCustomerEntry : ' . json_encode($request->all()));
    $report = new Report();

    DB::transaction(function () use ($request, $report) {
      // $mytime = Carbon\Carbon::now();
      // $edit = false;
      // if ($request->has('id')) {
      //   $edit = true;
      // }


      $report->judul = 'Customer Report Barbershop';
      $report->isi = $request->input('isi');
      $report->urlgambar = $request->input('urlgambar');
      $report->user_customer_id = Auth::id();
      $report->user_merchant_id = $request->input('user_merchant_id');
      $report->save();
    }, 5);

    return $this->getCustomerReportBarbershopReturn($request);
  }

  public function getCustomerReportBarbershopReturn(Request $request)
  {
    $input = $request->all();
    $user_id = Auth::id();
    $report = Report::with(['user_merchant'])
      ->orderBy('created_at', 'desc')
      ->find($user_id);
    $data = $report;
    return response()->json($this->setSuccessResponse($data, $input));
  }

  /// MENGAMBIL NAMA STYLIST PADA BOOKING STYLIST YANG DILAKUKAN MELALUI PILIH STYLIST PADA AWAL HALAMAN CUSTOMER ///////////////////
  public function getStylistFromNama(Request $request)
  {
    $input = $request->all();
    // $stylist = Stylist::where('id', '=', $request->input('id'))->get();

    $stylist = Stylist::with('user')->find($request->input('id'));

    // $data = $stylist;
    $data = [
      'stylist' => $stylist,
    ];
    return response()->json($this->setSuccessResponse($data, $input));
  }


  ////////////////////////////////////////////////////////GET PROMO TODAY  /////////////////////////////////////////////////////
  //********** DALAM BENTUK QUERY MYSQL ****************//
  // SELECT user.nama, promo.diskon, promo.namapromo
  // FROM u272891939_kms.user
  // INNER JOIN u272891939_kms.promo ON promo.user_id = user.id
  // WHERE promo.waktuawal <= CURDATE() AND promo.waktuakhir >= CURDATE() ; 

  // public function getPromoForTodayList(Request $request)
  // {
  //   // $user_id = Auth::id();
  //   $input = $request->all();
  //   $promo = Promo::with(['user'])
  //     // ->where('user_id', '=', $user_id)
  //     // ->whereBetween('reservation_from', [$from, $to])
  //     // ->whereDate('created_at', Carbon::today())
  //     // ->orderBy('created_at', 'desc')
  //     ->whereDate('waktuawal', '<=', Carbon::today())
  //     ->whereDate('waktuakhir', '>=', Carbon::today())
  //     ->orderBy('created_at', 'desc')
  //     ->get();
  //   $data = $promo;
  //   return response()->json($this->setSuccessResponse($data, $input));
  // }

  // public function getPromoForTodayFromId(Request $request)
  // {
  //   // $user_id = Auth::id();

  //   $input = $request->all();
  //   $promo = DB::table('promo')
  //     ->join('user', 'user.id', '=', 'promo.user_id')
  //     // ->join('orders', 'users.id', '=', 'orders.user_id')
  //     ->select('promo.diskon')
  //     ->whereDate('promo.waktuawal', '<=', Carbon::today())
  //     ->whereDate('promo.waktuakhir', '>=', Carbon::today())
  //     ->where('promo.user_id', '=', $request->input('id'))
  //     ->get();


  //   $data = $promo;
  //   return response()->json($this->setSuccessResponse($data, $input));
  // }
}
