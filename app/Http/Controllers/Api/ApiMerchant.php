<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\Promo;
use App\Models\Carousel;
use App\Models\Stylist;
use App\Models\Service;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;
use App\Models\Jadwalstylist;
// use App\Http\Controllers\Api\ApiMerchant as Profile;
use App\Models\Jadwalbarber;
use App\Models\BookingHeader;
use App\Models\Ratingkomentar;
use App\Models\Report;

class ApiMerchant extends Controller
{
  /////////////////////////////////////////////////////  PROFILE ///////////////////////////////////////////////////////////////
  public function postMerchantProfileEntry(Request $request)
  {
    $request->merge($request->input('data'));
    $this->createLogInfo('postMerchantProfileEntry : ' . json_encode($request->all()));

    // if ($request->has('jadwalbarbers')){
    //   $jadwalbarbers = [];
    //   foreach ($request->input('jadwalbarbers') as $key => $jadwal) {
    //     $jadwalbarber = new Jadwalbarber([
    //       'hari' => $jadwal['hari'],
    //       'jambuka' => $jadwal['jambuka'],
    //       'jamtutup' => $jadwal['jamtutup'],
    //       'bukatutup' => $jadwal['bukatutup'],
    //     ]);
    //     if (array_key_exists('id', $jadwal)) {
    //       $jadwalbarber->id = $jadwal['id'];
    //     };
    //     $jadwalbarbers[] = $jadwalbarber;
    //   }
    //   return response()->json($jadwalbarbers);
    // }

    $user_id = Auth::id();
    // $user_id = 2;
    $user = User::find($user_id);
    DB::transaction(function () use ($request, $user) {
      $edit = false;
      if ($request->has('id')) {
        $edit = true;
      }

      $user->nama = $request->input('nama');
      $user->telepon = $request->input('telepon');
      $user->pinalamat = $request->input('pinalamat');
      $user->pinlatitude = $request->input('pinlatitude');
      $user->pinlongitude = $request->input('pinlongitude');
      $user->statusbuka = $request->input('statusbuka');
      $user->no_rekening = $request->input('no_rekening');
      $user->bank = $request->input('bank');
      $user->atas_nama_bank = $request->input('atas_nama_bank');
      // $user->jenisuser = 2;
      $user->save();



      if ($request->has('carousels')) {
        $carousels = [];
        foreach ($request->input('carousels') as $key => $car) {
          $carousel = new Carousel([
            'urlgambar' => $car['urlgambar'],
            'utama' => $car['utama'],
          ]);
          if (array_key_exists('id', $car)) {
            $carousel->id = $car['id'];
          };
          $carousels[] = $carousel;
        }
        if ($edit) {
          $user->carousels()->forceDelete();
        }
        $user->carousels()->saveMany($carousels);
      }

      if ($request->has('jadwalbarbers')) {
        $jadwalbarbers = [];
        foreach ($request->input('jadwalbarbers') as $key => $jadwal) {
          $jadwalbarber = new Jadwalbarber([
            'hari' => $jadwal['hari'],
            'jambuka' => $jadwal['jambuka'],
            'jamtutup' => $jadwal['jamtutup'],
            'bukatutup' => $jadwal['bukatutup'],
          ]);
          if (array_key_exists('id', $jadwal)) {
            $jadwalbarber->id = $jadwal['id'];
          };
          $jadwalbarbers[] = $jadwalbarber;
        }
        if ($edit) {
          $user->jadwalbarbers()->forceDelete();
        }
        $user->jadwalbarbers()->saveMany($jadwalbarbers);
      }
    }, 5);
    // $profile = new Profile();
    return $this->getMerchantProfileData($request);
  }

  public function getMerchantProfileData(Request $request)
  {
    $input = $request->all();
    $user_id = Auth::id();
    // $user_id=2;
    $carousel = User::with(['carousels', 'carousel_utama', 'jadwalbarbers'])
      ->orderBy('created_at', 'desc')
      ->find($user_id);
    $data = $carousel;
    return response()->json($this->setSuccessResponse($data, $input));
  }

  public function getCarouselList(Request $request)
  {
    $input = $request->all();
    $carousel = Carousel::with(['user'])
      ->orderBy('created_at', 'desc')
      ->get();
    $data = $carousel;
    return response()->json($this->setSuccessResponse($data, $input));
  }

  /////////////////////////////////////////////////////////////// SERVICES ///////////////////////////////////////////////////////////////////////////

  public function postServiceEntry(Request $request)
  {
    $request->merge($request->input('data'));
    $this->createLogInfo('postServiceEntry : ' . json_encode($request->all()));
    $service = new Service();
    if ($request->has('id')) {
      $service = Service::find($request->input('id'));
    }
    DB::transaction(function () use ($request, $service) {
      $user_id = Auth::id();
      // $user_id=1;
      $edit = false;
      if ($request->has('id')) {
        $edit = true;
      }
      $service->namaservice = $request->input('namaservice');
      $service->tarif = $request->input('tarif');
      $service->lamaservice = $request->input('lamaservice');
      $service->tipeservice = $request->input('tipeservice');
      $service->user_id = $user_id;
      $service->save();
    }, 5);
    return $this->getServiceList($request);
  }

  public function getServiceList(Request $request)
  {
    $user_id = Auth::id();
    $input = $request->all();
    $service = Service::with(['user'])
      ->where('user_id', '=', $user_id)
      ->orderBy('created_at', 'desc')
      ->get();
    $data = $service;
    return response()->json($this->setSuccessResponse($data, $input));
  }
  public function getServiceFromId(Request $request)
  {
    $input = $request->all();
    $service = Service::find($request->input('id'));
    $data = $service;
    return response()->json($this->setSuccessResponse($data, $input));
  }
  public function getServiceDelete(Request $request)
  {
    $service = Service::find($request->input('id'));
    $service->delete();
    return $this->getServiceList($request);
  }

  //////////////////////////////////////////////////////// STYLIST ///////////////////////////////////////////////////////////////

  public function getStylistList(Request $request)
  {
    $user_id = Auth::id();
    $input = $request->all();
    $stylist = Stylist::with(['jadwalstylists', 'user', 'bookingheaders', 'services'])
      ->where('user_id', '=', $user_id)
      ->orderBy('created_at', 'desc')
      ->get();
    $data = $stylist;
    return response()->json($this->setSuccessResponse($data, $input));
  }
  public function getStylistFromId(Request $request)
  {
    $input = $request->all();
    $stylist = Stylist::with(['jadwalstylists', 'user'])->find($request->input('id'));
    $data = $stylist;
    return response()->json($this->setSuccessResponse($data, $input));
  }

  public function getStylistDelete(Request $request)
  {
    $stylist = Stylist::find($request->input('id'));
    $stylist->delete();
    return $this->getStylistList($request);
  }

  public function postStylistEntry(Request $request)
  {
    $request->merge($request->input('data'));
    $this->createLogInfo('postStylistEntry : ' . json_encode($request->all()));
    $stylist = new Stylist();
    if ($request->has('id')) {
      $stylist = Stylist::find($request->input('id'));
    }
    DB::transaction(function () use ($request, $stylist) {
      $user_id = Auth::id();
      // $user_id=1;
      $edit = false;
      if ($request->has('id')) {
        $edit = true;
      }
      $stylist->namastylist = $request->input('namastylist');
      $stylist->statuskerja =  $request->input('statuskerja');
      $stylist->urlgambar = $request->input('urlgambar');
      $stylist->user_id = $user_id;
      $stylist->save();

      if ($request->has('jadwalstylists')) {
        $jadwalstylists = [];
        foreach ($request->input('jadwalstylists') as $key => $jadwal) {
          $jadwalstylist = new Jadwalstylist([
            'harikerja' => $jadwal['harikerja'],
            'jamawalkerja' => $jadwal['jamawalkerja'],
            'jamakhirkerja' => $jadwal['jamakhirkerja'],
            'masuklibur' => $jadwal['masuklibur'],
          ]);
          if (array_key_exists('id', $jadwal)) {
            $jadwalstylist->id = $jadwal['id'];
          };
          $jadwalstylists[] = $jadwalstylist;
        }
        if ($edit) {
          $stylist->jadwalstylists()->forceDelete();
        }
        $stylist->jadwalstylists()->saveMany($jadwalstylists);
      }
    }, 5);
    return $this->getStylistList($request);
  }


  /////////////////////////////////////////// PROMO //////////////////////////////////////////////////////

  public function postPromoEntry(Request $request)
  {
    $request->merge($request->input('data'));
    $this->createLogInfo('postPromoEntry : ' . json_encode($request->all()));
    $promo = new Promo();
    if ($request->has('id')) {
      $promo = Promo::find($request->input('id'));
    }
    DB::transaction(function () use ($request, $promo) {
      $user_id = Auth::id();
      // $user_id=1;
      $edit = false;
      if ($request->has('id')) {
        $edit = true;
      }
      $promo->namapromo = $request->input('namapromo');
      $promo->diskon = $request->input('diskon');
      $promo->waktuawal = $request->input('waktuawal');
      $promo->waktuakhir = $request->input('waktuakhir');
      $promo->user_id = $user_id;
      $promo->save();
    }, 5);
    return $this->getPromoList($request);
  }
  public function getPromoList(Request $request)
  {
    $user_id = Auth::id();
    $input = $request->all();
    $promo = Promo::with(['user'])
      ->where('user_id', '=', $user_id)
      ->orderBy('created_at', 'desc')
      ->get();
    $data = $promo;
    return response()->json($this->setSuccessResponse($data, $input));
  }
  public function getPromoFromId(Request $request)
  {
    $input = $request->all();
    $promo = Promo::find($request->input('id'));
    $data = $promo;
    return response()->json($this->setSuccessResponse($data, $input));
  }
  public function getPromoDelete(Request $request)
  {
    $promo = Promo::find($request->input('id'));
    $promo->delete();
    return $this->getPromoList($request);
  }


  /////////////////////////////////////////////// JADWAL HARIAN //////////////////////////////////////////////////////
  // **** menampilkan daftar jadwal harian customer yang booking barbershop pada barbershop bersangkutan  ******
  public function getJadwalHarianList(Request $request)
  {
    $input = $request->all();
    $bookingheader = BookingHeader::with(['user_customer'])
      ->where('user_merchant_id', '=', Auth::id())
      ->where('status_booking', '=', '1')
      ->orWhere('status_booking', '=', '2')
      // where('user_customer_id', '=', $user_id)
      // // ->orderBy('created_at', 'desc')
      // ->orderBy('statusverifikasi', 'asc')
      ->orderBy('created_at', 'desc')
      // ->orderByRaw("statusverifikasi asc, created_at desc")
      ->get();
    $data = $bookingheader;
    return response()->json($this->setSuccessResponse($data, $input));
  }


  // public function getMerchantRatingKomentarReturn(Request $request)
  // {
  //   $input = $request->all();
  //   $user_id = Auth::id();
  //   $ratingkomentar = Ratingkomentar::with(['user_merchant', 'user_customer', 'booking_headers'])
  //     ->orderBy('created_at', 'desc')
  //     ->find($user_id);
  //   $data = $ratingkomentar;
  //   return response()->json($this->setSuccessResponse($data, $input));
  // }
  // public function getMerchantAvarageRatingKomentarFromId(Request $request)
  // {
  //   $input = $request->all();
  //   $ratingkomentar = Ratingkomentar::with(['user_merchant'])
  //     ->where('user_merchant_id', '=', $request->input('id'))
  //     ->avg('jumlah');
  //   $data = $ratingkomentar;
  //   return response()->json($this->setSuccessResponse($data, $input));
  // }


  ///////////////////////////////////////////////////////////// REPORT /////////////////////////////////////////////////////

  // ***** menympan transaksi booking header dan detail ke database *****
  public function postBarbershopReportCustomerEntry(Request $request)
  {
    $request->merge($request->input('data')); //return list (object)
    $this->createLogInfo('postBarbershopReportCustomerEntry : ' . json_encode($request->all()));
    $report = new Report();

    DB::transaction(function () use ($request, $report) {
      // $mytime = Carbon\Carbon::now();
      // $edit = false;
      // if ($request->has('id')) {
      //   $edit = true;
      // }


      $report->judul = 'Barbershop Report Customer';
      $report->isi = $request->input('isi');
      $report->urlgambar = $request->input('urlgambar');
      $report->user_merchant_id = Auth::id();
      $report->user_customer_id = $request->input('user_customer_id');
      $report->save();
    }, 5);

    return $this->getBarbershopReportCustomerReturn($request);
  }

  public function getBarbershopReportCustomerReturn(Request $request)
  {
    $input = $request->all();
    $user_id = Auth::id();
    $report = Report::with(['user_customer'])
      ->orderBy('created_at', 'desc')
      ->find($user_id);
    $data = $report;
    return response()->json($this->setSuccessResponse($data, $input));
  }

  ////////////////////////////////////////////////// GET DATA RATING & KOMENTAR //////////////////////////////////////////////////////////////////////////
  public function getRatingKomentarList(Request $request)
  {

    $input = $request->all();
    $ratingkomentar = Ratingkomentar::with(['booking_header', 'user_customer'])
      ->where('user_merchant_id', '=', $request->input('user_merchant_id'))
      ->orderBy('created_at', 'desc')
      ->get();
    $data = $ratingkomentar;
    return response()->json($this->setSuccessResponse($data, $input));
  }


  public function getRatingKomentarListFromAuth(Request $request)
  {
    $user_id = Auth::id();
    $input = $request->all();
    $ratingkomentar = Ratingkomentar::with(['booking_header', 'user_customer'])
      ->where('user_merchant_id', '=',  $user_id)
      ->orderBy('created_at', 'desc')
      ->get();
    $data = $ratingkomentar;
    return response()->json($this->setSuccessResponse($data, $input));
  }

  // public function getRatingKomentarCount(Request $request)
  // {

  //   $input = $request->all();
  //   $ratingkomentar = Ratingkomentar::with(['booking_header', 'user_customer'])
  //     ->where('user_merchant_id', '=', $request->input('user_merchant_id'))
  //     ->count();
  //   $data = $ratingkomentar;
  //   return response()->json($this->setSuccessResponse($data, $input));
  // }

  public function getRatingKomentarFromId(Request $request)
  {
    $input = $request->all();
    $ratingkomentar = Ratingkomentar::with(['booking_header', 'user_customer'])
      ->find($request->input('booking_header_id'));
    $data = $ratingkomentar;
    return response()->json($this->setSuccessResponse($data, $input));
  }


  ///////////////////////////////////////////////////////////// INFORMASI  /////////////////////////////////////////////////////
  // public function getInformasiBarbershopList(Request $request)
  // {
  //   $input = $request->all();
  //   // $user_id = Auth::id();
  //   $informasi = Informasi::with(['usermerchant'])
  //     ->where('user_merchant_id', '=', Auth::id())
  //     ->orderBy('created_at', 'desc')
  //     ->get();
  //   $data = $informasi;
  //   return response()->json($this->setSuccessResponse($data, $input));
  // }

  // public function getInformasiBarbershopFromId(Request $request)
  // {
  //   $input = $request->all();
  //   $informasi = Informasi::find($request->input('id'));
  //   $data = $informasi;
  //   return response()->json($this->setSuccessResponse($data, $input));
  // }

}
