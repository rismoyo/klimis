<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BookingDetail;
use App\Models\BookingHeader;
use App\Models\Jadwalstylist;
use App\Models\Service;
use App\Models\Stylist;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;
use DateTime;
use App\Models\User;

class ApiTransaksiBooking extends Controller
{

  // ***** menympan transaksi booking header dan detail ke database *****
  public function postBookingHeaderEntry(Request $request)
  {
    $request->merge($request->input('data')); //return list (object)
    $this->createLogInfo('postBookingHeaderEntry : ' . json_encode($request->all()));
    $bookingheader = new BookingHeader();
    $carbon = Carbon::now();
    // $carbon = new DateTime('2019-12-07 19:22:16');
    // $carbon = $carbon->format('Y-m-d H:i:s');
    $date_today = Carbon::today();
    $last_nota = BookingHeader::where('tanggal_transaksi', '=', $date_today)->count() + 1;
    $kode_nota = $this->generateCode('KMS', $date_today, $last_nota);

    $statusjalan = User::select('statusjalan')->where('id', '=', Auth::id())->value('statusjalan');
    if ($statusjalan == 0) {
      return response('Akun anda di blokir oleh admin !', 500);
    } else {
      //////////////////////////////////////////////////////////PENGECEKAN WAKTU  BOOKING PADA STYLIST ////////////////////////////////////////
      $jamNow = $carbon->toTimeString();
      $dateNow = $carbon->toDateString();
      $Wb = strtotime($request->input('waktu_booking'));
      $FragWaktuBooking = date('H:i:s', $Wb);
      $FragDateBooking = date('Y-m-d', $Wb);


      $total_hour = 0;
      if ($request->has('booking_details')) {
        foreach ($request->input('booking_details') as $key => $car) {
          $service = Service::find($car['service_id']);
          $total_hour = $total_hour + $service->lamaservice;
        }
      }
      ///////////////// cek untuk hari ini dan besok ///////////////////
      $tmp = $date_today->dayOfWeek;
      if ($tmp == 0) {
        $tmp = 7;
      };
      if ($dateNow == $FragDateBooking) {
        $result =  "today";
        $tmp = $tmp;
      } else {
        $result = "tommo";
        $tmp = $tmp + 1;
        if ($tmp == 8) {
          $tmp = 1;
        };
      };

      ///////////////// PENGECEKAN HARI LIBUR STYLIST ///////////////////////////
      $harikerja = Jadwalstylist::select('masuklibur')
        ->where('stylist_id', '=', $request->input('stylist_id'))
        ->where('harikerja', '=', $tmp)
        ->value('masuklibur');
      if ($harikerja == "LIBUR") {
        return response('Stylist Libur, pilih waktu Lain !', 501);
      } else {
        $jadi = 0;

        /////untuk memecah tanggal dan jam yang dipilih ////
        $time = strtotime("1970-01-01" . "T" . $FragWaktuBooking);
        $pickTime = date('Y-m-d H:i:s', $time);

        // echo "pick : " . $pickTime . " | ";
        $pickTime = new DateTime($pickTime);

        /////check jam awal stylist////
        $startTime = Jadwalstylist::select('jamawalkerja')
          ->where('stylist_id', '=', $request->input('stylist_id'))
          ->Where('harikerja', '=', $tmp)
          ->value('jamawalkerja');
        $startTime = (string) $startTime;
        // echo "startTime : " . $startTime . " | ";
        $startTime = new DateTime($startTime);

        //////check jam akhir stylist////////////////////
        $finishTime = Jadwalstylist::select('jamakhirkerja')
          ->where('stylist_id', '=', $request->input('stylist_id'))
          ->Where('harikerja', '=', $tmp)
          ->value('jamakhirkerja');
        $finishTime = (string) $finishTime;
        // echo "finishTime : " . $finishTime . " | ";
        $finishTime = new DateTime($finishTime);
        ////////////////checking, apakah stylist sudah dibooking/////////////
        if ($startTime < $pickTime && $finishTime > $pickTime) {
          $datetime_start = Carbon::createFromFormat('Y-m-d H:i:s', $request->input('waktu_booking'));
          $datetime_end = $datetime_start->addMinutes($request->input('waktu_pengerjaan'));
          $check_booking = DB::select("select waktu_booking `start`,DATE_ADD(waktu_booking, INTERVAL waktu_pengerjaan MINUTE) as `end`,waktu_pengerjaan
          from booking_header 
          where (waktu_booking between '" . $datetime_start->toDateTimeString() . "' and '" . $datetime_end->toDateTimeString() . "' or DATE_ADD(waktu_booking, INTERVAL waktu_pengerjaan MINUTE) between '" . $datetime_start->toDateTimeString() . "' and '" . $datetime_end->toDateTimeString() . "')
          and stylist_id = '" . $request->input('stylist_id') . "' ");
          if ($check_booking) {
            return response('Waktu Booking tidak tersedia', 503);
          }
        } else {
          return response('di luar jam kerja stylist, pilih waktu lain !', 502);
        };
      };
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    // $datetime_start = Carbon::createFromFormat('Y-m-d H:i:s', $request->input('waktu_booking'));
    // $datetime_end = $datetime_start->addMinutes($request->input('waktu_pengerjaan'));
    // $check_booking = DB::select("select waktu_booking `start`,DATE_ADD(waktu_booking, INTERVAL waktu_pengerjaan MINUTE) as `end`,waktu_pengerjaan
    //   from booking_header 
    //   where (waktu_booking between '" . $datetime_start->toDateTimeString() . "' and '" . $datetime_end->toDateTimeString() . "' or DATE_ADD(waktu_booking, INTERVAL waktu_pengerjaan MINUTE) between '" . $datetime_start->toDateTimeString() . "' and '" . $datetime_end->toDateTimeString() . "')
    //   and stylist_id = '" . $request->input('stylist_id') . "' ");



    // if ($check_booking) {
    //   return response('Pilih Waktu Booking yang lain', 503);
    //   // return response()->json($this->setErrorResponse([], [], 502, 'TEST'), 502);
    // }



    //////////////////////////

    DB::transaction(function () use ($request, $bookingheader, $kode_nota, $date_today) {
      $edit = false;
      if ($request->has('id')) {
        $edit = true;
      }

      // ["mulai" => $datetime_start->toDateTimeString(), "akhir" => $datetime_end->toDateTimeString(), "stylist_id" => $request->input('stylist_id')]
      $bookingheader->tanggal_transaksi  = $date_today;
      $bookingheader->kode_nota  = $kode_nota;
      $bookingheader->waktu_booking  = $request->input('waktu_booking');
      $bookingheader->total_booking  = $request->input('total_booking');
      $bookingheader->total_dp = $request->input('total_dp');
      $bookingheader->status_booking = $request->input('status_booking');
      $bookingheader->status_dp = $request->input('status_dp');
      $bookingheader->status_lunas = $request->input('status_lunas');

      $bookingheader->tanggal_dp = $date_today;
      // $bookingheader->tanggal_lunas = $request->input('tanggal_lunas');
      $bookingheader->sisa_pembayaran = $request->input('sisa_pembayaran');
      $bookingheader->biaya_lain = $request->input('biaya_lain');
      $bookingheader->total_potongan = $request->input('total_potongan');
      $bookingheader->grand_total = $request->input('grand_total');
      $bookingheader->deposit = $request->input('deposit');
      $bookingheader->alamat_home_services = $request->input('alamat_home_services');
      $bookingheader->alamat_barbershop_services = $request->input('alamat_barbershop_services');
      $bookingheader->tipebooking = $request->input('tipebooking');
      $bookingheader->waktu_pengerjaan = $request->input('waktu_pengerjaan');
      $bookingheader->keterangan = $request->input('keterangan');
      $bookingheader->user_customer_id = Auth::id();
      $bookingheader->user_merchant_id = $request->input('user_merchant_id');
      // $bookingheader->rating_komentar_id = 1;
      $bookingheader->stylist_id = $request->input('stylist_id');
      $bookingheader->promo_id = 1;
      $bookingheader->save();

      //pengurangan saldo
      $user_id = Auth::id();
      $user = User::find($user_id);
      $user->saldo = $user->saldo - $request->input('total_dp');
      $user->save();


      if ($request->has('booking_details')) {
        $bookingdetails = [];
        foreach ($request->input('booking_details') as $key => $car) {
          $bookingdetail = new BookingDetail([
            'harga' => $car['harga'],
            'service_id' => $car['service_id'],
          ]);
          if (array_key_exists('id', $car)) {
            $bookingdetail->id = $car['id'];
          };
          $bookingdetails[] = $bookingdetail;
        }
        $bookingheader->booking_details()->saveMany($bookingdetails);
      }
    }, 5);

    return $this->getCustomerTransaksiBookingList($request);
  }



  // ** CANCEL BOOKING  (dimana terjadi proses pengembalian/ penambahan saldo user customer ) ****
  public function postCancelBookingData(Request $request)
  {
    $input = $request->all();

    $bookingheader = BookingHeader::find($request->input('id'));;
    $bookingheader->status_booking = 4;
    $bookingheader->save();

    $user = User::find($bookingheader->user_customer_id);
    $user->saldo = $user->saldo + $bookingheader->total_dp;
    $user->save();

    // return $this->setSuccessResponse($request);
    return response()->json($this->setSuccessResponse($input));
  }



  // ** TERIMA BOOKING  (dimana terjadi proses pengurangan saldo user custome) ****
  public function postBarbershopTerimaBookingData(Request $request)
  {
    $input = $request->all();

    $bookingheader = BookingHeader::find($request->input('id'));;
    $bookingheader->status_booking = 1;
    $bookingheader->save();
    $data = $bookingheader;

    // return $this->setSuccessResponse($request);
    return response()->json($this->setSuccessResponse($data, $input));


    // return $this->getCustomerTransaksiBookingList($request);
  }

  // *** Mulai Pengerjaan BOOKING  ****
  public function postMulaiPengerjaanBookingData(Request $request)
  {
    $input = $request->all();

    $bookingheader = BookingHeader::find($request->input('id'));;
    $bookingheader->status_booking = 1;
    $bookingheader->save();
    $data = $bookingheader;

    // $user = User::find($bookingheader->user_customer_id);

    // $this->sendNotificationToDevice('Mulai Pengerjaan', 'Konfirmasi Mulai Pengerjaan', $user->firebase_fcm_token);

    // return $this->setSuccessResponse($request);
    return response()->json($this->setSuccessResponse($data, $input));

    // return $this->getCustomerTransaksiBookingList($request);
  }

  // *** Selesai Pengerjaan BOOKING  ****
  public function postSelesaiPengerjaanBookingData(Request $request)
  {
    $input = $request->all();
    $tgl_lunas = Carbon::today();
    $bookingheader = BookingHeader::find($request->input('id'));;
    $bookingheader->status_booking = 2;
    // $bookingheader->status_lunas = 1;
    $bookingheader->tanggal_lunas = $tgl_lunas;
    $bookingheader->save();

    // $user = User::find($bookingheader->user_merchant_id);
    // $user->saldo = $user->saldo + $bookingheader->grand_total;
    // $user->save();

    // $user = User::find($bookingheader->user_customer_id);
    // $user->saldo = $user->saldo - $bookingheader->sisa_pembayaran;
    // $user->save();

    // return $this->setSuccessResponse($request);
    return response()->json($this->setSuccessResponse($input));


    // return $this->getCustomerTransaksiBookingList($request);
  }


  // *** KONFIRMASI CUSTOMER Mulai Pengerjaan BOOKING  ****
  public function postKonfirmasiCustomerMulaiPengerjaanBookingData(Request $request)
  {
    $input = $request->all();

    $bookingheader = BookingHeader::find($request->input('id'));;
    $bookingheader->status_booking = 2;
    $bookingheader->save();
    $data = $bookingheader;

    $usercustomer = User::find($bookingheader->user_customer_id);
    $usermerchant = User::find($bookingheader->user_merchant_id);

    $this->sendNotificationToDevice('Mulai Pengerjaan', 'Konfirmasi Mulai Pengerjaan', $usercustomer->firebase_fcm_token);

    $this->sendNotificationToDevice('Mulai Pengerjaan', 'Konfirmasi Mulai Pengerjaan', $usermerchant->firebase_fcm_token);

    // return $this->setSuccessResponse($request);
    return response()->json($this->setSuccessResponse($data, $input));

    // return $this->getCustomerTransaksiBookingList($request);
  }


  // *** KONFIRMASI CUSTOMER SELESAI Pengerjaan BOOKING  ****
  public function postKonfirmasiCustomerSelesaiPengerjaanBookingData(Request $request)
  {
    $input = $request->all();
    $tgl_lunas = Carbon::today();
    $bookingheader = BookingHeader::find($request->input('id'));;
    $bookingheader->status_booking = 3;
    $bookingheader->status_lunas = 1;
    $bookingheader->tanggal_lunas = $tgl_lunas;
    $bookingheader->save();

    $user = User::find($bookingheader->user_merchant_id);
    $user->saldo = $user->saldo + $bookingheader->grand_total;
    $user->save();

    $user = User::find($bookingheader->user_customer_id);
    $user->saldo = $user->saldo - $bookingheader->sisa_pembayaran;
    $user->save();

    $usercustomer = User::find($bookingheader->user_customer_id);
    $usermerchant = User::find($bookingheader->user_merchant_id);

    $this->sendNotificationToDevice('Selesai Pengerjaan', 'Konfirmasi Selesai Pengerjaan', $usercustomer->firebase_fcm_token);

    $this->sendNotificationToDevice('Selesai Pengerjaan', 'Konfirmasi Selesai Pengerjaan', $usermerchant->firebase_fcm_token);


    // return $this->setSuccessResponse($request);
    return response()->json($this->setSuccessResponse($input));


    // return $this->getCustomerTransaksiBookingList($request);
  }




  // public function postSuksesBookingData(Request $request)
  // {
  //   $input = $request->all();

  //   $bookingheader = BookingHeader::find($request->input('id'));;
  //   $bookingheader->status_booking = 3;
  //   $bookingheader->save();

  //   $user = User::find($bookingheader->user_merchant_id);
  //   $user->saldo = $user->saldo + $bookingheader->grand_total;
  //   $user->save();

  //   $user = User::find($bookingheader->user_customer_id);
  //   $user->saldo = $user->saldo - $bookingheader->sisa_pembayaran;
  //   $user->save();
  //   // return $this->setSuccessResponse($request);
  //   return response()->json($this->setSuccessResponse($input));
  // }

  ///////////////////////////////////////////////////////////////////////////////////////////////////


  // **** menampilkan daftar transaksi booking yang dilakukan customer  ******
  public function getCustomerTransaksiBookingList(Request $request)
  {
    $input = $request->all();
    // $user_customer_id = Auth::id();
    $bookingheader = BookingHeader::with(['user_merchant'])
      ->where('user_customer_id', '=', Auth::id())
      // where('user_customer_id', '=', $user_id)
      // // ->orderBy('created_at', 'desc')
      // ->orderBy('statusverifikasi', 'asc')
      ->orderBy('created_at', 'desc')
      // ->orderByRaw("statusverifikasi asc, created_at desc")
      ->get();
    $data = $bookingheader;
    return response()->json($this->setSuccessResponse($data, $input));
  }



  // **** menampilkan data transaksi booking yang dilakukan barbershop pada detail transaksi  ******
  public function getCustomerTransaksiBookingFromId(Request $request)
  {
    $input = $request->all();
    $bookingheader = BookingHeader::with(['booking_details.service', 'user_customer', 'user_merchant', 'stylist', 'promo'])
      ->find($request->input('id'));

    // $bookingdetail = BookingDetail::with(['booking_header', 'service'])
    //   // ->find($request->input('id'));
    //   ->where('booking_header_id', '=', $request->input('id'))
    //   ->orderBy('created_at', 'desc')
    //   ->get();

    $data = [
      'bookingheader' => $bookingheader,
      // 'bookingdetail' => $bookingdetail,
    ];
    return response()->json($this->setSuccessResponse($data, $input));
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  // **** menampilkan daftar transaksi booking yang dilakukan merchant barbershop ******
  public function getBarbershopTransaksiBookingList(Request $request)
  {
    $input = $request->all();
    // $user_customer_id = Auth::id();
    $bookingheader = BookingHeader::with(['user_customer'])
      ->where('user_merchant_id', '=', Auth::id())
      // where('user_customer_id', '=', $user_id)
      // // ->orderBy('created_at', 'desc')
      // ->orderBy('statusverifikasi', 'asc')
      ->orderBy('created_at', 'desc')
      // ->orderByRaw("statusverifikasi asc, created_at desc")
      ->get();
    $data = $bookingheader;
    return response()->json($this->setSuccessResponse($data, $input));
  }




  // **** menampilkan data transaksi booking pada barbershop  pada detail transaksi******
  public function getBarbershopTransaksiBookingFromId(Request $request)
  {

    $input = $request->all();
    $bookingheader = BookingHeader::with(['booking_details.service', 'user_customer', 'user_merchant', 'stylist'])
      ->find($request->input('id'));

    $data = [
      'bookingheader' => $bookingheader,
    ];

    return response()->json($this->setSuccessResponse($data, $input));
  }


  // public function getCheckingWorkingHour(Request $request)
  // {
  //   $stylist = $request->input('getidstylist');
  //   $jamPicker = $request->input('gettimePicker');
  //   $datePicker = $request->input('getdatePicker');
  //   $lamapengerjaan = $request->input('getlama');

  //   $carbon = Carbon::now();
  //   // $carbon = new DateTime('2019-12-07 19:22:16');
  //   // $carbon = $carbon->format('Y-m-d H:i:s');
  //   $jamNow = $carbon->toTimeString();
  //   $dateNow = $carbon->toDateString();


  //   $tmp = $carbon->dayOfWeek;
  //   if ($tmp == 0) {
  //     $tmp = 7;
  //   };
  //   if ($dateNow == $datePicker) {
  //     $result =  "today";
  //     $tmp = $tmp;
  //   } else {
  //     $result = "tommo";
  //     $tmp = $tmp + 1;
  //     if ($tmp == 8) {
  //       $tmp = 1;
  //     };
  //   };
  //   $harikerja = Jadwalstylist::select('masuklibur')
  //     ->where('stylist_id', '=', $stylist)
  //     ->where('harikerja', '=', $tmp)
  //     ->get();

  //   if ($harikerja == "LIBUR") {

  //     echo "| tai cuk |";
  //     return 0;
  //   } else {
  //     $jadi = 0;

  //     /////check jam yang dipilih////
  //     $time = strtotime("1970-01-01" . "T" . $jamPicker);
  //     $pickTime = date('Y-m-d H:i:s', $time);

  //     // echo "pick : " . $pickTime . " | ";
  //     $pickTime = new DateTime($pickTime);

  //     /////check jam awal stylist////
  //     $startTime = Jadwalstylist::select('jamawalkerja')
  //       ->where('stylist_id', '=', $request->input('getidstylist'))
  //       ->Where('harikerja', '=', $tmp)
  //       ->value('jamawalkerja');
  //     $startTime = (string) $startTime;
  //     // echo "startTime : " . $startTime . " | ";
  //     $startTime = new DateTime($startTime);

  //     //////check jam akhir stylist///
  //     $finishTime = Jadwalstylist::select('jamakhirkerja')
  //       ->where('stylist_id', '=', $request->input('getidstylist'))
  //       ->Where('harikerja', '=', $tmp)
  //       ->value('jamakhirkerja');
  //     $finishTime = (string) $finishTime;
  //     // echo "finishTime : " . $finishTime . " | ";
  //     $finishTime = new DateTime($finishTime);
  //     //check jam kerja stylist
  //     if ($startTime < $pickTime && $finishTime > $pickTime) {
  //       $treatment = BookingHeader::select('id')
  //         ->where('stylist_id', '=', $request->input('getidstylist'))
  //         ->where('waktu_booking', 'like', '%' . $datePicker . '%')
  //         ->where(function ($query) {
  //           $query->Where('status_booking', '=', 1)
  //             ->orWhere('status_booking', '=', 2)
  //             ->orWhere('status_booking', '=', 3)
  //             ->orWhere('status_booking', '=', 0);
  //         })
  //         ->get();
  //       if (count($treatment) == 0) { //  check stylist, apakah sedang melayani orang lain?
  //         return 1;
  //       } else {
  //         for ($i = 0; $i <= count($treatment) - 1; $i++) {
  //           //echo $treatment->values()[$i]["id"];
  //           $waktubooking = BookingHeader::select('waktu_booking')
  //             ->where('id', '=', $treatment->values()[$i]["id"])
  //             ->value('waktu_booking');
  //           $dateNowtmp = $waktubooking->toDateString();

  //           // if ($dateNow == $dateNowtmp) {
  //           $lamap = BookingHeader::select('waktu_pengerjaan')
  //             ->where('id', '=', $treatment->values()[$i]["id"])
  //             ->value('waktu_pengerjaan');

  //           $dateTime1 = new DateTime($waktubooking);
  //           $dateTime1 = $dateTime1->format('Y-m-d H:i:s');

  //           $dateTime2 = new DateTime($waktubooking);
  //           $dateTime2->modify("+{$lamap} minutes");
  //           $dateTime2 = $dateTime2->format('Y-m-d H:i:s');

  //           $time = strtotime($datePicker . "T" . $jamPicker);
  //           $pickTime = date('Y-m-d H:i:s', $time);
  //           $pickTime = new DateTime($pickTime);
  //           $pickTime = $pickTime->format('Y-m-d H:i:s');

  //           echo count($treatment);
  //           echo $dateTime1 . " | " . $dateTime2 . " | " . $pickTime;
  //           if ($dateTime1 <= $pickTime && $dateTime2 >= $pickTime) {
  //             //current time is between morning and evening
  //             $jadi = 0;
  //             // echo "| gagal |";
  //           } else {
  //             //current time is earlier than morning or later than evening
  //             //cek overlap
  //             // $time = strtotime($datePicker . "T" . $jamPicker);
  //             // $pickTime2 = date('Y-m-d H:i:s', $time);
  //             // $pickTime2 = new DateTime($pickTime2);
  //             // $pickTime2 = $pickTime2->modify('+' . $lamapengerjaan . ' minutes');
  //             // if ($pickTime2 > $dateTime1) {
  //             //   $pickTime2 = $pickTime2->format('Y-m-d H:i:s');
  //             //   $dateTime1 = $dateTime1->format('Y-m-d H:i:s');
  //             //   echo $pickTime2;
  //             //   $jadi = 0;
  //             //   echo "| gagal2 |";
  //             //   echo $dateTime1;
  //             // } else {
  //             $jadi = 1;
  //             // echo "| masuk |";
  //             break;
  //             // };
  //           }
  //           // } else {
  //           $jadi = 0;
  //           // echo "| tai |";
  //           // };
  //         };
  //       };
  //     } else {
  //       // echo "| asu |";
  //       return 0;
  //     };

  //     if ($jadi == 1) {
  //       echo "| Stylist Bisa dibooking |";
  //       return 1;
  //     } else {
  //       echo "| Stylist Tidak Bisa dibooking|";
  //       return 0;
  //     };
  //   };
  // }
}
