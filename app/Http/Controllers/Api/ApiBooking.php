<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\BookingHeader;
use App\Models\BookingDetail;

use App\Models\Stylist;

class ApiBooking extends Controller
{
  public function postBookingEntry(Request $request)
  {
    // $request->merge($request->input('data'));
    $this->createLogInfo('postBookingEntry : ' . json_encode($request->all()));
    $bookingheader = new BookingHeader();
    if ($request->has('id')) {
      $bookingheader = BookingHeader::find($request->input('id'));
    }
    DB::transaction(function () use ($request, $bookingheader) {
      $user_id = Auth::id();
      // $user_id=1;
      $edit = false;
      if ($request->has('id')) {
        $edit = true;
      }
      $bookingheader->kode_nota  = $request->input('kode_nota');
      $bookingheader->tanggal_transaksi = $request->input('tanggal_transaksi');
      $bookingheader->waktu_booking = $request->input('waktu_booking');
      $bookingheader->total_booking = $request->input('total_booking');
      $bookingheader->total_dp = $request->input('total_dp');
      $bookingheader->status_booking = $request->input('status_booking');
      $bookingheader->status_dp = $request->input('status_dp');
      $bookingheader->status_lunas = $request->input('status_lunas');
      $bookingheader->tanggal_dp = $request->input('tanggal_dp');
      $bookingheader->tanggal_lunas = $request->input('tanggal_lunas');
      $bookingheader->sisa_pembayaran = $request->input('sisa_pembayaran');
      $bookingheader->biaya_lain = $request->input('biaya_lain');
      $bookingheader->total_potongan = $request->input('total_potongan');
      $bookingheader->grand_total = $request->input('grand_total');
      $bookingheader->deposit = $request->input('deposit');
      $bookingheader->alamat_home_services = $request->input('alamat_home_services');
      $bookingheader->waktu_pengerjaan = $request->input('waktu_pengerjaan');
      $bookingheader->keterangan = $request->input('keterangan');
      $bookingheader->user_customer_id = $user_id;
      $bookingheader->user_merchant_id = $user_id;
      $bookingheader->save();
    }, 5);
    return $this->getBookingList($request);
  }

  public function getBookingList(Request $request)
  {
    $input = $request->all();
    $bookingheader = BookingHeader::with(['promo', 'rating', 'stylist', 'user_customer', 'user_merchant', 'booking_details.service', 'historypembayarans'])
      ->orderBy('created_at', 'desc')
      ->get();
    $data = $bookingheader;
    return response()->json($this->setSuccessResponse($data, $input));
  }
  public function getBookingFromId(Request $request)
  {
    $input = $request->all();
    $bookingheader = BookingHeader::with(['promo', 'rating', 'stylist', 'user_customer', 'user_merchant', 'booking_details.service', 'historypembayarans'])
      ->find($request->input('id'));
    $data = $bookingheader;
    return response()->json($this->setSuccessResponse($data, $input));
  }

  public function postBookingDetailEntry(Request $request)
  {
    // $request->merge($request->input('data'));
    $this->createLogInfo('postBookingEntry : ' . json_encode($request->all()));
    $bookingdetail = new BookingDetail();
    if ($request->has('id')) {
      $bookingdetail = BookingHeader::find($request->input('id'));
    }
    DB::transaction(function () use ($request, $bookingdetail) {
      $user_id = Auth::id();
      // $user_id=1;
      $edit = false;
      if ($request->has('id')) {
        $edit = true;
      }
      $bookingdetail->harga  = $request->input('kode_nota');
      $bookingdetail->user_id = $user_id;
      $bookingdetail->save();
    }, 5);
    return $this->getBookingList($request);
  }

  public function getBookingDetailList(Request $request)
  {
    $input = $request->all();
    $bookingdetail = BookingDetail::with(['user'])
      ->orderBy('created_at', 'desc')
      ->get();
    $data = $bookingdetail;
    return response()->json($this->setSuccessResponse($data, $input));
  }
  public function getBookingDetailFromId(Request $request)
  {
    $input = $request->all();
    $bookingdetail = BookingDetail::find($request->input('id'));
    $data = $bookingdetail;
    return response()->json($this->setSuccessResponse($data, $input));
  }

  public function getBookingJadwalStylistFromId(Request $request)
  {
    $input = $request->all();
    $stylist = Stylist::with(['jadwalstylists','user','bookingheaders','services'])
    ->find($request->input('merchant_id'))
    ->orderBy('created_at','desc')
    ->get();
    $data = $stylist;
    return response()->json($this->setSuccessResponse($data, $input));
  } 

  public function getBookingJadwalStylistList(Request $request)
  {
    $input = $request->all();
    $stylist = Stylist::with(['jadwalstylists','user','bookingheaders','services'])
    ->find($request->input('merchant_id'))
    ->orderBy('created_at','desc')
    ->get();
    $data = $stylist;
    return response()->json($this->setSuccessResponse($data, $input));

}
}