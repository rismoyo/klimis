<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;
use App\Models\Transaksisaldo;
use App\Models\User;

class ApiTransaksiSaldo extends Controller
{
  public function addtopupsaldo(Request $request)
  {
    $request->merge($request->input('data')); //return list (object)
    $this->createLogInfo('addtopupsaldo : ' . json_encode($request->all()));
    $transaksisaldo = new Transaksisaldo();



    // $fcmtokenuser = DB::table('user')->select('firebase_fcm_token')->where('id', '=', 18)->get();
    // $titletext = 'Topup';
    // $bodytext = 'Anda Berhasil Topup';
    // $this->sendNotificationToDevice($titletext, $bodytext, $fcmtokenuser);

    DB::transaction(function () use ($request, $transaksisaldo) {

      $user_id = Auth::id();
      $transaksisaldo->tipetransaksi = "TOPUP";
      $transaksisaldo->nominal  = $request->input('nominal');
      $transaksisaldo->rekening = $request->input('rekening');
      $transaksisaldo->statusverifikasi = 0;
      $transaksisaldo->urlbuktibayar = $request->input('urlbuktibayar');
      $transaksisaldo->user_id = $user_id;
      $transaksisaldo->save();
    }, 5);
    return $this->getTopupSaldoList($request);
  }

  public function getTopupSaldoList(Request $request)
  {
    $input = $request->all();
    $user_id = Auth::id();
    $transaksisaldo = Transaksisaldo::with(['user'])
      ->where('user_id', '=', $user_id)
      // // ->orderBy('created_at', 'desc')
      // ->orderBy('statusverifikasi', 'asc')
      ->orderBy('created_at', 'desc')
      // ->orderByRaw("statusverifikasi asc, created_at desc")
      ->get();
    $data = $transaksisaldo;
    return response()->json($this->setSuccessResponse($data, $input));
  }


  public function getTopupSaldoFromId(Request $request)
  {
    $input = $request->all();
    $transaksisaldo = Transaksisaldo::with(['user'])
      ->find($request->input('id'))
      ->orderBy('created_at', 'desc')
      ->get();
    $data = $transaksisaldo;
    return response()->json($this->setSuccessResponse($data, $input));
  }

  public function addWithdrawSaldo(Request $request)
  {
    $request->merge($request->input('data')); //return list (object)
    $this->createLogInfo('addWithdrawSaldo : ' . json_encode($request->all()));
    $transaksisaldo = new Transaksisaldo();

    DB::transaction(function () use ($request, $transaksisaldo) {
      $user_id = Auth::id();
      $transaksisaldo->tipetransaksi = "WITHDRAW";
      $transaksisaldo->nominal  = $request->input('nominal');
      $transaksisaldo->rekening = $request->input('rekening');
      $transaksisaldo->statusverifikasi = 0;
      // $transaksisaldo->urlbuktibayar = $request->input('urlbuktibayar');
      $transaksisaldo->user_id = $user_id;
      $transaksisaldo->save();
    }, 5);
    return $this->getTopupSaldoList($request);
  }
}
