<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 19 Nov 2019 18:55:38 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Informasi
 * 
 * @property int $id
 * @property string $judul
 * @property string $isi
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $user_customer_id
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Informasi extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'informasi';

	protected $casts = [
		'user_customer_id' => 'int'
	];

	protected $fillable = [
		'judul',
		'isi',
		'user_customer_id'
	];

	public function usercustomer()
	{
		return $this->belongsTo(\App\Models\User::class, 'user_customer_id');
	}
}
