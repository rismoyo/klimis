<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Feb 2019 21:05:07 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Promo
 * 
 * @property int $id
 * @property string $namapromo
 * @property int $diskon
 * @property \Carbon\Carbon $waktuawal
 * @property \Carbon\Carbon $waktuakhir
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $user_id
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $bookings
 *
 * @package App\Models
 */
class Promo extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'promo';

	protected $casts = [
		'diskon' => 'int',
		'user_id' => 'int'
	];

	protected $dates = [
		'waktuawal',
		'waktuakhir'
	];

	protected $fillable = [
		'namapromo',
		'diskon',
		'waktuawal',
		'waktuakhir',
		'user_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function bookingheaders()
	{
		return $this->hasMany(\App\Models\BookingDetail::class);
	}
}
