<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Feb 2019 21:05:07 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Transaksisaldo
 * 
 * @property int $id
 * @property string $tipetransaksi
 * @property int $nominal
 * @property string $rekening
 * @property int $statusverifikasi
 * @property string $urlbuktibayar
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $user_id
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Transaksisaldo extends Eloquent
{
	protected $table = 'transaksisaldo';

	protected $casts = [
		'nominal' => 'int',
		'statusverifikasi' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'tipetransaksi',
		'nominal',
		'rekening',
		'statusverifikasi',
		'urlbuktibayar',
		'user_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
