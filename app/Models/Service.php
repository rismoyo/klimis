<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 26 May 2019 14:02:18 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Service
 * 
 * @property int $id
 * @property string $namaservice
 * @property int $tarif
 * @property string $keterangan
 * @property string $tipeservice
 * @property \Carbon\Carbon $created_at
 * @property string $deleted_at
 * @property \Carbon\Carbon $updated_at
 * @property int $user_id
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $bookings
 * @property \Illuminate\Database\Eloquent\Collection $stylists
 *
 * @package App\Models
 */
class Service extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'service';

	protected $casts = [
		'tarif' => 'int',
		'lamaservice' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'namaservice',
		'tarif',
		'lamaservice',
		'keterangan',
		'tipeservice',
		'user_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function bookingheaders()
	{
		return $this->hasMany(\App\Models\BookingHeader::class);
	}

	public function bookingdetails()
	{
		return $this->hasMany(\App\Models\BookingDetail::class);
	}

	public function stylists()
	{
		return $this->belongsToMany(\App\Models\Stylist::class, 'stylistservices')
			->withPivot('service_id', 'id');
	}
}
