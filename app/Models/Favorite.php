<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 16 Jul 2019 16:28:37 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Favorite
 * 
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $usermerchant_id
 * @property int $usercustomer_id
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Favorite extends Eloquent
{
	protected $table = 'favorite';
	public $incrementing = false;

	protected $casts = [
		'id' => 'int',
		'usermerchant_id' => 'int',
		'usercustomer_id' => 'int'
	];

	protected $fillable = [
		'usermerchant_id',
		'usercustomer_id'
	];

	public function usermerchant()
	{
		// return $this->belongsTo(\App\Models\User::class);
		return $this->belongsTo(\App\Models\User::class, 'usermerchant_id');
	}

	public function usercustomer()
	{
		// return $this->belongsTo(\App\Models\User::class);
		return $this->belongsTo(\App\Models\User::class, 'usercustomer_id');
	}
}
