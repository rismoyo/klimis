<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 26 May 2019 14:02:27 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Stylist
 * 
 * @property int $id
 * @property string $namastylist
 * @property int $statuskerja
 * @property string $urlgambar
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $user_id
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $bookings
 * @property \Illuminate\Database\Eloquent\Collection $jadwalstylists
 * @property \Illuminate\Database\Eloquent\Collection $services
 *
 * @package App\Models
 */
class Stylist extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'stylist';

	protected $casts = [
		'statuskerja' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'namastylist',
		'statuskerja',
		'urlgambar',
		'user_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function bookingheaders()
	{
		return $this->hasMany(\App\Models\BookingHeader::class);
	}

	public function jadwalstylists()
	{
		return $this->hasMany(\App\Models\Jadwalstylist::class);
	}

	public function services()
	{
		return $this->belongsToMany(\App\Models\Service::class, 'stylistservices')
			->withPivot('stylist_id', 'id');
	}
}
