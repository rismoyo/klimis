<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Feb 2019 21:05:07 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Jadwalbarber
 * 
 * @property int $id
 * @property \Carbon\Carbon $jambuka
 * @property \Carbon\Carbon $jamtutup
 * @property string $hari
 * @property string $keterangan
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $user_id
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Jadwalbarber extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'jadwalbarber';

	protected $casts = [
		'user_id' => 'int'
	];

	protected $dates = [
		'jambuka',
		'jamtutup'
	];

	protected $fillable = [
		'jambuka',
		'jamtutup',
		'hari',
		'bukatutup',
		'user_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
