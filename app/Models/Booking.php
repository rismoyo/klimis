<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Feb 2019 21:05:06 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Booking
 * 
 * @property int $id
 * @property \Carbon\Carbon $waktubooking
 * @property int $totalbooking
 * @property float $totaldp
 * @property int $statusbooking
 * @property int $statusdp
 * @property int $statuslunas
 * @property \Carbon\Carbon $tanggaldp
 * @property \Carbon\Carbon $tanggallunas
 * @property float $sisapembayaran
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $user_id
 * @property int $stylist_id
 * @property int $promo_id
 * @property int $rating_id
 * @property int $service_id
 * 
 * @property \App\Models\Promo $promo
 * @property \App\Models\Rating $rating
 * @property \App\Models\Service $service
 * @property \App\Models\Stylist $stylist
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $historypembayarans
 *
 * @package App\Models
 */
class Booking extends Eloquent
{
	protected $table = 'booking';

	protected $casts = [
		'totalbooking' => 'int',
		'totaldp' => 'float',
		'statusbooking' => 'int',
		'statusdp' => 'int',
		'statuslunas' => 'int',
		'sisapembayaran' => 'float',
		'user_id' => 'int',
		'stylist_id' => 'int',
		'promo_id' => 'int',
		'rating_id' => 'int',
		'service_id' => 'int'
	];

	protected $dates = [
		'waktubooking',
		'tanggaldp',
		'tanggallunas'
	];

	protected $fillable = [
		'waktubooking',
		'totalbooking',
		'totaldp',
		'statusbooking',
		'statusdp',
		'statuslunas',
		'tanggaldp',
		'tanggallunas',
		'sisapembayaran',
		'user_id',
		'stylist_id',
		'promo_id',
		'rating_id',
		'service_id'
	];

	public function promo()
	{
		return $this->belongsTo(\App\Models\Promo::class);
	}

	public function rating()
	{
		return $this->belongsTo(\App\Models\Rating::class);
	}

	public function service()
	{
		return $this->belongsTo(\App\Models\Service::class);
	}

	public function stylist()
	{
		return $this->belongsTo(\App\Models\Stylist::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function historypembayarans()
	{
		return $this->hasMany(\App\Models\Historypembayaran::class);
	}
}
