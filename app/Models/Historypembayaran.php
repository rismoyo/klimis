<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Feb 2019 21:05:07 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Historypembayaran
 * 
 * @property int $id
 * @property float $totalpembayaran
 * @property \Carbon\Carbon $tanggalpembayaran
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $update_at
 * @property int $booking_id
 * 
 * @property \App\Models\Booking $booking
 *
 * @package App\Models
 */
class Historypembayaran extends Eloquent
{
	protected $table = 'historypembayaran';
	public $timestamps = false;

	protected $casts = [
		'totalpembayaran' => 'float',
		'booking_header_id' => 'int'
	];

	protected $dates = [
		'tanggalpembayaran',
		'update_at'
	];

	protected $fillable = [
		'totalpembayaran',
		'tanggalpembayaran',
		'update_at',
		'booking_header_id'
	];

	public function booking_header()
	{
		return $this->belongsTo(\App\Models\BookingHeader::class);
	}
}
