<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 12 Nov 2019 16:37:02 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BookingHeader
 * 
 * @property int $id
 * @property string $kode_nota
 * @property \Carbon\Carbon $tanggal_transaksi
 * @property \Carbon\Carbon $waktu_booking
 * @property int $total_booking
 * @property float $total_dp
 * @property int $status_booking
 * @property int $status_dp
 * @property int $status_lunas
 * @property \Carbon\Carbon $tanggal_dp
 * @property \Carbon\Carbon $tanggal_lunas
 * @property float $sisa_pembayaran
 * @property float $biaya_lain
 * @property float $total_potongan
 * @property float $grand_total
 * @property float $deposit
 * @property string $alamat_home_services
 * @property string $alamat_barbershop_services
 * @property string $tipebooking
 * @property int $waktu_pengerjaan
 * @property string $keterangan
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $stylist_id
 * @property int $promo_id
 * @property int $user_customer_id
 * @property int $user_merchant_id
 * 
 * @property \App\Models\Promo $promo
 * @property \App\Models\Stylist $stylist
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $booking_details
 * @property \Illuminate\Database\Eloquent\Collection $historypembayarans
 *
 * @package App\Models
 */
class BookingHeader extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'booking_header';

	protected $casts = [
		'total_booking' => 'int',
		'total_dp' => 'float',
		'status_booking' => 'int',
		'status_dp' => 'int',
		'status_lunas' => 'int',
		'sisa_pembayaran' => 'float',
		'biaya_lain' => 'float',
		'total_potongan' => 'float',
		'grand_total' => 'float',
		'deposit' => 'float',
		'stylist_id' => 'int',
		'promo_id' => 'int',
		'user_customer_id' => 'int',
		'user_merchant_id' => 'int',
		'waktu_pengerjaan' => 'int'
	];

	protected $dates = [
		'tanggal_transaksi',
		'waktu_booking',
		'tanggal_dp',
		'tanggal_lunas'
	];

	protected $fillable = [
		'kode_nota',
		'tanggal_transaksi',
		'waktu_booking',
		'total_booking',
		'total_dp',
		'status_booking',
		'status_dp',
		'status_lunas',
		'tanggal_dp',
		'tanggal_lunas',
		'sisa_pembayaran',
		'biaya_lain',
		'total_potongan',
		'grand_total',
		'deposit',
		'alamat_home_services',
		'alamat_barbershop_services',
		'tipebooking',
		'waktu_pengerjaan',
		'keterangan',
		'stylist_id',
		'promo_id',
		'user_customer_id',
		'user_merchant_id'
	];

	public function promo()
	{
		return $this->belongsTo(\App\Models\Promo::class);
	}

	public function stylist()
	{
		return $this->belongsTo(\App\Models\Stylist::class);
	}

	public function user_customer()
	{
		return $this->belongsTo(\App\Models\User::class, 'user_customer_id');
	}

	public function user_merchant()
	{
		return $this->belongsTo(\App\Models\User::class, 'user_merchant_id');
	}

	public function booking_details()
	{
		return $this->hasMany(\App\Models\BookingDetail::class);
	}

	public function historypembayarans()
	{
		return $this->hasMany(\App\Models\Historypembayaran::class);
	}
}
