<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Feb 2019 21:05:07 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Jadwalstylist
 * 
 * @property int $id
 * @property \Carbon\Carbon $jamawalkerja
 * @property \Carbon\Carbon $jamakhirkerja
 * @property string $harikerja
 * @property string $keterangan
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $stylist_id
 * 
 * @property \App\Models\Stylist $stylist
 *
 * @package App\Models
 */
class Jadwalstylist extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'jadwalstylist';

	protected $casts = [
		'stylist_id' => 'int',
		'harikerja' => 'int',
	];

	protected $dates = [
		'jamawalkerja',
		'jamakhirkerja'
	];

	protected $fillable = [
		'jamawalkerja',
		'jamakhirkerja',
		'masuklibur',
		'stylist_id',
		'harikerja'
	];

	public function stylist()
	{
		return $this->belongsTo(\App\Models\Stylist::class);
	}
}
