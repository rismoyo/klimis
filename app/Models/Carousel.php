<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Feb 2019 21:05:07 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Carousel
 * 
 * @property int $id
 * @property string $namacarousel
 * @property string $urlgambar
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $user_id
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Carousel extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'carousel';

	protected $casts = [
		'user_id' => 'int',
		'utama' => 'int',
	];

	protected $fillable = [
		'namacarousel',
		'urlgambar',
		'user_id',
		'utama',
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
