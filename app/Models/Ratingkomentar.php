<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 13 Nov 2019 10:14:54 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Ratingkomentar
 * 
 * @property int $id
 * @property string $review
 * @property int $jumlah
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $user_customer_id
 * @property int $booking_header_id
 * @property int $user_merchant_id
 * 
 * @property \App\Models\BookingHeader $booking_header
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Ratingkomentar extends Eloquent
{
	protected $table = 'ratingkomentar';

	protected $casts = [
		'jumlah' => 'int',
		'user_customer_id' => 'int',
		'booking_header_id' => 'int',
		'user_merchant_id' => 'int'
	];

	protected $fillable = [
		'review',
		'jumlah',
		'user_customer_id',
		'booking_header_id',
		'user_merchant_id'
	];

	public function booking_header()
	{
		return $this->belongsTo(\App\Models\BookingHeader::class);
	}

	public function user_customer()
	{
		return $this->belongsTo(\App\Models\User::class, 'user_customer_id');
	}

	public function user_merchant()
	{
		return $this->belongsTo(\App\Models\User::class, 'user_merchant_id');
	}
}
