<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 26 May 2019 14:03:09 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Stylistservice
 * 
 * @property int $id
 * @property int $stylist_id
 * @property int $service_id
 * 
 * @property \App\Models\Stylist $stylist
 * @property \App\Models\Service $service
 *
 * @package App\Models
 */
class Stylistservice extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'stylist_id' => 'int',
		'service_id' => 'int'
	];

	protected $fillable = [
		'stylist_id',
		'service_id'
	];

	public function stylist()
	{
		return $this->belongsTo(\App\Models\Stylist::class);
	}

	public function service()
	{
		return $this->belongsTo(\App\Models\Service::class);
	}
}
