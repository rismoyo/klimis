<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 19 Nov 2019 18:01:47 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Report
 * 
 * @property int $id
 * @property string $judul
 * @property string $isi
 * @property string $urlgambar
 * @property int $statusreport
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $user_customer_id
 * @property int $user_merchant_id
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Report extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'report';

	protected $casts = [
		'statusreport' => 'int',
		'user_customer_id' => 'int',
		'user_merchant_id' => 'int'
	];

	protected $fillable = [
		'judul',
		'isi',
		'urlgambar',
		'statusreport',
		'user_customer_id',
		'user_merchant_id'
	];

	public function user_customer()
	{
		return $this->belongsTo(\App\Models\User::class, 'user_customer_id');
	}

	public function user_merchant()
	{
		return $this->belongsTo(\App\Models\User::class, 'user_merchant_id');
	}
}
