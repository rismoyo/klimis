<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 26 May 2019 13:21:10 +0700.
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;
use DB;

class User extends Authenticatable
{
	use Notifiable;
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'user';

	protected $casts = [
		'saldo' => 'float',
		'statusjalan' => 'int',
		'statusverified' => 'int',
		'jenisuser' => 'int',
		'pinlatitude' => 'float',
		'pinlongitude' => 'float',
		'statusbuka' => 'int'
	];

	protected $hidden = [
		'password',
		'remember_token',
		'api_token',
		'firebase_auth_token',
		'firebase_auth_provider',
		'firebase_auth_uid',
		'firebase_instance_id',
		'firebase_fcm_token',
		'firebase_auth_password_hash',
		'firebase_auth_token_valid_at',
		'firebase_auth_last_login_at'
	];

	protected $fillable = [
		'nama',
		'email',
		'password',
		'telepon',
		'alamat',
		'saldo',
		'statusjalan',
		'statusverified',
		'jenisuser',
		'remember_token',
		'api_token',
		'pinalamat',
		'pinlatitude',
		'pinlongitude',
		'statusbuka',
		'firebase_auth_token',
		'firebase_auth_provider',
		'firebase_auth_uid',
		'firebase_instance_id',
		'firebase_fcm_token',
		'firebase_auth_password_hash',
		'firebase_auth_token_valid_at',
		'firebase_auth_last_login_at',
		'no_rekening',
		'bank',
		'atas_nama_bank'
	];

	protected $appends = ['total_rating'];

	public function bookingheaders()
	{
		return $this->hasMany(\App\Models\BookingHeader::class);
	}

	// public function bookingheader_customer()
	// {
	// 	return $this->hasMany(\App\Models\BookingHeader::class, 'user_customer_id');
	// }

	// public function bookingheader_merchant()
	// {
	// 	return $this->hasMany(\App\Models\BookingHeader::class, 'user_merchant_id');
	// }

	public function informasis()
	{
		return $this->hasMany(\App\Models\Informasi::class);
	}

	public function carousels()
	{
		return $this->hasMany(\App\Models\Carousel::class)->orderBy('utama', 'desc');
	}

	public function jadwalbarbers()
	{
		return $this->hasMany(\App\Models\Jadwalbarber::class);
	}

	public function jadwalbarber_today()
	{
		return $this->hasOne(\App\Models\Jadwalbarber::class)->where('hari', Carbon::now()->dayOfWeek);
	}

	public function promos()
	{
		return $this->hasMany(\App\Models\Promo::class);
	}

	public function ratings()
	{
		return $this->hasMany(\App\Models\Rating::class);
	}

	public function services()
	{
		return $this->hasMany(\App\Models\Service::class);
	}

	public function stylists()
	{
		return $this->hasMany(\App\Models\Stylist::class);
	}

	public function favorite()
	{
		return $this->hasMany(\App\Models\Favorite::class);
	}

	public function transaksisaldos()
	{
		return $this->hasMany(\App\Models\Transaksisaldo::class);
	}

	public function ratingkomentars_to()
	{
		return $this->hasMany(\App\Models\Ratingkomentar::class, 'user_customer_id');
	}

	public function ratingkomentars_from()
	{
		return $this->hasMany(\App\Models\Ratingkomentar::class, 'user_merchant_id');
	}

	public function carousel_utama()
	{
		return $this->hasOne(\App\Models\Carousel::class)->where('utama', 1);
	}

	public function promo_aktif()
	{
		return $this->hasOne(\App\Models\Promo::class)
			// ->where('statusjalan', '=', 1)
			// ->where('statusbuka', '=', 1)
			->whereDate('waktuawal', '<=', Carbon::today())
			->whereDate('waktuakhir', '>=', Carbon::today());
	}

	public function getTotalRatingAttribute()
	{
		if ($this->ratingkomentars_from()->exists()) {
			return $this->ratingkomentars_from()->avg('jumlah');
		}
		return 0;
	}


	public static function getNearByLocation($center_lat, $center_lng, $distance)
	{
		// mencari radius
		return User::selectRaw('*,( 6371 * acos( cos( radians(?) ) * cos( radians( pinlatitude ) ) * cos( radians( pinlongitude ) - radians(?) ) + sin( radians(?) ) * sin( radians( pinlatitude ) ) ) ) AS jarak', [$center_lat, $center_lng, $center_lat])
			->where('jenisuser', '3')
			->having('jarak', '<', $distance);
	}
	// public static function getNearBy($center_lat, $center_lng, $distance)
	// {
	// 	$near_by = DB::select('SELECT x.*, concat(round(jarak,2)," km") as jarak_string FROM (
	// 		SELECT id, nama, email, telepon, alamat, ( 6371 * acos( cos( radians(?) ) * cos( radians( pinlatitude ) ) * cos( radians( pinlongitude ) - radians(?) ) + sin( radians(?) ) * sin( radians( pinlatitude ) ) ) ) AS jarak
	// 		FROM user 
	// 		WHERE jenisuser = 3
	// 		HAVING jarak < ?
	// 	  ) x ORDER BY jarak;', [$center_lat, $center_lng, $center_lat,$distance]);
	// 	return $near_by;
	// }
}
