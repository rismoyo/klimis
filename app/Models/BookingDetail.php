<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 23 Jun 2019 21:23:43 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BookingDetail
 * 
 * @property int $id
 * @property float $harga
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $service_id
 * @property int $booking_header_id
 * 
 * @property \App\Models\Service $service
 * @property \App\Models\BookingHeader $booking_header
 *
 * @package App\Models
 */
class BookingDetail extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'booking_detail';
	// public $incrementing = false;
	// public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'harga' => 'float',
		'service_id' => 'int',
		'booking_header_id' => 'int'
	];

	protected $dates = [];

	protected $fillable = [
		'harga',
		'service_id',
		'booking_header_id'
	];

	public function service()
	{
		return $this->belongsTo(\App\Models\Service::class);
	}

	public function booking_header()
	{
		return $this->belongsTo(\App\Models\BookingHeader::class);
	}
}
