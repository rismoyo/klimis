<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 23 Feb 2019 21:05:07 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Rating
 * 
 * @property int $id
 * @property string $review
 * @property int $jumlah
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $user_id
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $bookings
 *
 * @package App\Models
 */
class Rating extends Eloquent
{
	protected $table = 'rating';

	protected $casts = [
		'jumlah' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'review',
		'jumlah',
		'user_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function bookingheaders()
	{
		return $this->hasMany(\App\Models\BookingHeader::class);
	}
}
