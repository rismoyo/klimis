<?php

use Illuminate\Http\Request;
use App\Models\Booking;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('send-notif', 'Api\ApiAuth@postNotification');

Route::get('notif', 'Api\ApiCustomer@sendNotifBooking');

// Route::get('datafavorite','Api\ApiCustomer@getFavoriteData');


Route::get('/', function () {
  return 'api';
});

Route::get('home/data', 'Api\ApiHome@getData');
Route::get('search/stylist', 'Api\ApiHome@getSearchStylistList');
Route::get('getnearby', 'Api\ApiHome@getNearbyMerchant');
Route::get('home/datauser', 'Api\ApiHome@getDataUser');
Route::get('view/merchant/fromid', 'Api\ApiHome@getViewMerchantDetail');
Route::get('view/stylist/merchant/fromid', 'Api\ApiHome@getViewStylistMerchantDetail'); // memilih stylist, menampilkan service dengan tipe home& barbershop

Route::get('view/stylist/nama', 'Api\ApiCustomer@getStylistFromNama');



// Route::get('check/stylist', 'Api\ApiTransaksiBooking@getCheckingWorkingHour');//gak dipake


// untuk menampilkan list rating komen yng dimiliki merchant berdasarkna id merchant yang dpilih
Route::get('getrating/list', 'Api\ApiMerchant@getRatingKomentarList');

// Route::get('getrating/authlist', 'Api\ApiMerchant@getRatingKomentarListFromAuth');
// Route::get('getrating/count', 'Api\ApiMerchant@getRatingKomentarCount');
// Route::get('getrating/fromid', 'Api\ApiMerchant@getRatingKomentarFromId');

// Route::get('promonow/list', 'Api\ApiCustomer@getPromoForTodayList');
// Route::get('promonow/fromid', 'Api\ApiCustomer@getPromoForTodayFromId');

// Route::get('bookingheader/databarber', 'Api\ApiTransaksiBooking@getBarbershopTransaksiBookingData');
// Route::get('bookingheader/datacust', 'Api\ApiTransaksiBooking@getCustomerTransaksiBookingData');

Route::prefix('auth')->group(function () {
  Route::post('login', 'Api\ApiAuth@authFirebase');
  Route::post('fcm-token', 'Api\ApiAuth@postFcmToken')->middleware(['auth:api']);
});

Route::middleware(['auth:api'])->group(function () {
  Route::prefix('jadwalharian')->group(function () {
    Route::get('list', 'Api\ApiMerchant@getJadwalHarianList');
  });

  Route::prefix('transaksisaldo')->group(function () {
    Route::post('entrytopup', 'Api\ApiTransaksiSaldo@addtopupsaldo');
    Route::get('list', 'Api\ApiTransaksiSaldo@getTopupSaldoList');
    Route::get('fromid', 'Api\ApiTransaksiSaldo@getTopupSaldoFromId');

    Route::post('entrywithdraw', 'Api\ApiTransaksiSaldo@addWithdrawSaldo');
  });

  // Route::prefix('promonow')->group(function () {
  //   Route::get('list', 'Api\ApiCustomer@getPromoForTodayList');
  // });

  Route::prefix('komen')->group(function () { });

  Route::prefix('report')->group(function () {
    Route::post('entrycust', 'Api\ApiCustomer@postCustomerReportBarbershopEntry');
    Route::post('entrybarber', 'Api\ApiMerchant@postBarbershopReportCustomerEntry');
  });

  Route::prefix('customer')->group(function () {
    Route::post('entrycust', 'Api\ApiCustomer@postCustomerReportBarbershopEntry');
  });

  Route::prefix('rating')->group(function () {
    Route::post('entrycust', 'Api\ApiCustomer@postCustomerRatingKomentar');
    Route::get('fromid', 'Api\ApiMerchant@getMerchantAvarageRatingKomentarFromId');

    //// ditampilkan di user merchant berdasrkan auth
    Route::get('list', 'Api\ApiMerchant@getRatingKomentarListFromAuth');
    Route::get('check', 'Api\ApiCustomer@getCheckRatingKomentarFromId');
  });


  Route::prefix('booking')->group(function () {
    Route::post('entry', 'Api\ApiTransaksiBooking@postBookingHeaderEntry');

    Route::get('listcust', 'Api\ApiTransaksiBooking@getCustomerTransaksiBookingList');
    Route::get('listbarber', 'Api\ApiTransaksiBooking@getBarbershopTransaksiBookingList');

    Route::get('idbarber', 'Api\ApiTransaksiBooking@getBarbershopTransaksiBookingFromId');
    Route::get('idcust', 'Api\ApiTransaksiBooking@getCustomerTransaksiBookingFromId');

    // Route::post('postbarber', 'Api\ApiTransaksiBooking@postBarbershopTransaksiBookingData');
    Route::post('terima', 'Api\ApiTransaksiBooking@postBarbershopTerimaBookingData');

    Route::post('mulai', 'Api\ApiTransaksiBooking@postMulaiPengerjaanBookingData');

    Route::post('selesai', 'Api\ApiTransaksiBooking@postSelesaiPengerjaanBookingData');


    Route::prefix('customer')->group(function () {
      Route::post('mulai', 'Api\ApiTransaksiBooking@postKonfirmasiCustomerMulaiPengerjaanBookingData');
      Route::post('selesai', 'Api\ApiTransaksiBooking@postKonfirmasiCustomerSelesaiPengerjaanBookingData');
    });
    // postSuksesBookingData
  });

  Route::prefix('updatesaldo')->group(function () {
    //  ****  booking (proses pengurngan saldo -> saldo - dp "penghitungan dilakukan di android") *****
    // Route::post('entrykurang', 'Api\ApiHome@postUpdateSaldoUser');

    //  **** cancel booking & tolak booking (proses pengembalian saldo -> saldo + dp) *****
    Route::post('entrytambah', 'Api\ApiTransaksiBooking@postCancelBookingData');
  });


  Route::prefix('favorite')->group(function () {
    Route::post('entry', 'Api\ApiCustomer@postFavoriteEntry');
    Route::get('data', 'Api\ApiCustomer@getFavoriteData');
    Route::post('delete', 'Api\ApiCustomer@getFavoriteDelete');
    Route::get('list', 'Api\ApiCustomer@getFavoriteList');
  });

  // Route::prefix('bookingdetail')->group(function () {
  //   Route::get('entry', 'Api\ApiBooking@postBookingDetailEntry');
  //   Route::get('list', 'Api\ApiBooking@getBookingDetailList');
  //   Route::get('fromid', 'Api\ApiBooking@getBookingDetailFromId');
  // });


  Route::prefix('informasi')->group(function () {
    // Route::post('entry', 'Api\ApiCustomer@psotInformasi');

    Route::prefix('customer')->group(function () {
      Route::get('list', 'Api\ApiCustomer@getInformasiCustomerList');
      Route::get('fromid', 'Api\ApiCustomer@getInformasiCustomerFromId');
    });

    // Route::prefix('barbershop')->group(function () {
    //   Route::get('list', 'Api\ApiMerchant@getInformasiBarbershopList');
    //   Route::get('fromid', 'Api\ApiMerchant@getInformasiBarbershopFromId');
    // });
  });


  Route::prefix('merchant')->group(function () {

    Route::prefix('profile')->group(function () {
      Route::post('entry', 'Api\ApiMerchant@postMerchantProfileEntry');
      Route::get('info', 'Api\ApiMerchant@getMerchantProfileData');
    });

    Route::prefix('master')->group(function () {

      Route::prefix('carousel')->group(function () {
        Route::get('list', 'Api\ApiMerchant@getCarouselList');
      });

      Route::prefix('stylist')->group(function () {
        Route::post('entry', 'Api\ApiMerchant@postStylistEntry');
        Route::get('list', 'Api\ApiMerchant@getStylistList');
        Route::get('fromid', 'Api\ApiMerchant@getStylistFromId');
        Route::post('delete', 'Api\ApiMerchant@getStylistDelete');
      });
      Route::prefix('service')->group(function () {
        Route::post('entry', 'Api\ApiMerchant@postServiceEntry');
        Route::get('list', 'Api\ApiMerchant@getServiceList');
        Route::get('fromid', 'Api\ApiMerchant@getServiceFromId');
        Route::post('delete', 'Api\ApiMerchant@getServiceDelete');
      });
      Route::prefix('promo')->group(function () {
        Route::post('entry', 'Api\ApiMerchant@postPromoEntry');
        Route::get('list', 'Api\ApiMerchant@getPromoList');
        Route::get('fromid', 'Api\ApiMerchant@getPromoFromId');
        Route::post('delete', 'Api\ApiMerchant@getPromoDelete');
      });
    });
  });
});

///////////////////////////////////////// LOGIN CUSTOMER //////////////////////////////////////////////////////////////////
// Route::post('login', 'Android\ControllerLoginA@loginuser');
// Route::post('logingoogle', 'Android\ControllerLoginA@logingoogle');

// /////////////////////////////////////// LOGIN MERCHANT BARBERSHOP ////////////////////////////////////////////////////////
// Route::post('loginmerchant', 'Android\ControllerLoginA@loginmerchant');
// Route::post('logincustomer', 'Android\ControllerLoginA@logincustomer');

// // Route::post('loginbarbershop', 'Android\ControllerLoginA@loginbarbershop');



// Route::get('book', function () {
//   return Booking::with(['promo', 'rating', 'service', 'stylist', 'user', 'historypembayarans'])->get();
// });



// Route::post('daftaruser', 'Android\ControllerRegisterA@daftaruser');
// //(data dikirim ke android ke class NetworkService, letak class function data user yang mau diambildari database);
// Route::get('getlistusermerchant', 'Android\ControllerUserA@getlistusermerchant');
// // Route::get('getdatauser', 'Android\ControllerUserA@getdatauser');




// Route::middleware(['auth:api'])->group(function () {

//   /////////////////////////////////////////////////// USER //////////////////////////////////////////////////////////////////
//   Route::get('getdatauser', 'Android\ControllerUserA@getdatauser');

//   Route::post('verifikasiuser', 'Android\ControllerUserA@verifikasiuser');

//   Route::put('edituser', 'Android\ControllerUserA@edituser');

//   /////////////////////////////////////// TRANSAKSI SALDO //////////////////////////////////////////////////////////////////
//   Route::post('addtopupsaldo', 'Android\ControllerTransaksiSaldoA@addtopupsaldo');
//   Route::post('addwithdrawsaldo', 'Android\ControllerTransaksiSaldoA@addwithdrawsaldo');


//   ///////////////////////////////////////// PROMO /////////////////////////////////////////////////////////////////////////////
//   Route::get('getlistpromo', 'Android\ControllerPromoA@getlistpromo');
//   Route::get('getdatapromo', 'Android\ControllerPromoA@getdatapromo');
//   Route::post('addpromo', 'Android\ControllerPromoA@addpromo');
//   Route::delete('deletepromo', 'Android\ControllerPromoA@deletepromo');
//   Route::put('editpromo', 'Android\ControllerPromoA@editpromo');


//   //////////////////////////////////////// SERVICE //////////////////////////////////////////////////////////////////////////////
//   Route::get('getlistservicemerchant', 'Android\ControllerServiceA@getlistservicemerchant');
//   Route::get('getlistservicecustomer', 'Android\ControllerServiceA@getlistservicecustomer');
//   Route::post('addservice', 'Android\ControllerServiceA@addservice');
//   Route::get('getdataservice', 'Android\ControllerServiceA@getdataservice');
//   Route::put('editservice', 'Android\ControllerServiceA@editservice');
//   Route::delete('deleteservice', 'Android\ControllerServiceA@deleteservice');

//   ////////////////////////////////////////JADWAL BARBER //////////////////////////////////////////////////////////////////////////////
//   Route::get('getlistjadwalbarber', 'Android\ControllerJadwalBarberA@getlistjadwalbarber');
//   Route::get('getdatajadwalbarber ', 'Android\ControllerJadwalBarberA@getdatajadwalbarber');
//   Route::post('addjadwalbarber', 'Android\ControllerJadwalBarberA@addjadwalbarber');
//   Route::put('editjadwalbarber', 'Android\ControllerJadwalBarberA@editjadwalbarber');
//   Route::delete('deletejadwalbarber', 'Android\ControllerJadwalBarberA@deletejadwalbarber');

//   ////////////////////////////////////////// JADWAL STYLIST ////////////////////////////////////////////////////////////////////////////
//   Route::get('getliststylistmerchant', 'Android\ControllerStylistA@getliststylistmerchant');
//   Route::get('getliststylistcustomer', 'Android\ControllerStylistA@getliststylistcustomer');
//   Route::get('getdatastylist', 'Android\ControllerStylistA@getdatastylist');
//   Route::post('addjadwalstylist', 'Android\ControllerJadwalStylistA@addjadwalstylist');
//   Route::put('editstylist', 'Android\ControllerStylistA@editstylist');
//   Route::delete('deletestylist', 'Android\ControllerStylistA@deletestylist');

//   ////////////////////////////////////////// STYLIST ////////////////////////////////////////////////////////////////////////////
//   Route::get('getlistjadwalstylist', 'Android\ControllerJadwalStylistA@getlistjadwalstylist');
//   Route::get('getdatajadwalstylist', 'Android\ControllerJadwalStylistA@getdatajadwalstylist');
//   Route::post('addstylist', 'Android\ControllerStylistA@addstylist');
//   Route::put('editjadwalstylist', 'Android\ControllerJadwalStylistA@editjadwalstylist');
//   Route::delete('deletejadwalstylist', 'Android\ControllerJadwalStylistA@deletejadwalstylist');

//   /////////////////////////////////////// CAROUSEL ///////////////////////////////////////////////////////////////////////////////
//   Route::get('getlistcarousel', 'Android\ControllerCarouselA@getlistcarousel');
//   Route::get('getdatacarousel', 'Android\ControllerCarouselA@getdatacarousel');
//   Route::post('addcarousel', 'Android\ControllerCarouselA@addcarousel');
//   Route::put('editcaraousel', 'Android\ControllerCarouselA@editcaraousel');
//   Route::delete('deletecarousel', 'Android\ControllerCarouselA@deletecarousel');
// });
