<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//Form login
Route::get('/', 'Website\User\ControllerLogin@FormLogin')->name('login');
Route::get('cobalogin', 'Website\User\ControllerLogin@cobalogin');

Route::get('FormLogin', 'Website\User\ControllerLogin@Formlogin');
Route::post('DataLogin', 'Website\User\ControllerLogin@DataLogin');

Route::get('FormRegister', 'Website\User\ControllerRegister@FormRegister');
Route::post('AddDataRegister', 'Website\User\ControllerRegister@AddDataRegister');


Route::middleware(['web', 'auth'])->group(function () { // jika belum login redirect ke halaman login
    Route::get('logout', 'Website\User\ControllerLogin@logout');

    //Forms Home
    Route::get('FormLaporan', 'Website\Laporan\ControllerLaporan@FormLaporan');

    //Forms Home
    Route::get('FormHome', 'Website\Home\ControllerHome@FormHome');

    // Route::get('DataJumlahUser', 'Website\Home\ControllerHome@DataJumlahUser');

    // Route::get('DataBlockUser', 'Website\Home\ControllerHome@DataBlockUser');

    // Route::get('DataTopupUser', 'Website\Home\ControllerHome@DataTopupUser');


    // Form Barbershop
    Route::get('FormUser', 'Website\User\ControllerUser@FormUser');
    Route::get('AddForm', 'Website\User\ControllerUser@AddForm');
    Route::get('EditForm/{id}', 'Website\User\ControllerUser@EditForm');

    // Data barbershop
    Route::post('AddData', 'Website\User\ControllerUser@AddData');
    Route::post('EditData', 'Website\User\ControllerUser@EditData');
    Route::get('DeleteData/{id}', 'Website\User\ControllerUser@DeleteData');

    //Form Block & Unblock
    Route::get('FormBlockUser', 'Website\BlockUnblock\ControllerBlockUser@FormBlockUser');

    //Data BLock & BlockUnblock
    Route::get('BlockUser/{id}', 'Website\BlockUnblock\ControllerBlockUser@BlockUser');
    Route::get('UnblockUser/{id}', 'Website\BlockUnblock\ControllerBlockUser@UnblockUser');

    //Form Verifikasi User
    Route::get('FormVerifikasiUser', 'Website\Verifikasi\ControllerVerifikasiUser@FormVerifikasiUser');

    //Data BLock & BlockUnblock
    Route::get('VerifikasiUser/{id}', 'Website\Verifikasi\ControllerVerifikasiUser@VerifikasiUser');

    // Form Topup
    Route::get('FormTopup', 'Website\TransaksiSaldo\ControllerTransaksiSaldo@FormTopup');
    Route::get('VerifikasiTopupUser/{id}', 'Website\TransaksiSaldo\ControllerTransaksiSaldo@VerifikasiTopupUser');

    // Form Report
    Route::get('FormReport', 'Website\Report\ControllerReport@FormReport');
    Route::get('ActionReportBlockUser/{id}', 'Website\Report\ControllerReport@ActionReportBlockUser');

    // Form Withdraw
    Route::get('FormWithdraw', 'Website\TransaksiSaldo\ControllerTransaksiSaldo@FormWithdraw');
    Route::get('VerifikasiWithdrawUser/{id}', 'Website\TransaksiSaldo\ControllerTransaksiSaldo@VerifikasiWithdrawUser');
    // Route::post('EditData', 'Website\User\ControllerUser@EditData');
    // Route::get('DeleteData/{id}', 'Website\User\ControllerUser@DeleteData');

    //LAPORAN
    Route::get('FormBarberAktif', 'Website\Laporan\ControllerLaporan@FormBarberAktif');
    Route::get('FormVerifikasi', 'Website\Laporan\ControllerLaporan@FormVerifikasi');
    Route::get('FormPembelianSaldo', 'Website\Laporan\ControllerLaporan@FormPembelianSaldo');
    Route::get('FormPenarikanTunai', 'Website\Laporan\ControllerLaporan@FormPenarikanTunai');

    // Route::get('ChartDanaBarber', 'Website\Laporan\ControllerLaporan@ChartDanaBarber');
    Route::get('ChartTopupTerbanyak', 'Website\Laporan\ControllerLaporan@ChartTopupTerbanyak');
    Route::get('ChartWithdrawTerbanyak', 'Website\Laporan\ControllerLaporan@ChartWithdrawTerbanyak');
    Route::get('ChartstylistTerlaris', 'Website\Laporan\ControllerLaporan@ChartstylistTerlaris');
});
