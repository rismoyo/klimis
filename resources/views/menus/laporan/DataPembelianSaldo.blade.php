@extends('layouts.default')
@section('content')
<section class="content-header">
  <h1>
    Data Pembelian Saldo Users
    <small>Preview</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-users"></i> Data Pembelian Saldo</a></li>
    <li><a href="#">Forms</a></li>
    <li class="active">General Elements</li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">

        <div class="box-body">
          <table class="table table-bordered" id="list-table">
          <thead>
            <tr>
              {{-- <th style="width: 10px">#</th> --}}
              <th>Nama</th>
              <th>Nominal</th>
              <th>Bukit Transfer</th>
              <th>Tanggal</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach($transaksisaldo as $value)
              <tr>
                {{-- <td>{{$value->id}}</td> --}}
                <td>{{$value->user->nama}}</td>
                <td>{{$value->nominal}}</td>
                <td>{{$value->created_at}}</td>
                <td>  <a href="{{($value->urlbuktibayar)}}">
                    <img src="{{($value->urlbuktibayar)}}" height="70px" width="50px" /></a></td>
                @if($value->statusverifikasi==0)
                <td><span class="label label-danger" values = "{{$value->statusverified}}" >Belum Dikonfirmasi</span> </td>
                {{-- <td class="center">
                  <!-- <a class="btn btn-info"  data-toggle="modal"   data-target="#edit-modal"> -->
                    <a class="btn btn-info" href="{{url('DetailFormTopup/'.$value->id)}}">
                      <i class="glyphicon glyphicon-edit icon-white"></i>
                      Detail
                  </a>
                  <a class="btn bg-red btn-delete " href="{{url('VerifikasiTopupUser/'.$value->id)}}">
                      <i class="glyphicon glyphicon-check icon-white"></i>
                      Verifikasi
                  </a>
              </td> --}}
                @else
               <td><span class="label label-success" values = "{{$value->statusverifikasi}}" >Sudah Dikonfirmasi</span> </td>
                @endif

               
              </tr>
            @endforeach
            <tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <!-- <div class="modal fade" id="edit-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" align="center"><b>Edit User</b></h4>
        </div>
        <div class="modal-body">
          <form role="form" action="/edit_user">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="box-body">
              <div class="form-group">
                <label for="exampleInputEmail1">User ID</label>
                <input type="text" class="form-control" name="user_id" placeholder="User ID" >
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Username</label>
                <input type="text" class="form-control" name="username" placeholder="Enter username">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="text" class="form-control" name="email" placeholder="Enter email">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Contact</label>
                <input type="text" class="form-control" name="contact" placeholder="Enter contact">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Change Password</label>
                <input type="password" class="form-control" name="change_password" placeholder="Enter password">
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div> -->

</section>

@endsection

@push('below_css')
<link rel="stylesheet" href="{{asset('assets/bower_components/datatablesnet-bs/css/dataTables.bootstrap.min.css')}}">
@endpush

@push('below_script')
<script src="{{asset('assets/bower_components/datatablesnet/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/bower_components/datatablesnet-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
$(function(){
  thismenu.init();
}), thismenu = {
  init : function (){
    thismenu.init_confirm();
    thismenu.table_init();
  },
  init_confirm : function (){
      $('.btn-delete').on('click',function(e){
        e.preventDefault();
        swal({
          title: "Apa kamu yakin?",
          text: "Melakukan verifikasi topup user!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            window.location = $(this).attr('href');
          }
        });

      });
    },


  table_init : function() {
    $('#list-table').DataTable({
      "pageLength": 10,
    });
  },
};
</script>
@endpush
