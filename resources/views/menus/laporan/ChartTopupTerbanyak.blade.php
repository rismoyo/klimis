@extends('layouts.default')
@section('content')
    <section class="content-header">
        <h1>
            Topup Terbanyak
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-users"></i> Data Topup Terbanyak</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        {!! $chart->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('below_css')

@endpush

@push('below_script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    {!! $chart->script() !!}
@endpush
