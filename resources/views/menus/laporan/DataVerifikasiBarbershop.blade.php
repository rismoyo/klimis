@extends('layouts.default')
@section('content')
<section class="content-header">
  <h1>
    Data Verifikasi Barbershop
    <small>Preview</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i  class="fa fa-user-times"></i> Data Verifikasi User</a></li>
    <li><a href="#">Forms</a></li>
    <li class="active">General Elements</li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">

        <div class="box-body">
          <table class="table table-bordered" id="list-table">
          <thead>
            <tr>
              {{-- <th style="width: 10px">#</th> --}}
              <th>Nama</th>
              <th>Alamat</th>
              <th>No HP</th>
              <th>Status</th>
              {{-- <th>Actions</th> --}}
            </tr>
          </thead>
          <tbody>
            @foreach($user as $value)
              <tr>
                {{-- <td>{{$value->id}}</td> --}}
                <td>{{$value->nama}}</td>
                <td>{{$value->pinalamat}}</td>
                <td>{{$value->telepon}}</td>
                @if($value->statusverified==1)
                <td><span class="label label-success" values = "{{$value->statusverified}}" >Sudah Diverifikasi</span> </td>
                @else
               <td><span class="label label-danger" values = "{{$value->statusverified}}" >Belum Diverifikasi</span> </td>
                    {{-- <td class="center">
                      <a class="btn bg-green btn-verifikasi" href="{{url('VerifikasiUser/'.$value->id)}}">
                          <i class="fa fa-fw fa-check icon-white"></i>
                          Verifikasi
                      </a>
                  </td> --}}
                @endif
               
              </tr>

            @endforeach
            <tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@push('below_css')
<link rel="stylesheet" href="{{asset('assets/bower_components/datatablesnet-bs/css/dataTables.bootstrap.min.css')}}">
@endpush

@push('below_script')
<script src="{{asset('assets/bower_components/datatablesnet/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/bower_components/datatablesnet-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>


$(function(){
  thismenu.init();
}), thismenu = {
  init : function (){
    thismenu.init_confirm();
    thismenu.table_init();
  },
  init_confirm : function (){
      $('.btn-verifikasi').on('click',function(e){
        e.preventDefault();
        swal({
          title: "Are you sure?",
          text: " This user will be verified!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
              window.location = $(this).attr('href');
            }
          });
      });

    },

  table_init : function() {
    $('#list-table').DataTable({
      "pageLength": 10,
    });
  },
};
</script>
@endpush
