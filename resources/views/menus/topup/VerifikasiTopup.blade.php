@extends('layouts.default')
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->

          <div class="col-md-3">
          <!-- general form elements -->
          <div class="box box-primary">
            @php
            // @if ($errors->any())
            //      <div class="alert alert-danger">
            //          <ul>
            //              @foreach ($errors->all() as $error)
            //                  <li>{{ $error }}</li>
            //              @endforeache
            //          </ul>
            //      </div>
            //  @endif
            @endphp

            <div class="box-header with-border">
              <h3 class="box-title">Input User</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <form role="form" method="POST" action="{{url('AddData')}}">
              {{ csrf_field() }}
              @isset($user)
              <input type="hidden" name="id" class="form-control" value="{{old('id', (isset($user) ? $user->id : ''))}}">
              @endisset

              <div class="box-body">
                <div class="form-group {{($errors->has('nama')) ? 'has-error' : ''}}">
                  <label for="InputNama">Nama</label>
                  <input type="name" class="form-control" name= "nama" id="InputNama" placeholder="Enter nama" value="{{old('nama', (isset($user) ? $user->nama : ''))}}">
                  @foreach($errors->get('nama') as $messages)
                  <span class="help-block glyphicon glyphicon-remove-sign">{{$messages}}</span>
                  @endforeach
                </div>

                <div class="form-group {{($errors->has('email')) ? 'has-error' : ''}}">
                  <label for="InputEmail">Email address</label>
                  <input type="email" class="form-control" name= "email" id="InputEmail" placeholder="Enter email" value="{{old('email', (isset($user) ? $user->email : ''))}}">
                  @foreach($errors->get('email') as $messages)
                  <span class="help-block glyphicon glyphicon-remove-sign">{{$messages}}</span>
                  @endforeach
                </div>

                <!-- <div class="form-group">
                  <label for="InputPassword">Password</label>
                  <input type="name" class="form-control" name= email id="InputPassword" placeholder="Enter password" value="{{old('password', (isset($user) ? $user->password : ''))}}">
                </div> -->

                <div class="form-group {{($errors->has('alamat')) ? 'has-error' : ''}}">
                  <label for="InputAlamat">Alamat</label>
                  <input type="name" class="form-control" name= "alamat" id="InputAlamat" placeholder="Enter alamat" value="{{old('alamat', (isset($user) ? $user->alamat : ''))}}">
                  @foreach($errors->get('alamat') as $messages)
                  <span class="help-block glyphicon glyphicon-remove-sign">{{$messages}}</span>
                  @endforeach
                </div>

                <!-- <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" id="exampleInputFile">

                  <p class="help-block">Example block-level help text here.</p>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Check me out
                  </label>
                </div> -->
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
              </div>
            </form>
          </div>

          <!-- /.box -->

        </section>
@endsection
