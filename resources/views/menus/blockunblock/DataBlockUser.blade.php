@extends('layouts.default')
@section('content')
<section class="content-header">
  <h1>
    Block & Unblock User 
    <small>Preview</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i  class="fa fa-user-times"></i> Block & Unblock User</a></li>
    <li><a href="#">Forms</a></li>
    <li class="active">General Elements</li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">

        <div class="box-body">
          <table class="table table-bordered" id="list-table">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Nama</th>
              {{-- <th>Email</th> --}}
              <th>Status</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach($user as $value)
              <tr>
                <td>{{$value->id}}</td>
                <td>{{$value->nama}}</td>
                {{-- <td>{{$value->email}}</td> --}}
                @if($value->statusjalan==1)
                <td><span class="label label-success" values = "{{$value->statusjalan}}" >Aktif</span> </td>
                <td class="center">
                 
                  <a class="btn bg-red btn-blok " href="{{url('BlockUser/'.$value->id)}}">
                      <i class="fa fa-fw fa-close icon-white"></i>
                      Block
                  </a>
              </td>
                @else
               <td><span class="label label-danger" values = "{{$value->statusjalan}}" >Non-Aktif</span> </td>
               <td class="center">
                  <a class="btn bg-green btn-unblok" href="{{url('UnblockUser/'.$value->id)}}">
                   <i class="fa fa-fw fa-check icon-white"></i>
                    Unblock
                  </a>
                </td>
                @endif
                {{-- <td class="center">
                    <a class="btn bg-green btn-unblok" href="{{url('UnblockUser/'.$value->id)}}">
                        <i class="fa fa-fw fa-check icon-white"></i>
                        Unblock
                    </a>
                    <a class="btn bg-red btn-blok " href="{{url('BlockUser/'.$value->id)}}">
                        <i class="fa fa-fw fa-close icon-white"></i>
                        Block
                    </a>
                </td> --}}
              </tr>

            @endforeach
            <tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@push('below_css')
<link rel="stylesheet" href="{{asset('assets/bower_components/datatablesnet-bs/css/dataTables.bootstrap.min.css')}}">
@endpush

@push('below_script')
<script src="{{asset('assets/bower_components/datatablesnet/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/bower_components/datatablesnet-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>

$(function(){
  thismenu.init();
}), thismenu = {
  init : function (){
    thismenu.init_confirmblock();
    thismenu.init_confirmunblock();
    thismenu.table_init();
  },
  init_confirmblock : function (){
      $('.btn-blok').on('click',function(e){
        e.preventDefault();
        swal({
          title: "Are you sure?",
          text: " This user will be blocked!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
              window.location = $(this).attr('href');
            }
          });
      });

    },
    init_confirmunblock : function (){
        $('.btn-unblok').on('click',function(e){
          e.preventDefault();
          swal({
            title: "Are you sure?",
            text: " This user will be unblocked!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                window.location = $(this).attr('href');
              }
            });
        });

      },


  table_init : function() {
    $('#list-table').DataTable({
      "pageLength": 10,
    });
  },
};
</script>
@endpush
