@extends('layouts.default')
@section('content')
<section class="content-header">
  <h1>
    Data Report User 
    <small>Preview</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i  class="	fa fa-warning"></i> Data Report User</a></li>
    <li><a href="#">Forms</a></li>
    <li class="active">General Elements</li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">

        <div class="box-body">
          <table class="table table-bordered" id="list-table">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Judul</th>
              <th>Isi Report</th>
              <th>Tanggal</th>
              <th>Customer</th>
              <th>Barbershop</th>
              <th>Bukti</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach($report as $value)
              <tr>
                <td>{{$value->id}}</td>
                <td>{{$value->judul}}</td>
                <td>{{$value->isi}}</td>
                <td>{{$value->created_at}}</td>
                <td>{{$value->user_customer_id}}</td>
                <td>{{$value->user_merchant_id}}</td>
                <td>  <a href="{{($value->urlgambar)}}">
                    <img src="{{($value->urlgambar)}}" height="70px" width="50px" /></a></td>
                @if($value->statusreport==0)
                {{-- <td><span class="label label-success" values = "{{$value->statusjalan}}" >Aktif</span> </td> --}}
                <td class="center">
                  {{-- <a class="btn bg-green btn-unblok" href="{{url('UnblockUser/'.$value->id)}}">
                      <i class="fa fa-fw fa-check icon-white"></i>
                      Unblock
                  </a> --}}
                  <a class="btn bg-red btn-blok " href="{{url('ActionReportBlockUser/'.$value->id)}}">
                      <i class="fa fa-fw fa-close icon-white"></i>
                      Block
                  </a>
              </td>
                @else
               <td><span class="label label-success" values = "{{$value->statusreport}}" >Selesai</span> </td>
                @endif
             
              </tr>

            @endforeach
            <tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@push('below_css')
<link rel="stylesheet" href="{{asset('assets/bower_components/datatablesnet-bs/css/dataTables.bootstrap.min.css')}}">
@endpush

@push('below_script')
<script src="{{asset('assets/bower_components/datatablesnet/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/bower_components/datatablesnet-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>

$(function(){
  thismenu.init();
}), thismenu = {
  init : function (){
    thismenu.init_confirmblock();
    thismenu.init_confirmunblock();
    thismenu.table_init();
  },
  init_confirmblock : function (){
      $('.btn-blok').on('click',function(e){
        e.preventDefault();
        swal({
          title: "Are you sure?",
          text: " This user will be blocked!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
              window.location = $(this).attr('href');
            }
          });
      });

    },
    init_confirmunblock : function (){
        $('.btn-unblok').on('click',function(e){
          e.preventDefault();
          swal({
            title: "Are you sure?",
            text: " This user will be unblocked!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                window.location = $(this).attr('href');
              }
            });
        });

      },


  table_init : function() {
    $('#list-table').DataTable({
      "pageLength": 10,
    });
  },
};
</script>
@endpush
