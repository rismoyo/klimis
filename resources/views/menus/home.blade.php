@extends('layouts.default')
@section('content')
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    {{-- <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{$jumlahUser}}</h3>

          <p>Data User</p>
        </div>
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
        
        <a href="{{url('FormUser')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div> --}}
 <!-- ./col -->

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{$belumVerifikasi}}</h3>

          <p>Barbershop belum diverifikasi</p>
        </div>
        <div class="icon">
          <i class="fa fa-user"></i>
        </div>
        
        <a href="{{url('FormVerifikasiUser')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-blue">
        <div class="inner">
          <h3>{{$statusReport}}</h3>

          <p>Report User</p>
        </div>
        <div class="icon">
          <i class="fa fa-comment"></i>
        </div>
        
        <a href="{{url('FormReport')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{$blockUser}}</h3>
          {{-- <sup style="font-size: 20px">%</sup> --}}
          <p>Blocked User</p>
        </div>
        <div class="icon">
          <i class="fa fa-user-times"></i>
        </div>
        <a href="{{url('FormBlockUser')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{$topupUser}}</h3>

          <p>Topup belum dikonfirmasi</p>
        </div>
        <div class="icon">
          <i class="fa fa-dollar"></i>
        </div>
        <a href="{{url('FormTopup')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{$withdrawUser}}</h3>

          <p>Withdraw belum dikonfirmasi</p>
        </div>
        <div class="icon">
          <i class="fa fa-money"></i>
          </div>
        <a href= "{{url('FormWithdraw')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
  </div>
  @endsection
