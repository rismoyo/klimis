@extends('layouts.default')
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->

          <div class="col-md-3">
          <!-- general form elements -->
          <div class="box box-primary">

            <div class="box-header with-border">
              <h3 class="box-title">Edit User</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{url('EditData')}}">
              {{ csrf_field() }}
              <input type="hidden" name="id" class="form-control" value="{{old('id', (isset($user) ? $user->id : ''))}}">
              <div class="box-body">
                <div class="form-group">
                  <label for="InputNama">Nama</label>
                  <input type="name" class="form-control" name= "nama" id="InputNama" placeholder="Enter nama" value="{{old('nama', (isset($user) ? $user->nama : ''))}}">
                </div>
                <div class="form-group">
                  <label for="InputEmail">Email address</label>
                  <input type="email" class="form-control" name= "email" id="InputEmail" placeholder="Enter email" value="{{old('email', (isset($user) ? $user->email : ''))}}">
                </div>

                <!-- <div class="form-group">
                  <label for="InputPassword">Password</label>
                  <input type="name" class="form-control" name= email id="InputPassword" placeholder="Enter password" value="{{old('password', (isset($user) ? $user->password : ''))}}">
                </div> -->

                <div class="form-group">
                  <label for="InputAlamat">Alamat</label>
                  <input type="name" class="form-control" name= "alamat" id="InputAlamat" placeholder="Enter alamat" value="{{old('alamat', (isset($user) ? $user->alamat : ''))}}">
                </div>

                <!-- <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" id="exampleInputFile">

                  <p class="help-block">Example block-level help text here.</p>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Check me out
                  </label>
                </div> -->
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
              </div>
            </form>
          </div>

          <!-- /.box -->

        </section>
@endsection
