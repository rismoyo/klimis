<!DOCTYPE html>
<html>
<head>
  @include('includes.meta')
  @stack('below_css')
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
  @include('includes.header')
  @include('includes.sidebar')

  <div class="content-wrapper">
    @yield('content','')
  </div>
  @include('includes.footer')
</div>
@include('includes.script')
@stack('below_script')
</body>
</html>
