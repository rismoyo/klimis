
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div  class="user-panel">
      <div class="pull-left image">
        <img  src="resources/img/faces_2.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>ADMIN</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>

        <!-- <div class="dropdown">
              <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                <span class="caret"></span></button>
           <ul class="dropdown-menu">
             <li><a href="#">Profile</a></li>
             <li><a href="#">logout</a></li>
           </ul>
      </div> -->
      </div>

    </div>

    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>

    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li>
        <a href="{{url('FormHome')}}">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>
      {{-- <li>
        <a href="{{url('FormUser')}}">
          <i class="fa fa-users"></i> <span>Data User</span>
          <span class="pull-right-container">
            <!-- <small class="label pull-right bg-green">new</small> -->
          </span>
        </a>
      </li> --}}

      <li>
        <a href="{{url('FormVerifikasiUser')}}">
          <i class="fa fa-user"></i> <span>Verifikasi Barbershop</span>
          <span class="pull-right-container">
            <!-- <small class="label pull-right bg-green">new</small> -->
          </span>
        </a>
      </li>


      <li>
        <a href="{{url('FormReport')}}">
          <i class="fa fa-warning"></i> <span>Report User</span>
          <span class="pull-right-container">
            <!-- <small class="label pull-right bg-green">new</small> -->
          </span>
        </a>
      </li>
      {{-- <li>
        <a href="{{url('FormUser')}}">
          <i class="fa fa-envelope-o"></i> <span>Inbox</span>
          <span class="pull-right-container">
            <!-- <small class="label pull-right bg-green">new</small> -->
          </span>
        </a>
      </li> --}}
      <li>
        <a href="{{url('FormTopup')}}">
          <i class="fa fa-dollar "></i> <span>Top Up</span>
          <span class="pull-right-container">
            <!-- <small class="label pull-right bg-green">new</small> -->
          </span>
        </a>
      </li>
      <li>
        <a href="{{url('FormWithdraw')}}">
          <i class="fa fa-money"></i> <span>Withdraw</span>
          <span class="pull-right-container">
            <!-- <small class="label pull-right bg-green">new</small> -->
          </span>
        </a>
      </li>
      <li>
          <a href="{{url('FormBlockUser')}}">
          <i class="fa fa-user-times"></i> <span>Block & Unblock User</span>
          <span class="pull-right-container">
            <!-- <small class="label pull-right bg-green">new</small> -->
          </span>
        </a>
      </li>

   

      <li class="treeview">
      {{-- <li> --}}
          {{-- <a href="{{url('FormLaporan')}}"> --}}
        <a class="treeview" >
          <i class="fa fa-pie-chart"></i>
          <span>Laporan</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('FormBarberAktif')}}"><i class="fa fa-circle-o text-red"></i> Barbershop Aktif </a></li>
          <li><a href="{{url('FormVerifikasi')}}"><i class="fa fa-circle-o text-yellow"></i> Verifikasi Barbershop </a></li>
          <li><a href="{{url('FormPembelianSaldo')}}"><i class="fa fa-circle-o text-green"></i> Pembelian Saldo </a></li>
          <li><a href="{{url('FormPenarikanTunai')}}"><i class="fa fa-circle-o text-blue"></i> Penarikan Tunai </a></li>
          
          {{-- <li><a href="{{url('ChartDanaBarber')}}"><i class="fa fa-circle-o text-blue"></i> Data Penarikan Tunai </a></li> --}}
          <li><a href="{{url('ChartTopupTerbanyak')}}"><i class="fa fa-circle-o text-green"></i> Topup Tebanyak </a></li>
          <li><a href="{{url('ChartWithdrawTerbanyak')}}"><i class="fa fa-circle-o text-yellow"></i> Withdraw Tebanyak </a></li>
          <li><a href="{{url('ChartstylistTerlaris')}}"><i class="fa fa-circle-o text-red"></i> Stylist Terlaris </a></li>
        </ul>
      </li>

      <li>
        <!-- <a class="btn-keluar" href="#"> -->
        <a class="btn-keluar" href="{{url('logout')}}">
          <i class="fa fa-power-off"></i> <span>Logout</span>
          <span class="pull-right-container">
            <!-- <small class="label pull-right bg-green">new</small> -->
          </span>
        </a>
      </li>
  </section>
  <!-- /.sidebar -->
</aside>
