-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: sql148.main-hosting.eu    Database: u990489859_kms
-- ------------------------------------------------------
-- Server version	5.5.5-10.2.23-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `booking_detail`
--

DROP TABLE IF EXISTS `booking_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_detail` (
  `id` int(11) NOT NULL,
  `harga` decimal(18,4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `service_id` int(11) NOT NULL,
  `booking_header_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service_id` (`service_id`),
  KEY `booking_header_id` (`booking_header_id`),
  CONSTRAINT `booking_detail_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`),
  CONSTRAINT `booking_detail_ibfk_2` FOREIGN KEY (`booking_header_id`) REFERENCES `booking_header` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_detail`
--

LOCK TABLES `booking_detail` WRITE;
/*!40000 ALTER TABLE `booking_detail` DISABLE KEYS */;
INSERT INTO `booking_detail` VALUES (0,2000.0000,'1970-01-01 03:15:00','1970-01-01 03:15:00','1970-01-01 03:15:00',1,1),(1,2000.0000,'1970-01-01 03:15:00','1970-01-01 03:15:00',NULL,1,1);
/*!40000 ALTER TABLE `booking_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking_header`
--

DROP TABLE IF EXISTS `booking_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_nota` text DEFAULT NULL,
  `tanggal_transaksi` datetime DEFAULT NULL,
  `waktu_booking` datetime DEFAULT NULL,
  `total_booking` int(11) DEFAULT NULL,
  `total_dp` decimal(16,2) DEFAULT NULL,
  `status_booking` int(11) DEFAULT NULL,
  `status_dp` int(11) DEFAULT NULL,
  `status_lunas` int(11) DEFAULT NULL,
  `tanggal_dp` datetime DEFAULT NULL,
  `tanggal_lunas` datetime DEFAULT NULL,
  `sisa_pembayaran` decimal(18,4) DEFAULT NULL,
  `biaya_lain` decimal(18,4) DEFAULT NULL,
  `total_potongan` decimal(18,4) DEFAULT NULL,
  `grand_total` decimal(18,4) DEFAULT NULL,
  `deposit` decimal(18,4) DEFAULT NULL,
  `alamat_home_services` text DEFAULT NULL,
  `waktu_pengerjaan` datetime DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `rating_id` int(11) NOT NULL,
  `stylist_id` int(11) NOT NULL,
  `promo_id` int(11) DEFAULT NULL,
  `user_customer_id` int(11) NOT NULL,
  `user_merchant_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_booking_stylist1_idx` (`stylist_id`),
  KEY `fk_booking_promo1_idx` (`promo_id`),
  KEY `fk_booking_rating1_idx` (`rating_id`),
  KEY `fk_user_customer_id` (`user_customer_id`),
  KEY `fk_user_merchant_id` (`user_merchant_id`),
  CONSTRAINT `fk_booking_promo1` FOREIGN KEY (`promo_id`) REFERENCES `promo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_rating1` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_stylist1` FOREIGN KEY (`stylist_id`) REFERENCES `stylist` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_customer_id` FOREIGN KEY (`user_customer_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_user_merchant_id` FOREIGN KEY (`user_merchant_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_header`
--

LOCK TABLES `booking_header` WRITE;
/*!40000 ALTER TABLE `booking_header` DISABLE KEYS */;
INSERT INTO `booking_header` VALUES (1,'qwert12345','1970-01-01 03:15:00','1970-01-01 03:15:00',50000,250000.00,1,1,1,'1970-01-01 03:15:00','1970-01-01 03:15:00',2000.0000,20.0000,20.0000,2000.0000,1000.0000,'surabaya','1970-01-01 03:15:00','booking pak','1970-01-01 03:15:00','1970-01-01 03:15:00',NULL,1,2,1,2,2);
/*!40000 ALTER TABLE `booking_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carousel`
--

DROP TABLE IF EXISTS `carousel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carousel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namacarousel` varchar(45) DEFAULT NULL,
  `urlgambar` text DEFAULT NULL,
  `utama` int(11) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_carousel_user1_idx` (`user_id`),
  CONSTRAINT `fk_carousel_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carousel`
--

LOCK TABLES `carousel` WRITE;
/*!40000 ALTER TABLE `carousel` DISABLE KEYS */;
INSERT INTO `carousel` VALUES (3,NULL,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/carousel%2Fe4d9ead9-a80e-4f11-8cea-38c58c23149d?alt=media&token=068aaba2-8850-4891-88d6-85de432dd750',1,'2019-05-27 15:03:17','2019-05-27 15:03:17',NULL,2),(10,NULL,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/images%2Fstylist%2F723fb79c-9806-469c-9234-3ffc8fc3c5ff?alt=media&token=b02253df-891a-4968-99f1-a5a88e3cefb2',0,'2019-05-27 15:03:17','2019-05-27 15:03:17',NULL,2),(11,NULL,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/images%2Fstylist%2F6d67d90b-d785-492c-b7ef-65a6cdb20a3d?alt=media&token=1325970f-f3ef-4cb5-9266-cb8da4bd6644',1,'2019-07-03 23:20:09','2019-07-03 23:20:09',NULL,16),(12,NULL,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/images%2Fstylist%2F40e72395-d67d-4013-a6b5-f324463377a8?alt=media&token=bcabd1c6-88b4-4e70-b100-ca757bf67585',0,'2019-05-27 18:13:44','2019-05-27 18:13:44',NULL,17),(13,NULL,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/images%2Fstylist%2Fc4477282-c302-4397-9754-4c78c1077a23?alt=media&token=a74c0d47-37b7-4d48-9e36-0b071a4cf08c',1,'2019-05-27 18:13:44','2019-05-27 18:13:44',NULL,17),(14,NULL,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/images%2Fcarousel%2F7048ab37-06fb-4bec-9cb5-c0266694c492?alt=media&token=29e41a63-1718-493f-b43f-bcdf2c5fcf07',1,'2019-07-15 23:57:35','2019-07-15 23:57:35',NULL,18),(15,NULL,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/images%2Fcarousel%2F14ac95bf-678c-442b-831c-85fa4e9fdeba?alt=media&token=dc29a1fc-b9ee-44e0-aeeb-59c794423eee',0,'2019-07-15 23:57:35','2019-07-15 23:57:35',NULL,18);
/*!40000 ALTER TABLE `carousel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorite`
--

DROP TABLE IF EXISTS `favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorite` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `usermerchant_id` int(11) NOT NULL,
  `usercustomer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usermerchant_id` (`usermerchant_id`),
  KEY `usercustomer_id` (`usercustomer_id`),
  CONSTRAINT `favorite_ibfk_1` FOREIGN KEY (`usermerchant_id`) REFERENCES `user` (`id`),
  CONSTRAINT `favorite_ibfk_2` FOREIGN KEY (`usercustomer_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorite`
--

LOCK TABLES `favorite` WRITE;
/*!40000 ALTER TABLE `favorite` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historypembayaran`
--

DROP TABLE IF EXISTS `historypembayaran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historypembayaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `totalpembayaran` decimal(16,2) DEFAULT NULL,
  `tanggalpembayaran` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `booking_header_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_historypembayaran_booking1_idx` (`booking_header_id`),
  CONSTRAINT `fk_historypembayaran_booking1` FOREIGN KEY (`booking_header_id`) REFERENCES `booking_header` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historypembayaran`
--

LOCK TABLES `historypembayaran` WRITE;
/*!40000 ALTER TABLE `historypembayaran` DISABLE KEYS */;
/*!40000 ALTER TABLE `historypembayaran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informasi`
--

DROP TABLE IF EXISTS `informasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informasi` (
  `id` int(11) NOT NULL,
  `judul` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isi` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `informasi_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informasi`
--

LOCK TABLES `informasi` WRITE;
/*!40000 ALTER TABLE `informasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `informasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jadwalbarber`
--

DROP TABLE IF EXISTS `jadwalbarber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jadwalbarber` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jambuka` datetime DEFAULT NULL,
  `jamtutup` datetime DEFAULT NULL,
  `hari` int(11) DEFAULT NULL,
  `bukatutup` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_jadwal_user1_idx` (`user_id`),
  CONSTRAINT `fk_jadwal_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jadwalbarber`
--

LOCK TABLES `jadwalbarber` WRITE;
/*!40000 ALTER TABLE `jadwalbarber` DISABLE KEYS */;
INSERT INTO `jadwalbarber` VALUES (6,'1970-01-01 08:00:00','1970-01-01 21:00:00',1,'LIBUR','2019-05-27 15:03:17','2019-05-27 15:03:17',NULL,2),(7,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'LIBUR','2019-05-27 15:03:17','2019-05-27 15:03:17',NULL,2),(8,'1970-01-01 08:00:00','1970-01-01 21:00:00',3,'BUKA','2019-05-27 15:03:17','2019-05-27 15:03:17',NULL,2),(9,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'LIBUR','2019-05-27 15:03:17','2019-05-27 15:03:17',NULL,2),(10,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'LIBUR','2019-05-27 15:03:17','2019-05-27 15:03:17',NULL,2),(11,'1970-01-01 08:00:00','1970-01-01 21:00:00',6,'BUKA','2019-05-27 15:03:17','2019-05-27 15:03:17',NULL,2),(12,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'LIBUR','2019-05-27 15:03:17','2019-05-27 15:03:17',NULL,2),(13,'1970-01-01 08:00:00','1970-01-01 21:00:00',1,'LIBUR','2019-07-03 23:20:09','2019-07-03 23:20:09',NULL,16),(14,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'BUKA','2019-07-03 23:20:09','2019-07-03 23:20:09',NULL,16),(15,'1970-01-01 08:00:00','1970-01-01 21:00:00',3,'BUKA','2019-07-03 23:20:09','2019-07-03 23:20:09',NULL,16),(16,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'BUKA','2019-07-03 23:20:09','2019-07-03 23:20:09',NULL,16),(17,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'BUKA','2019-07-03 23:20:09','2019-07-03 23:20:09',NULL,16),(18,'1970-01-01 08:00:00','1970-01-01 21:00:00',6,'BUKA','2019-07-03 23:20:09','2019-07-03 23:20:09',NULL,16),(19,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'MASUK','2019-07-03 23:20:09','2019-07-03 23:20:09',NULL,16),(20,'1970-01-01 08:00:00','1970-01-01 21:00:00',1,'BUKA','2019-05-27 18:13:44','2019-05-27 18:13:44',NULL,17),(21,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'BUKA','2019-05-27 18:13:44','2019-05-27 18:13:44',NULL,17),(22,'1970-01-01 08:00:00','1970-01-01 21:00:00',3,'BUKA','2019-05-27 18:13:44','2019-05-27 18:13:44',NULL,17),(23,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'LIBUR','2019-05-27 18:13:44','2019-05-27 18:13:44',NULL,17),(24,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'BUKA','2019-05-27 18:13:44','2019-05-27 18:13:44',NULL,17),(25,'1970-01-01 08:00:00','1970-01-01 21:00:00',6,'BUKA','2019-05-27 18:13:44','2019-05-27 18:13:44',NULL,17),(26,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'BUKA','2019-05-27 18:13:44','2019-05-27 18:13:44',NULL,17),(27,'1970-01-01 08:00:00','1970-01-01 21:00:00',1,'BUKA','2019-07-15 23:57:35','2019-07-15 23:57:35',NULL,18),(28,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'BUKA','2019-07-15 23:57:35','2019-07-15 23:57:35',NULL,18),(29,'1970-01-01 08:00:00','1970-01-01 21:00:00',3,'BUKA','2019-07-15 23:57:35','2019-07-15 23:57:35',NULL,18),(30,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'BUKA','2019-07-15 23:57:35','2019-07-15 23:57:35',NULL,18),(31,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'BUKA','2019-07-15 23:57:35','2019-07-15 23:57:35',NULL,18),(32,'1970-01-01 08:00:00','1970-01-01 21:00:00',6,'BUKA','2019-07-15 23:57:35','2019-07-15 23:57:35',NULL,18),(33,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'TUTUP','2019-07-15 23:57:35','2019-07-15 23:57:35',NULL,18);
/*!40000 ALTER TABLE `jadwalbarber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jadwalstylist`
--

DROP TABLE IF EXISTS `jadwalstylist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jadwalstylist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jamawalkerja` datetime DEFAULT NULL,
  `jamakhirkerja` datetime DEFAULT NULL,
  `harikerja` int(11) DEFAULT NULL,
  `masuklibur` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `stylist_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_jadwalstylist_stylist1_idx` (`stylist_id`),
  CONSTRAINT `fk_jadwalstylist_stylist1` FOREIGN KEY (`stylist_id`) REFERENCES `stylist` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jadwalstylist`
--

LOCK TABLES `jadwalstylist` WRITE;
/*!40000 ALTER TABLE `jadwalstylist` DISABLE KEYS */;
INSERT INTO `jadwalstylist` VALUES (1,'1970-01-01 03:15:00','1970-01-01 21:00:00',1,'MASUK','2019-07-02 17:21:37','2019-07-02 17:21:37',NULL,3),(4,'1970-01-01 08:00:00','1970-01-01 21:00:00',1,'MASUK','2019-07-02 15:04:13','2019-07-02 15:04:13',NULL,1),(5,'1970-01-01 06:15:00','1970-01-01 17:15:00',1,'MASUK','2019-05-25 23:06:43','2019-05-25 23:06:43',NULL,17),(6,'1970-01-01 06:30:00','1970-01-01 09:00:00',1,'MASUK','2019-06-18 00:06:52','2019-06-18 00:06:52',NULL,9),(7,'1970-01-01 15:00:00','1970-01-01 23:00:00',1,'MASUK','2019-07-08 20:05:58','2019-07-08 20:05:58',NULL,20),(8,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'MASUK','2019-07-08 20:05:58','2019-07-08 20:05:58',NULL,20),(9,'1970-01-01 08:00:00','1970-01-01 21:00:00',3,'MASUK','2019-07-08 20:05:58','2019-07-08 20:05:58',NULL,20),(10,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'MASUK','2019-07-08 20:05:58','2019-07-08 20:05:58',NULL,20),(11,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'MASUK','2019-07-08 20:05:58','2019-07-08 20:05:58',NULL,20),(12,'1970-01-01 20:00:00','1970-01-01 21:00:00',6,'MASUK','2019-07-08 20:05:58','2019-07-08 20:05:58',NULL,20),(13,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'LIBUR','2019-07-08 20:05:58','2019-07-08 20:05:58',NULL,20),(14,'1970-01-01 08:00:00','1970-01-01 21:00:00',1,'LIBUR','2019-07-08 20:05:00','2019-07-08 20:05:00',NULL,11),(15,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'LIBUR','2019-07-08 20:05:00','2019-07-08 20:05:00',NULL,11),(16,'1970-01-01 08:00:00','1970-01-01 21:00:00',3,'MASUK','2019-07-08 20:05:00','2019-07-08 20:05:00',NULL,11),(17,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'MASUK','2019-07-08 20:05:00','2019-07-08 20:05:00',NULL,11),(18,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'LIBUR','2019-07-08 20:05:00','2019-07-08 20:05:00',NULL,11),(19,'1970-01-01 08:00:00','1970-01-01 21:00:00',6,'MASUK','2019-07-08 20:05:00','2019-07-08 20:05:00',NULL,11),(20,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'MASUK','2019-07-08 20:05:00','2019-07-08 20:05:00',NULL,11),(21,'1970-01-01 08:00:00','1970-01-01 21:00:00',1,'MASUK','2019-06-18 00:06:56','2019-06-18 00:06:56',NULL,8),(22,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'MASUK','2019-06-18 00:06:56','2019-06-18 00:06:56',NULL,8),(23,'1970-01-01 08:00:00','1970-01-01 21:00:00',3,'MASUK','2019-06-18 00:06:56','2019-06-18 00:06:56',NULL,8),(24,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'MASUK','2019-06-18 00:06:56','2019-06-18 00:06:56',NULL,8),(25,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'MASUK','2019-06-18 00:06:56','2019-06-18 00:06:56',NULL,8),(26,'1970-01-01 08:00:00','1970-01-01 21:00:00',6,'MASUK','2019-06-18 00:06:56','2019-06-18 00:06:56',NULL,8),(27,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'MASUK','2019-06-18 00:06:56','2019-06-18 00:06:56',NULL,8),(28,'1970-01-01 08:00:00','1970-01-01 21:00:00',1,'MASUK','2019-07-02 15:04:15','2019-07-02 15:04:15',NULL,6),(29,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'MASUK','2019-07-02 15:04:15','2019-07-02 15:04:15',NULL,6),(30,'1970-01-01 08:00:00','1970-01-01 21:00:00',3,'LIBUR','2019-07-02 15:04:15','2019-07-02 15:04:15',NULL,6),(31,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'MASUK','2019-07-02 15:04:15','2019-07-02 15:04:15',NULL,6),(32,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'MASUK','2019-07-02 15:04:15','2019-07-02 15:04:15',NULL,6),(33,'1970-01-01 08:00:00','1970-01-01 21:00:00',6,'MASUK','2019-07-02 15:04:15','2019-07-02 15:04:15',NULL,6),(34,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'MASUK','2019-07-02 15:04:15','2019-07-02 15:04:15',NULL,6),(35,'1970-01-01 08:00:00','1970-01-01 18:30:00',1,'MASUK','2019-07-02 17:21:34','2019-07-02 17:21:34',NULL,5),(36,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'LIBUR','2019-07-02 17:21:34','2019-07-02 17:21:34',NULL,5),(37,'1970-01-01 12:00:00','1970-01-01 21:00:00',3,'MASUK','2019-07-02 17:21:34','2019-07-02 17:21:34',NULL,5),(38,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'MASUK','2019-07-02 17:21:34','2019-07-02 17:21:34',NULL,5),(39,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'LIBUR','2019-07-02 17:21:34','2019-07-02 17:21:34',NULL,5),(40,'1970-01-01 08:00:00','1970-01-01 21:00:00',6,'LIBUR','2019-07-02 17:21:34','2019-07-02 17:21:34',NULL,5),(41,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'LIBUR','2019-07-02 17:21:34','2019-07-02 17:21:34',NULL,5),(42,'1970-01-01 08:00:00','1970-01-01 21:00:00',1,'MASUK','2019-07-08 20:05:11','2019-07-08 20:05:11',NULL,10),(43,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'MASUK','2019-07-08 20:05:11','2019-07-08 20:05:11',NULL,10),(44,'1970-01-01 08:00:00','1970-01-01 21:00:00',3,'MASUK','2019-07-08 20:05:11','2019-07-08 20:05:11',NULL,10),(45,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'LIBUR','2019-07-08 20:05:11','2019-07-08 20:05:11',NULL,10),(46,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'LIBUR','2019-07-08 20:05:11','2019-07-08 20:05:11',NULL,10),(47,'1970-01-01 08:00:00','1970-01-01 21:00:00',6,'MASUK','2019-07-08 20:05:11','2019-07-08 20:05:11',NULL,10),(48,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'MASUK','2019-07-08 20:05:11','2019-07-08 20:05:11',NULL,10),(49,'1970-01-01 08:00:00','1970-01-01 21:00:00',1,'MASUK','2019-07-02 17:21:36','2019-07-02 17:21:36',NULL,4),(50,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'MASUK','2019-07-02 17:21:36','2019-07-02 17:21:36',NULL,4),(51,'1970-01-01 08:00:00','1970-01-01 21:00:00',3,'MASUK','2019-07-02 17:21:36','2019-07-02 17:21:36',NULL,4),(52,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'MASUK','2019-07-02 17:21:36','2019-07-02 17:21:36',NULL,4),(53,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'MASUK','2019-07-02 17:21:36','2019-07-02 17:21:36',NULL,4),(54,'1970-01-01 08:00:00','1970-01-01 21:00:00',6,'LIBUR','2019-07-02 17:21:36','2019-07-02 17:21:36',NULL,4),(55,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'LIBUR','2019-07-02 17:21:36','2019-07-02 17:21:36',NULL,4),(56,'1970-01-01 08:00:00','1970-01-01 21:00:00',1,'MASUK','2019-07-08 20:05:56','2019-07-08 20:05:56',NULL,21),(57,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'MASUK','2019-07-08 20:05:56','2019-07-08 20:05:56',NULL,21),(58,'1970-01-01 08:00:00','1970-01-01 21:00:00',3,'MASUK','2019-07-08 20:05:56','2019-07-08 20:05:56',NULL,21),(59,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'MASUK','2019-07-08 20:05:56','2019-07-08 20:05:56',NULL,21),(60,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'MASUK','2019-07-08 20:05:56','2019-07-08 20:05:56',NULL,21),(61,'1970-01-01 08:00:00','1970-01-01 21:00:00',6,'MASUK','2019-07-08 20:05:56','2019-07-08 20:05:56',NULL,21),(62,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'MASUK','2019-07-08 20:05:56','2019-07-08 20:05:56',NULL,21),(63,'1970-01-01 08:00:00','1970-01-01 21:00:00',1,'MASUK','2019-07-08 20:05:54','2019-07-08 20:05:54',NULL,22),(64,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'MASUK','2019-07-08 20:05:54','2019-07-08 20:05:54',NULL,22),(65,'1970-01-01 08:00:00','1970-01-01 21:00:00',3,'MASUK','2019-07-08 20:05:54','2019-07-08 20:05:54',NULL,22),(66,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'MASUK','2019-07-08 20:05:54','2019-07-08 20:05:54',NULL,22),(67,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'MASUK','2019-07-08 20:05:54','2019-07-08 20:05:54',NULL,22),(68,'1970-01-01 08:00:00','1970-01-01 21:00:00',6,'MASUK','2019-07-08 20:05:54','2019-07-08 20:05:54',NULL,22),(69,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'MASUK','2019-07-08 20:05:54','2019-07-08 20:05:54',NULL,22),(70,'1970-01-01 08:00:00','1970-01-01 21:00:00',1,'MASUK','2019-07-02 17:20:15','2019-07-02 17:20:15',NULL,23),(71,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'MASUK','2019-07-02 17:20:15','2019-07-02 17:20:15',NULL,23),(72,'1970-01-01 08:00:00','1970-01-01 21:00:00',3,'MASUK','2019-07-02 17:20:15','2019-07-02 17:20:15',NULL,23),(73,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'MASUK','2019-07-02 17:20:15','2019-07-02 17:20:15',NULL,23),(74,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'MASUK','2019-07-02 17:20:15','2019-07-02 17:20:15',NULL,23),(75,'1970-01-01 08:00:00','1970-01-01 21:00:00',6,'MASUK','2019-07-02 17:20:15','2019-07-02 17:20:15',NULL,23),(76,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'MASUK','2019-07-02 17:20:15','2019-07-02 17:20:15',NULL,23),(77,'1970-01-01 08:00:00','1970-01-01 21:00:00',1,'MASUK','2019-07-02 17:21:53','2019-07-02 17:21:53',NULL,24),(78,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'MASUK','2019-07-02 17:21:53','2019-07-02 17:21:53',NULL,24),(79,'1970-01-01 08:00:00','1970-01-01 21:00:00',3,'MASUK','2019-07-02 17:21:53','2019-07-02 17:21:53',NULL,24),(80,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'MASUK','2019-07-02 17:21:53','2019-07-02 17:21:53',NULL,24),(81,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'MASUK','2019-07-02 17:21:53','2019-07-02 17:21:53',NULL,24),(82,'1970-01-01 08:00:00','1970-01-01 21:00:00',6,'MASUK','2019-07-02 17:21:53','2019-07-02 17:21:53',NULL,24),(83,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'MASUK','2019-07-02 17:21:53','2019-07-02 17:21:53',NULL,24),(84,'1970-01-01 08:00:00','1970-01-01 18:30:00',1,'MASUK','2019-07-03 23:09:00','2019-07-03 23:09:00',NULL,25),(85,'1970-01-01 08:00:00','1970-01-01 18:30:00',2,'MASUK','2019-07-03 23:09:00','2019-07-03 23:09:00',NULL,25),(86,'1970-01-01 08:00:00','1970-01-01 18:30:00',3,'MASUK','2019-07-03 23:09:00','2019-07-03 23:09:00',NULL,25),(87,'1970-01-01 08:00:00','1970-01-01 18:30:00',4,'MASUK','2019-07-03 23:09:00','2019-07-03 23:09:00',NULL,25),(88,'1970-01-01 08:00:00','1970-01-01 18:30:00',5,'MASUK','2019-07-03 23:09:00','2019-07-03 23:09:00',NULL,25),(89,'1970-01-01 08:00:00','1970-01-01 18:30:00',6,'MASUK','2019-07-03 23:09:00','2019-07-03 23:09:00',NULL,25),(90,'1970-01-01 08:00:00','1970-01-01 18:30:00',7,'MASUK','2019-07-03 23:09:00','2019-07-03 23:09:00',NULL,25),(91,'1970-01-01 08:00:00','1970-01-01 21:00:00',1,'LIBUR','2019-07-03 23:36:30','2019-07-03 23:36:30',NULL,26),(92,'1970-01-01 08:00:00','1970-01-01 21:00:00',2,'MASUK','2019-07-03 23:36:30','2019-07-03 23:36:30',NULL,26),(93,'1970-01-01 08:00:00','1970-01-01 21:00:00',3,'MASUK','2019-07-03 23:36:30','2019-07-03 23:36:30',NULL,26),(94,'1970-01-01 08:00:00','1970-01-01 21:00:00',4,'MASUK','2019-07-03 23:36:30','2019-07-03 23:36:30',NULL,26),(95,'1970-01-01 08:00:00','1970-01-01 21:00:00',5,'MASUK','2019-07-03 23:36:30','2019-07-03 23:36:30',NULL,26),(96,'1970-01-01 08:00:00','1970-01-01 21:00:00',6,'MASUK','2019-07-03 23:36:30','2019-07-03 23:36:30',NULL,26),(97,'1970-01-01 08:00:00','1970-01-01 21:00:00',7,'MASUK','2019-07-03 23:36:30','2019-07-03 23:36:30',NULL,26);
/*!40000 ALTER TABLE `jadwalstylist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `promo`
--

DROP TABLE IF EXISTS `promo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `promo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namapromo` varchar(45) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `waktuawal` datetime DEFAULT NULL,
  `waktuakhir` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_promo_user1_idx` (`user_id`),
  CONSTRAINT `fk_promo_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `promo`
--

LOCK TABLES `promo` WRITE;
/*!40000 ALTER TABLE `promo` DISABLE KEYS */;
INSERT INTO `promo` VALUES (1,'abc',95,'2019-02-28 06:30:00','2019-03-06 06:30:00','2019-02-26 07:39:47','2019-04-03 23:06:21',NULL,2),(2,'Ok',80,'2019-02-26 18:04:00','2019-02-26 18:04:00','2019-02-26 18:04:43','2019-02-26 18:04:43',NULL,4),(3,'Lkjh',50,'2019-03-19 20:09:00','2019-03-21 20:09:00','2019-03-19 20:10:10','2019-03-19 20:10:10',NULL,NULL);
/*!40000 ALTER TABLE `promo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review` text DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rating_user1_idx` (`user_id`),
  CONSTRAINT `fk_rating_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
INSERT INTO `rating` VALUES (1,'5',5,'1970-01-01 03:15:00','1970-01-01 03:15:00',1);
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namaservice` varchar(45) DEFAULT NULL,
  `tarif` decimal(18,4) DEFAULT 0.0000,
  `lamaservice` int(11) NOT NULL DEFAULT 0,
  `keterangan` text DEFAULT NULL,
  `tipeservice` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_service_user1_idx` (`user_id`),
  CONSTRAINT `fk_service_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'Potong Rambut',20.0000,0,'Plus','Home & Barbershop','2019-02-23 21:27:31',NULL,'2019-02-23 21:27:31',2),(20,'Qwertyuiop',20.0000,0,'Qwert','Only Barbershop','2019-05-18 20:32:30',NULL,'2019-05-18 20:32:30',2),(21,'Pros',20.0000,10,'Pross','Home dan Barbershop','2019-05-18 22:44:50',NULL,'2019-07-08 16:36:49',18),(22,'Akmyu',20000.0000,0,NULL,'Home & Services','2019-06-12 20:39:45',NULL,'2019-06-12 20:39:45',19),(23,'Cutting',20000.0000,0,NULL,'Home & Barbershop','2019-06-12 21:49:24',NULL,'2019-06-12 21:49:24',19),(24,'Ybtbtbgb',0.0000,0,NULL,'Home & Barbershop','2019-06-12 22:03:21',NULL,'2019-06-12 22:03:21',19),(25,'Ughjj',0.0000,0,NULL,'Home & Barbershop','2019-06-12 22:04:01',NULL,'2019-06-12 22:04:01',19),(26,'tyui',60000.0000,0,NULL,'Home dan Barbershop','2019-06-12 23:03:50',NULL,'2019-07-08 16:36:35',18),(27,'iuytt',50000.0000,35,NULL,'Home & Barbershop','2019-06-12 23:56:07',NULL,'2019-06-21 01:47:39',16),(28,'uuhj',6000.0000,0,NULL,'Home & Barbershop','2019-06-13 00:55:16',NULL,'2019-06-13 00:55:16',16),(29,'cutting',50000.0000,2,NULL,'Home & Barbershop','2019-06-21 00:53:20',NULL,'2019-06-21 01:08:07',19),(30,'gyuh',20000.0000,20,NULL,'Home dan Barbershop','2019-06-21 01:47:13',NULL,'2019-07-08 16:36:32',18),(31,'abcdef',40000.0000,40,NULL,'Home dan Barbershop','2019-06-22 18:40:20',NULL,'2019-07-08 16:36:28',18),(32,'null',60000.0000,60,NULL,'Home & Barbershop','2019-07-03 23:24:24',NULL,'2019-07-03 23:24:24',16),(33,'Jos',500000.0000,60,NULL,'Home dan Barbershop','2019-07-08 16:08:10',NULL,'2019-07-08 16:36:25',18),(34,'Top',5000.0000,50,NULL,'Home dan Barbershop','2019-07-08 16:16:12',NULL,'2019-07-08 16:36:22',18),(35,'Loi',5000.0000,10,NULL,'Home dan Barbershop','2019-07-08 16:20:47',NULL,'2019-07-08 17:05:49',18);
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stylist`
--

DROP TABLE IF EXISTS `stylist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stylist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namastylist` varchar(45) DEFAULT NULL,
  `statuskerja` int(11) DEFAULT 0,
  `urlgambar` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_stylist_user1_idx` (`user_id`),
  CONSTRAINT `fk_stylist_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stylist`
--

LOCK TABLES `stylist` WRITE;
/*!40000 ALTER TABLE `stylist` DISABLE KEYS */;
INSERT INTO `stylist` VALUES (1,'Paijo',0,'/storage/emulated/0/DCIM/Camera/IMG_20190220_161757.jpg','2019-02-26 07:40:11','2019-07-02 15:04:13',NULL,18),(2,'Abc',0,NULL,'2019-02-28 02:26:11','2019-07-02 15:04:13',NULL,18),(3,'Sastro Gendon',0,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/fotostylist%2Fb1d92a4e-da5c-41b8-9282-ad69274b4eaf?alt=media&token=d18fbab8-157f-4eef-a362-1d6dccc5e12b','2019-02-28 22:43:51','2019-07-02 15:04:12',NULL,18),(4,'Timo',0,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/fotostylist%2F6903a608-71c8-455b-8385-37c692d5067e?alt=media&token=86207e5f-3afa-46ab-bcca-0e94684a748c','2019-02-28 22:44:44','2019-06-13 11:26:55',NULL,18),(5,'Bg',0,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/fotostylist%2Fea9b7418-af16-4beb-abe2-e544aee30eda?alt=media&token=513b0b09-c399-4d76-aa4a-76943c24451d','2019-02-28 22:57:56','2019-07-02 15:05:01',NULL,18),(6,'Bejo',0,'/storage/emulated/0/DCIM/Camera/IMG_20190227_220202.jpg','2019-03-01 20:21:38','2019-07-02 15:04:15',NULL,18),(7,'Abcde',0,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/fotostylist%2Fe6b94d83-2d62-4159-9190-0c76e486bdae?alt=media&token=033f6aa3-9e58-4740-89e1-a0d484754495','2019-03-01 20:33:14','2019-07-08 19:18:54',NULL,18),(8,'Hjkl',0,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/fotostylist%2Fa56b5215-2887-4939-801c-1526ab127f3f?alt=media&token=be7a2659-da8b-4ee9-ab47-a08398b01be1','2019-03-01 22:11:40','2019-06-22 23:10:20','2019-06-22 23:10:20',18),(9,'Lol',1,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/fotostylist%2Feb0664c6-09fc-4e49-bf9c-04cbdc0348fd?alt=media&token=ffa8653b-e799-4ef2-83ed-381a4693a30a','2019-03-01 23:11:44','2019-07-02 17:21:24','2019-07-02 17:21:24',18),(10,'Pop',0,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/fotostylist%2Fe8eb2c76-5b75-419d-8adc-e77a636906db?alt=media&token=a070f141-5d16-4f3c-82b5-ada565425b83','2019-03-01 23:12:04','2019-07-02 17:20:13',NULL,18),(11,'Tem',0,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/fotostylist%2Fdf9478e8-b321-4640-ae15-29d6a59f396e?alt=media&token=da81f805-7cc2-491b-85ad-590842ed7045','2019-03-01 23:12:42','2019-07-02 17:20:15',NULL,18),(12,'Momonokkk',1,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/fotostylist%2F336a5411-7e20-477c-89e9-9e8c220afccd?alt=media&token=709cde9c-f0cb-4c6b-839a-f64b2316142c','2019-03-01 23:16:46','2019-05-25 23:08:14','2019-05-25 23:08:14',2),(13,'Ajja',1,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/fotostylist%2F5fb39352-51b5-4097-add7-7119aba1d407?alt=media&token=36760ca1-8aca-4790-a1a8-81edab916b82','2019-03-01 23:19:46','2019-05-25 23:08:22','2019-05-25 23:08:22',2),(14,'Budi',1,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/fotostylist%2F5fb39352-51b5-4097-add7-7119aba1d407?alt=media&token=36760ca1-8aca-4790-a1a8-81edab916b82','2019-03-01 23:47:21','2019-03-02 01:46:34','2019-03-02 01:46:34',2),(15,'Yuy',1,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/fotostylist%2Fe56adafa-66d6-42f1-a87f-e73e87d18d85?alt=media&token=3f39a1e7-3b47-49c5-98e6-1da2bcadae2b','2019-03-01 23:59:04','2019-05-25 22:59:36','2019-05-25 22:59:36',2),(16,'Jklmn',1,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/fotostylist%2Fe6b94d83-2d62-4159-9190-0c76e486bdae?alt=media&token=033f6aa3-9e58-4740-89e1-a0d484754495','2019-03-02 00:00:55','2019-05-25 22:51:15','2019-05-25 22:51:15',2),(17,'Qwertyhu',1,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/fotostylist%2F52332123-dc09-4223-80b1-de1d7c61df88?alt=media&token=405f58d1-1dfa-4958-b8e0-86ab68b34ba3','2019-03-02 00:28:37','2019-05-25 23:06:51','2019-05-25 23:06:51',1),(20,'ggggdspo',0,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/images%2Fstylist%2F32c1fa82-f37b-40d3-8042-039c5e433c57?alt=media&token=ebbbdcde-a453-4a85-905d-2afca2a7550f','2019-05-25 22:00:36','2019-07-02 17:20:13',NULL,18),(21,'pidi',0,NULL,'2019-06-22 23:07:41','2019-06-29 00:34:42',NULL,18),(22,'reyo',0,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/images%2Fstylist%2F2f24b64c-731a-4508-9ae9-08e32a2760c3?alt=media&token=af01d887-0035-4f8f-8bd6-2cca32bee041','2019-06-22 23:19:48','2019-07-02 17:20:16',NULL,18),(23,'reno',0,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/images%2Fstylist%2F5b637856-5f07-4e79-a5fd-0d5f6deb8a30?alt=media&token=3a7f6985-5407-42d0-b924-9ad626c74006','2019-06-22 23:23:24','2019-06-29 00:07:53',NULL,18),(24,'pije',0,NULL,'2019-07-02 17:21:53','2019-07-02 17:21:53',NULL,18),(25,'edo',0,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/images%2Fstylist%2F1585b017-edf2-4bae-adef-d43b669b2c22?alt=media&token=32e697f4-87e0-40f8-9357-1c5a19d0fa2c','2019-07-03 23:08:57','2019-07-03 23:08:57',NULL,16),(26,'joko',0,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/images%2Fstylist%2Ff60e66d0-7a4c-48e2-8553-e64e3e9e3e80?alt=media&token=cc622930-adbf-4109-81e5-141fed58e91d','2019-07-03 23:36:30','2019-07-03 23:36:30',NULL,16);
/*!40000 ALTER TABLE `stylist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stylistservices`
--

DROP TABLE IF EXISTS `stylistservices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stylistservices` (
  `id` int(11) NOT NULL,
  `stylist_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stylist_id` (`stylist_id`),
  KEY `service_id` (`service_id`),
  CONSTRAINT `stylistservices_ibfk_1` FOREIGN KEY (`stylist_id`) REFERENCES `stylist` (`id`),
  CONSTRAINT `stylistservices_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stylistservices`
--

LOCK TABLES `stylistservices` WRITE;
/*!40000 ALTER TABLE `stylistservices` DISABLE KEYS */;
INSERT INTO `stylistservices` VALUES (1,1,1);
/*!40000 ALTER TABLE `stylistservices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksisaldo`
--

DROP TABLE IF EXISTS `transaksisaldo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksisaldo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipetransaksi` varchar(45) DEFAULT NULL,
  `nominal` decimal(18,4) DEFAULT 0.0000,
  `rekening` varchar(45) DEFAULT NULL,
  `statusverifikasi` int(11) DEFAULT 0,
  `urlbuktibayar` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_transaksisaldo_user1_idx` (`user_id`),
  CONSTRAINT `fk_transaksisaldo_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksisaldo`
--

LOCK TABLES `transaksisaldo` WRITE;
/*!40000 ALTER TABLE `transaksisaldo` DISABLE KEYS */;
INSERT INTO `transaksisaldo` VALUES (1,'WITHDRAW',6000.0000,NULL,1,NULL,'2019-02-23 21:39:26','2019-02-24 22:33:41',3),(2,'TOPUP',50000.0000,'123456789',0,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/buktitransfer%2F4de8a1a2-640e-464d-815d-c246aa72dd11?alt=media&token=e7044783-8272-43ec-817a-014e49389739','2019-02-23 23:30:24','2019-02-24 14:46:45',3),(3,'TOPUP',50000.0000,'123456789',1,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/buktitransfer%2F8d084c14-f947-4d65-93ca-7f91fb2fd6b5?alt=media&token=38e4a69b-169e-45e7-8d43-2441c40be4b2','2019-02-24 00:46:56','2019-02-24 14:48:17',3),(4,'TOPUP',500000.0000,'123456789',0,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/buktitransfer%2F566cf12d-4ac5-4bbd-a0a9-d68adde307aa?alt=media&token=336e34a1-149e-448b-929c-8a5a82efe771','2019-02-28 00:58:03','2019-02-28 00:58:03',3),(5,'TOPUP',90000.0000,'123456789',0,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/buktitransfer%2Febc106f3-8b81-4c7d-9170-6eda8f139882?alt=media&token=43c5871d-9644-4f38-bff1-4980d6f1621e','2019-02-28 01:20:26','2019-02-28 01:20:26',3),(6,'TOPUP',50000.0000,'123456789',1,'https://firebasestorage.googleapis.com/v0/b/barberklimis-d4fb9.appspot.com/o/buktitransfer%2Fabf1b7fb-d26f-4be7-bdff-cca3e1c51c5e?alt=media&token=f1aedb83-809e-4b79-a453-635e503bdd02','2019-03-17 17:39:43','2019-03-17 17:41:54',3),(7,'TOPUP',50000.0000,NULL,0,NULL,'2019-06-19 18:37:59','2019-06-19 18:37:59',3),(8,'TOPUP',0.0000,NULL,0,NULL,'2019-06-22 17:29:23','2019-06-22 17:29:23',18),(9,'TOPUP',NULL,NULL,0,NULL,'2019-06-22 18:05:39','2019-06-22 18:05:39',18),(10,'TOPUP',50000.0000,'1741852936',0,NULL,'2019-06-22 18:29:04','2019-06-22 18:29:04',3),(11,'TOPUP',50000.0000,'1741852936',0,NULL,'2019-06-22 18:31:53','2019-06-22 18:31:53',3),(12,'TOPUP',50000.0000,'1741852936',0,NULL,'2019-06-22 18:34:22','2019-06-22 18:34:22',3),(13,'TOPUP',NULL,NULL,0,NULL,'2019-06-22 18:55:05','2019-06-22 18:55:05',18),(14,'TOPUP',NULL,NULL,0,NULL,'2019-06-22 19:11:02','2019-06-22 19:11:02',18),(15,'TOPUP',NULL,NULL,0,NULL,'2019-06-22 19:18:56','2019-06-22 19:18:56',18),(16,'TOPUP',NULL,NULL,0,NULL,'2019-06-22 22:21:39','2019-06-22 22:21:39',18),(17,'TOPUP',NULL,NULL,0,NULL,'2019-06-22 22:54:31','2019-06-22 22:54:31',18),(18,'TOPUP',NULL,NULL,0,NULL,'2019-06-22 23:25:35','2019-06-22 23:25:35',18),(19,'TOPUP',NULL,NULL,0,NULL,'2019-06-22 23:28:48','2019-06-22 23:28:48',18),(20,'TOPUP',NULL,NULL,0,NULL,'2019-06-22 23:29:02','2019-06-22 23:29:02',18),(21,'TOPUP',NULL,NULL,0,NULL,'2019-06-23 23:03:36','2019-06-23 23:03:36',18),(22,'TOPUP',50.0000,'1741852936',0,NULL,'2019-06-23 23:20:11','2019-06-23 23:20:11',3),(23,'TOPUP',50.0000,'1741852936',0,NULL,'2019-06-23 23:47:52','2019-06-23 23:47:52',3),(24,'TOPUP',NULL,NULL,0,NULL,'2019-06-24 00:07:17','2019-06-24 00:07:17',18),(25,'TOPUP',NULL,NULL,0,NULL,'2019-06-24 00:07:32','2019-06-24 00:07:32',18),(26,'TOPUP',NULL,NULL,0,NULL,'2019-06-24 00:07:35','2019-06-24 00:07:35',18),(27,'TOPUP',NULL,NULL,0,NULL,'2019-06-24 00:07:38','2019-06-24 00:07:38',18),(28,'TOPUP',50.0000,'1741852936',0,NULL,'2019-06-27 22:41:02','2019-06-27 22:41:02',3),(29,'TOPUP',50.0000,'1741852936',0,NULL,'2019-06-27 22:42:30','2019-06-27 22:42:30',3),(30,'TOPUP',50.0000,'1741852936',0,NULL,'2019-06-27 22:43:11','2019-06-27 22:43:11',3),(31,'TOPUP',50.0000,'1741852936',0,NULL,'2019-06-27 22:43:55','2019-06-27 22:43:55',3),(32,'TOPUP',50.0000,'1741852936',0,NULL,'2019-06-27 22:47:02','2019-06-27 22:47:02',3),(33,'TOPUP',50.0000,'1741852936',0,NULL,'2019-06-27 22:49:32','2019-06-27 22:49:32',3),(34,'TOPUP',50.0000,'1741852936',0,'dkbgjdljbndjobndoboubndojbdg','2019-06-27 22:50:01','2019-06-27 22:50:01',3),(35,'TOPUP',50.0000,'1741852936',0,'dkbgjdljbndjobndoboubndojbdg','2019-06-27 22:54:28','2019-06-27 22:54:28',3),(36,'TOPUP',NULL,NULL,0,NULL,'2019-06-29 00:02:03','2019-06-29 00:02:03',18),(37,'TOPUP',NULL,NULL,0,NULL,'2019-07-08 14:30:43','2019-07-08 14:30:43',18),(38,'TOPUP',NULL,NULL,0,NULL,'2019-07-08 14:34:21','2019-07-08 14:34:21',18),(39,'TOPUP',NULL,NULL,0,NULL,'2019-07-08 14:36:33','2019-07-08 14:36:33',18),(40,'TOPUP',NULL,NULL,0,NULL,'2019-07-08 14:38:56','2019-07-08 14:38:56',18),(41,'TOPUP',NULL,NULL,0,NULL,'2019-07-08 17:07:33','2019-07-08 17:07:33',18),(42,'TOPUP',NULL,NULL,0,NULL,'2019-07-08 17:29:40','2019-07-08 17:29:40',18),(43,'TOPUP',NULL,NULL,0,NULL,'2019-07-08 17:34:13','2019-07-08 17:34:13',18),(44,'TOPUP',NULL,NULL,0,NULL,'2019-07-08 17:56:05','2019-07-08 17:56:05',18),(45,'TOPUP',NULL,NULL,0,NULL,'2019-07-08 18:09:22','2019-07-08 18:09:22',18);
/*!40000 ALTER TABLE `transaksisaldo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `telepon` varchar(45) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `saldo` decimal(16,2) NOT NULL DEFAULT 0.00,
  `statusjalan` tinyint(4) DEFAULT NULL,
  `statusverified` tinyint(4) DEFAULT NULL,
  `jenisuser` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `remember_token` text DEFAULT NULL,
  `api_token` text DEFAULT NULL,
  `pinalamat` text DEFAULT NULL,
  `pinlatitude` decimal(11,8) DEFAULT 0.00000000,
  `pinlongitude` decimal(11,8) DEFAULT 0.00000000,
  `statusbuka` int(1) DEFAULT 1,
  `firebase_auth_token` text DEFAULT NULL,
  `firebase_auth_provider` varchar(45) DEFAULT NULL,
  `firebase_auth_uid` text DEFAULT NULL,
  `firebase_instance_id` text DEFAULT NULL,
  `firebase_fcm_token` text DEFAULT NULL,
  `firebase_auth_password_hash` text DEFAULT NULL,
  `firebase_auth_token_valid_at` text DEFAULT NULL,
  `firebase_auth_last_login_at` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','admin@gmail.com','$2y$10$CEcqEi2c0U0oJRFVcqCzkeT22DmeSiWLDfjTAt7c5gz07yDdMsIU.',NULL,NULL,0.00,1,1,1,'2019-02-23 21:09:18','2019-02-23 21:09:18',NULL,'o4ugyh3L08R6a0XRVF3VHvfq6rEFcSqyfUmQZdgDH3N2n5sFlhNA2cCNdnoT','yUoNq7P2n6DMgZcBMT58RjWGgG466QjxKjk4pTUA9V9A8DyweUsMiSJB8qay',NULL,-7.27208350,112.74260940,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Metroa Barbershopy.','metro@gmail.com','$2y$10$K8X/25gPFJdHHihljIhLs.ZqoEUtla2Vf3MuETdvRh9pDgJe.GSGm','0987654321','Jl. Tenggilis Mejoyo Selatan 2 no. 18 Surabaya',0.00,1,1,2,'2019-02-23 21:12:38','2019-05-27 15:03:17',NULL,NULL,'D1zMNVDG7a355Orw2QNsiA8hHYvIyxrimy3WnP8QLbZODubeyLmYnn4ZVh9d',NULL,-7.27208350,112.74260940,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Temmy Kurniawan','temmy@gmail.com','$2y$10$CewjHRJ5uA6u9GLH/0XeNeZDUSDwTzKYXdleBNYfEJ3kWNLenKagW','58479312','Surabaya',94000.00,1,1,2,'2019-02-23 21:12:59','2019-05-18 00:45:01',NULL,NULL,'qKTZFkhUdhCgv8lo5a9nCrGzLPovwpQMX7D1Hu7ngtJARttLrskEPAtkjmpJ','',-7.27208350,112.74260940,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'','',NULL,NULL,NULL,0.00,NULL,0,2,'2019-02-26 07:44:55','2019-02-26 07:44:55',NULL,NULL,'DDmCQ8i07cVfVr3WSSLsbIaAZO4Eb2nd8NfKqFK0QgYc2u6HC4JYN2NJBTzO',NULL,-7.27208350,112.74260940,NULL,'112226324620034947415','google',NULL,NULL,NULL,NULL,NULL,NULL),(5,'Yusuf Barbershop','yusuf@gmail.com','$2y$10$OmUZWXBlTXNC5U1w9AB8u.1n.8TTRDfNWjrf9XkuOkUKXqz8TXXV2',NULL,'Jl.Panjang jiwo no.20 Surabaya',0.00,1,1,3,'2019-02-26 07:51:42','2019-02-26 07:51:42',NULL,NULL,'2jaOD8XZI8ZKMDqiuhd0FB6tkcmnciuFPKXoQDsW9OoCkNn1unKaWJlcf77w',NULL,-7.27208350,112.74260940,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'Bejo Barberahop','bejo@gmail.com','$2y$10$8t00PLbORAng7hjda12G2uA6wo8It1US6KKEVYiuItujwA7RpAkXy','0123456789','Surabaya',0.00,1,1,3,'2019-04-16 17:25:30','2019-04-16 17:25:30',NULL,NULL,'Y4EDciSYGbC35lgkkjfhKjhZcDpaMrVzeI2E6LxfP3yqqLCPS6WQEh7YnYgm',NULL,-7.27208350,112.74260940,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'Tino Kurniawan','tino@gmail.com','$2y$10$IULUCIhkOWcJYzNbYVJNAOV08SBX9u4tjMisKx02FhNcN0vAYx8ji','1243454997','Surabaya',0.00,1,1,3,'2019-05-18 01:00:12','2019-05-18 03:10:19',NULL,NULL,'yK653EGl9zj5ovkngyz7PMyuwUMHTSZzosI0wPlH1gppwpVqg2AO8Mfvgu7a','Jl. Tenggilis Mejoyo Selatan II No.18, Tenggilis Mejoyo, Kec. Tenggilis Mejoyo, Kota SBY, Jawa Timur 60292, Indonesia, Kecamatan Tenggilis Mejoyo',-7.32092530,112.76022840,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,'budi','budi@gmail.com','$2y$10$1iLLAFi2xMAHTqnQ.b4qvOeH6SueOPY0xLjLPI/32wmAgEKg4p/FG','147258369','Surabaya',0.00,1,1,3,'2019-05-18 03:34:56','2019-05-18 03:36:15',NULL,NULL,'P6EuTAZwXuVjO6pC1nCFPlBbEXqGeuvVB2YML4aXfrtWb4Hw1jG1YcCxrvwr','Jl. Tenggilis Mejoyo Selatan II No.18, Tenggilis Mejoyo, Kec. Tenggilis Mejoyo, Kota SBY, Jawa Timur 60292, Indonesia, Kecamatan Tenggilis Mejoyo',-7.32092530,112.76022840,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,'angga','angga@gmail.com','$2y$10$kQMtBHZZCajhP6GtpjFBHuzRYczHL4jqF3GAz6Fh257W6fLI.Bg8i','8521473699','Surabaya',0.00,1,1,3,'2019-05-18 03:47:00','2019-05-18 03:49:41',NULL,NULL,'kJFawJja5xgnsf1KrDBxfwPdl66gKVgVtcbeYNM0hFP0vnhvXqrHEe48dnkd','Jl. Tenggilis Mejoyo Selatan II No.18, Tenggilis Mejoyo, Kec. Tenggilis Mejoyo, Kota SBY, Jawa Timur 60292, Indonesia, Kecamatan Tenggilis Mejoyo',-7.32093520,112.76022940,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,'Barber Kita Kita Baru Buka Ya Jadi Apa Adanya','dian.yulius@gmail.com',NULL,'',NULL,0.00,NULL,NULL,3,'2019-05-27 15:09:54','2019-07-03 22:47:05',NULL,NULL,'QsgHTQKrskaJakBP8S9nkAEtTIy112nvPkPCRYbFDs5ZFqNvRUEfr3To7AxA','Ruko Apartment Metropolis, Jl. Raya Tenggilis Mejoyo No.127, Tenggilis Mejoyo, Kec. Tenggilis Mejoyo, Kota SBY, Jawa Timur 60292, Indonesia, Kecamatan Tenggilis Mejoyo',-7.32002380,112.75897250,1,'eyJhbGciOiJSUzI1NiIsImtpZCI6IjY2NDNkZDM5ZDM4ZGI4NWU1NjAxN2E2OGE3NWMyZjM4YmUxMGM1MzkiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiRGlhbiBOdWdyYWhhIiwicGljdHVyZSI6Imh0dHBzOi8vbGg2Lmdvb2dsZXVzZXJjb250ZW50LmNvbS8tdkZwcDNkZDQtSFUvQUFBQUFBQUFBQUkvQUFBQUFBQUFBQUEvQUNMR3lXRGFpRnRzX1lDUXJSbk1kQjk3cXBOZVhYWVhEdy9zOTYtYy9waG90by5qcGciLCJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vYmFyYmVya2xpbWlzLWQ0ZmI5IiwiYXVkIjoiYmFyYmVya2xpbWlzLWQ0ZmI5IiwiYXV0aF90aW1lIjoxNTU4OTQ0NTg2LCJ1c2VyX2lkIjoiUWo2MVlsRzdMaFp3UkF2RTBiTHlhek5iNzdwMiIsInN1YiI6IlFqNjFZbEc3TGhad1JBdkUwYkx5YXpOYjc3cDIiLCJpYXQiOjE1NTg5NDQ1ODcsImV4cCI6MTU1ODk0ODE4NywiZW1haWwiOiJkaWFuLnl1bGl1c0BnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJnb29nbGUuY29tIjpbIjEwODM4NjY5MjE0MzIyNTIyNTY2MSJdLCJlbWFpbCI6WyJkaWFuLnl1bGl1c0BnbWFpbC5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJnb29nbGUuY29tIn19.UYLmrKPIkFn-aat1o49fzznHuBITiEV0vPRh4XJ_04EvB-R1Lv5iRWEXSbVncZbJjiRC2xaYGAQR9-_qj3f6CCJRz3t2p4oZ8_WhCX8unZBYQg8l3Suy7aNrHwSoy90UT8SY_-gU8VDyngsa4m8PxTBhgQdXd_paNTEH8cMDukDpmQswCa3l4EpK1VOLU5tBF7HAtNYxlDx-ilyalb3qyc7VHZSufSzENE_Ehi-c4mtKlcZgY2sBbovOScgRQWUEUgw2WtCelMkuXGZ2MamJLCfCgZ99ubW7BTqKCk2OPjUpd1C4ofShV-eA_2hroMDzcOpLMxjMjW_FVcGZtibBKg','google.com','Qj61YlG7LhZwRAvE0bLyazNb77p2',NULL,'c84Tt51yONw:APA91bESq0HJ0mw-iFVnOW_Mr0nEAL_QknrkuwmOQAy6-KNo1J3DED2SpSViw3ApqWCpRdfXuuCRO4V3ibnM44uPFl_frfyGtkOIFPDtmRXX-DeHsEU5Q2RpFZ6PkJpfE5XKFALhoTnI','','2018-05-01 17:53:46','2019-05-27 15:09:46'),(17,'dian nugraha','dian.yulius11@gmail.com',NULL,'',NULL,0.00,NULL,NULL,3,'2019-05-27 18:12:59','2019-05-27 18:13:44',NULL,NULL,'ySkttaOmsz3W7elV88TSfmIEEGNMjTG2BBgOhwE5aybCWCXW4HftYgjE5DJO',NULL,-7.27208350,112.74260940,1,'eyJhbGciOiJSUzI1NiIsImtpZCI6IjY2NDNkZDM5ZDM4ZGI4NWU1NjAxN2E2OGE3NWMyZjM4YmUxMGM1MzkiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiZGlhbiBudWdyYWhhIiwicGljdHVyZSI6Imh0dHBzOi8vbGg1Lmdvb2dsZXVzZXJjb250ZW50LmNvbS8tT19SdEt0Qm5DRlkvQUFBQUFBQUFBQUkvQUFBQUFBQUFBQUEvQUNIaTNyZGt4STJ2Wm1PWDc2dTVjaXhKdnduNVdzVGQ3Zy9zOTYtYy9waG90by5qcGciLCJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vYmFyYmVya2xpbWlzLWQ0ZmI5IiwiYXVkIjoiYmFyYmVya2xpbWlzLWQ0ZmI5IiwiYXV0aF90aW1lIjoxNTU4OTU1NTc3LCJ1c2VyX2lkIjoiSTF2SnJaYUVmSFFPZmRYUXJtNlliSmtFVDlqMSIsInN1YiI6IkkxdkpyWmFFZkhRT2ZkWFFybTZZYkprRVQ5ajEiLCJpYXQiOjE1NTg5NTU1NzgsImV4cCI6MTU1ODk1OTE3OCwiZW1haWwiOiJkaWFuLnl1bGl1czExQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7Imdvb2dsZS5jb20iOlsiMTA3NTU2MzQyMjU0MjU3NTY3MjU1Il0sImVtYWlsIjpbImRpYW4ueXVsaXVzMTFAZ21haWwuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoiZ29vZ2xlLmNvbSJ9fQ.V2aGqdBwyaF7OR8ODeBqQuhA70DovpUMcR7Y6lRGqvZhofT64n4D5ghgoKitFHpJW4qquJW4F2vaGsNatPevQcABf79Cl586zVA30af65oZENE3O_Ocn6hi0HlJSOztIMBxJeRTpXm8VfpawpGsWHNAznWtxUX75TRBQIIK3NL5RcaZLAixuY9a90VusYk-7zKAh0aZeFvUJqeJMoq6xrcLvxlZ1N1CiXE6sK1YQqAH_lncFCY-Vyyh9qhivX43Rr_KpGR5WCLycBBuAXj3sC7DHavOE-fzG-HnUetyyEjB0Fx4-RATszjQgRsHgikEbQC_dB3BgqTUveCJDFtZtZA','google.com','I1vJrZaEfHQOfdXQrm6YbJkET9j1',NULL,NULL,'','2019-05-27 18:12:57','2019-05-27 18:12:57'),(18,'Rismoyo Suryo Hutomo','suryo.rismoyo@gmail.com',NULL,'',NULL,0.00,NULL,NULL,3,'2019-05-27 18:25:34','2019-07-14 17:30:57',NULL,NULL,'NOMR5oKlS8Q7X0uJYc9dDGauzBBg2ha7qgBwuKLGhr2AkExtefV8hgq2VACP','Jl. Tenggilis Mejoyo Selatan II No.16, Tenggilis Mejoyo, Kec. Tenggilis Mejoyo, Kota SBY, Jawa Timur 60292, Indonesia, Kecamatan Tenggilis Mejoyo',-7.32102940,112.76021360,1,'eyJhbGciOiJSUzI1NiIsImtpZCI6IjY2NDNkZDM5ZDM4ZGI4NWU1NjAxN2E2OGE3NWMyZjM4YmUxMGM1MzkiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiUmlzbW95byBTdXJ5byBIdXRvbW8iLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDUuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1weUNheHQyaENocy9BQUFBQUFBQUFBSS9BQUFBQUFBQUFQWS9QTHZqSF9qbXJIby9zOTYtYy9waG90by5qcGciLCJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vYmFyYmVya2xpbWlzLWQ0ZmI5IiwiYXVkIjoiYmFyYmVya2xpbWlzLWQ0ZmI5IiwiYXV0aF90aW1lIjoxNTU4OTU2MzI2LCJ1c2VyX2lkIjoib1BOdncxa2VxNFEyUkRwR3gzQ1h0cnNnR3lkMiIsInN1YiI6Im9QTnZ3MWtlcTRRMlJEcEd4M0NYdHJzZ0d5ZDIiLCJpYXQiOjE1NTg5NTYzMzMsImV4cCI6MTU1ODk1OTkzMywiZW1haWwiOiJzdXJ5by5yaXNtb3lvQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7Imdvb2dsZS5jb20iOlsiMTEyMjI2MzI0NjIwMDM0OTQ3NDE1Il0sImVtYWlsIjpbInN1cnlvLnJpc21veW9AZ21haWwuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoiZ29vZ2xlLmNvbSJ9fQ.cFtmm0xfj5WxquxTSiJdrZ-72EQdO2f9T1UKN1LtH2IVVEjiC3WjfPKMugGIJPhd6Ld71vtwQD4DNPjQoFnTb8f5F8FL6pYTeMQGiFLPkqdxC14XFeBicnMypt5K6aRBCa0-BVP9Q8AaLm04_3vfTGdpz5AxE78UpL55Mfa3v2nLmOkEEsICMIiJ3B_xjCj364h6t6A1HhjlYNeYkCdCrQLS1xr84DxoeyVDS6Fser_TJA8XUVgrQN9k_QKnbZPjSXvllYDP1C1RmQIkbfNu5-8wcxgH3fuppngzvHOIiILMRb7WkueGKsggAkbJR0P-ao_gosNSdMkf8AiBWD4caA','google.com','oPNvw1keq4Q2RDpGx3CXtrsgGyd2',NULL,'fA4RpLuK6c0:APA91bGTzd3c1pEg65z5Hm1GPUjpydvcYS4CIFKeiJPfduYjGn7frbSNNBYO0bPpD89KOrHAJ7ECJRdJ4YTXnRRCs0OvDdGYYRjcw7vUGGlTJ5nrqrm58F9TN2elsWoctBar5g8w5Ax2','','2018-04-30 21:25:13','2019-05-27 18:25:26'),(19,'RISMOIO','rismoyo.suryo@gmail.com',NULL,'',NULL,0.00,NULL,NULL,2,'2019-05-27 18:35:50','2019-06-12 18:01:39',NULL,NULL,'BFHIiaM2hmNzthar0YWEC3yYdirNPct187mIHkSCPcQ8aLuZYV2Vc79zY6m7',NULL,-7.27208350,112.74260940,1,'eyJhbGciOiJSUzI1NiIsImtpZCI6IjY2NDNkZDM5ZDM4ZGI4NWU1NjAxN2E2OGE3NWMyZjM4YmUxMGM1MzkiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiUklTTU9JTyIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vLWlveHMtLTN5X3pnL0FBQUFBQUFBQUFJL0FBQUFBQUFBQU84L3VmbzNneVZKNDlBL3M5Ni1jL3Bob3RvLmpwZyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9iYXJiZXJrbGltaXMtZDRmYjkiLCJhdWQiOiJiYXJiZXJrbGltaXMtZDRmYjkiLCJhdXRoX3RpbWUiOjE1NTg5NTY5NDgsInVzZXJfaWQiOiJhVlpMdTFMcDcwY1BWeUxub2ZRZDJTTm1tMnAyIiwic3ViIjoiYVZaTHUxTHA3MGNQVnlMbm9mUWQyU05tbTJwMiIsImlhdCI6MTU1ODk1Njk0OSwiZXhwIjoxNTU4OTYwNTQ5LCJlbWFpbCI6InJpc21veW8uc3VyeW9AZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZ29vZ2xlLmNvbSI6WyIxMDM2ODg2MDk1NzAyNjE0ODQ5NDUiXSwiZW1haWwiOlsicmlzbW95by5zdXJ5b0BnbWFpbC5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJnb29nbGUuY29tIn19.K2k6Jj2OPtVVLoLtYjDU5dbOLEMIxGARzYpeFHq47BsKnPicOpjAX2H7pXFjUqFB04YjSbIr1gtIezlFEY3I7TbSbW9V5I74ocYSrQ-Kk9IGRudFoG9bhdeL-flxcnn1EY8QFwVYUtJg6pFDWlefX6qGv6NaRdg44e9tEFq7GXLWS_VXrF64vMU3t27I4Lxzxbph4ZoE3nDd5_aY1B5QpHoooNryiTryCcubVcLwpCO2MC_NBySQKWLk_i2gjD_5jQIuom6XHws82OQfXPm9fCwTw65uLtBqKXrJQvW-As9Q-oNMpRjxIwYCeO8rp-7iH6uhxWK88BIeQr8dKwh5ww','google.com','aVZLu1Lp70cPVyLnofQd2SNmm2p2',NULL,'dORGb-bFU-c:APA91bH31YPCIylzmFH77qzm9o5vgggXlGgOejfHuiVFMvMnlrUP5FyKwb57jDWgPS0Z4xcS2izn4p-1OAuV9S4Hf4b8KXk19D0MG26xG-Gn8EAnt1z1AdVGyKdC7Ef9EcDy5_Q4LOBb','','2019-05-27 18:35:48','2019-05-27 18:35:48');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-21 22:47:10
